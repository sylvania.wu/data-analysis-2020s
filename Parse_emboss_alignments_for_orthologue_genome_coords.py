#!/usr/bin/python
import os
import re
import pandas as pd
from optparse import OptionParser
from functions_used_in_find_5utrs import get_base_at_position as get_one_base

# Parse_emboss_alignments_for_orthologue_genome_coords.py
# cd /srv/scratch/z3460340/PhDfiles/termseq2020/compare_with_predicted_terminators_hoon_etal2005/test_results_first_100_lines_for_test_no_NAs_NC_002745_Staph_orthologues_to_jkd6008_seqs
# module add python/intel-3.6.8
# export PATH=$PATH:"/srv/scratch/z3460340/PhDfiles/termseq2020/scripts/"
# parsing_script="../../scripts/Parse_emboss_alignments_for_orthologue_genome_coords.py"
# python $parsing_script --debug=1 --strain="N315" --df_term="table_predicted_terminators_in_N315_orthologues_in_jkd6008.tsv" > stdout_err_parsing_script 2>&1

# Last update June 9, 2020 at 2:14 PM.

parser = OptionParser()

parser.add_option("-d","--debug",action="store",type="int",dest="debug")
parser.add_option("-s", "--strain",action="store",type="string",dest="from_strain")
parser.add_option("-T", "--df_term",action="store",type="string",dest="df_term")

(options, args) = parser.parse_args()


###----------------         Functions                ----------------###

def get_first_number_and_spaces_from_start_of_line(string):
    num_spaces = 0
    found_first_position = ""
    last_type = ""
    for index in range(0, len(string) - 1):
        current_char = string[index]

        if current_char == ' ':
           print("space at " + str(index) + "\n")
           num_spaces = num_spaces + 1
           if last_type == "num":
               break  ### It's a space now and a digit previously. That means we found the first coordinate label.
           last_type = "spac"

        elif current_char.isdigit():
            print("digit at " + str(index) + "\n")
            found_first_position = found_first_position + current_char
            last_type = "num"
        else:
            continue
                         ## keep going until i hit a space
        return ( num_spaces,int(found_first_position) ) ## the first sequence coordinate is found_first_position.
    ### # Margin of file is 7 spaces.
### So num_spaces minus 7 is the number of bases in the original sequence
## The last (rightmost) digit is placed over the labeled base.
## In matcher file, gaps do NOT count towards coordinate numbers.

def grep_alignment_result_from_tempfile(temp_path):
    # previous: os.system("grep 'Score' "+str(temp_path)+" -A 9 | head -n21 | tail -n5 > temp.txt")
    os.system('grep Score '+str(temp_path)+' -A100 | while read -r line; do if [[ $line =~ "Aligned" ]]; then break; else echo "$line" >> temp_from_'+str( os.path.basename(temp_path) )+'; fi; done')
    os.system('head -n -3 temp_from_'+str( os.path.basename(temp_path) )+' > edited_temp_from_'+str( os.path.basename(temp_path) ) )
    tempfile = open('edited_temp_from_'+str( os.path.basename(temp_path) ),'r')
    result_lines = tempfile.readlines()
    tempfile.close()
    return result_lines

### The .readlines() result looks like:
## ['           10        20        30        40        50    \n', '       TGAAGAATAATAAAAAATAAGACTTCCCTATATGTAGGGGAGTCTTATTT\n', '       :::::: ::::::: :::::::::::::::::::::: : ::::::::::\n', '       TGAAGA-TAATAAACAATAAGACTTCCCTATATGTAGCGAAGTCTTATTT\n', '          10         20        30        40        50    \n']


def parse_alignment_file_for_coordinates(*coords,align_file_path,seq_strand,genome_start):
    if (options.debug == 1):
        print("Running parse_alignment_file_for_coordinates()...\n")

    if ( os.path.exists(align_file_path) == False ) or ("NA" in coords) or (genome_start == "NA"):
        print("Alignment does not exist (align_file_path = "+str(align_file_path)+"\n")
        return pd.Series(["NA","NA","NA","NA","NA","NA"],index=[0,0,0,0,0,0])
    # previously.... os.system("grep 'Score' alignment_between_terminator_in_"+str(orthologue_name)+"and"+str(genes_name)+".txt -A 9 | head -n21 | tail -n5")
    # grep_result = grep_alignment_result_from_tempfile("alignment_between_terminator_in_"+str(orthologue_name)+"and"+str(genes_name)+".txt")
    grep_result = grep_alignment_result_from_tempfile(align_file_path)

    #posns_line_og_strain = grep_result[0]
    #seq_line_og = grep_result[1].rstrip()
    #posns_line_jkd6008 = grep_result[4]
    # replace('       ','',1)
    matcher_seqs = [ string.replace('\n','').replace('-','') for string in grep_result if (  ('A' in string[0]) or ('T' in string[0]) or ('C' in string[0]) or ('G' in string[0]) ) ]
    if (options.debug == 1):
        print("For the alignment file: "+str(align_file_path)+"\nmatcher_seqs is:\n"+str(matcher_seqs)+"\n")
    ## Only replace first occurrence of 7 spaces in a row. That's the margin.

    # aligned_seq_pairs = [ (matcher_seqs[n],matcher_seqs[n+1]) for n in range(0,len(matcher_seqs)-1,2) ]
    # starting from 0, even-numbered sequences belong to -asequence (JKD6008), and odd-numbered ones belong to -bsequence (orthologous terminator)
    jkd6008_align_seq = ""
    ogstr_align_seq = ""
    for n in range(0,len(matcher_seqs)-1):
        if n%2 == 0:
            jkd6008_align_seq = jkd6008_align_seq + matcher_seqs[n]
        else:
           ogstr_align_seq = ogstr_align_seq + matcher_seqs[n]

    # .replace('       ','',1)
    matcher_posns = [ line.replace('\n','') for line in grep_result if line.replace('\n','').replace(' ','').isdigit() ]
    if (options.debug == 1):
        print("For the alignment file: "+str(align_file_path)+"\ngrep_result is:\n"+str(grep_result)+"\n")
        print("For the alignment file: "+str(align_file_path)+"\nmatcher_posns is:\n"+str(matcher_posns)+"\n")

    #### I don't need to find number of spaces minus 7. Redo all
    (spaces_from_first_posn_jkd, posn_marker_jkd) = get_first_number_and_spaces_from_start_of_line(matcher_posns[0])
    (spaces_from_first_posn_og, posn_marker_og) = get_first_number_and_spaces_from_start_of_line(matcher_posns[1])

    # ALignment result:
    #            10        20        30        40        50
    #        TGAAGAATAATAAAAAATAAGACTTCCCTATATGTAGGGGAGTCTTATTT The 10th base is "A"
    # Original sequence: CAGATGAAGAATAATAAAAAATAAGACTTCCCTATATGTAGGGGAGTCTTATTTTTATGCTAG


    coord_where_aligned_starts_og = posn_marker_og - spaces_from_first_posn_og + 1
    coord_where_aligned_starts_jkd = posn_marker_jkd - spaces_from_first_posn_jkd + 1

    coord_where_aligned_ends_og = posn_marker_og + len(ogstr_align_seq) - spaces_from_first_posn_og + 1
    coord_where_aligned_ends_jkd = posn_marker_jkd + len(jkd6008_align_seq) - spaces_from_first_posn_jkd + 1

    ## sequence of numbers
    og_coords = range(coord_where_aligned_starts_og,len(ogstr_align_seq) - spaces_from_first_posn_og + 1)
    jkd_coords = range(coord_where_aligned_starts_jkd,len(jkd6008_align_seq) - spaces_from_first_posn_jkd + 1)

    ## zip together / dictionary?
    from itertools import zip_longest
    coord_conversion = {k:v for k,v in zip_longest(og_coords,jkd_coords)}
    ## Now searching by any coordinate of terminator in orthologous strain, will get coordinate in jkd6008 aligned sequence.

    if (options.debug == 1):
        outfile = open("coord_conversion_"+str(align_file_path),'w')
        for key,value in coord_conversion.items():
            outfile.write("coord in "+str(options.from_strain)+":"+str(key)+", coord in jkd6008: "+str(value)+"\n"+"type(key): "+str(type(key))+"\n")

    converted = pd.Series({})
    for coord in coords: ## there should be four coords
        if (options.debug == 1):
            print("coord is: "+str(coord)+"\n")
        try:
            if (seq_strand == "+"):
                #converted = converted.append( pd.Series("converted_"+str(coord)+"_into_jkd6008_coords:"+str( int(coord_conversion[coord])+int(genome_start) )) )
                converted = converted.append( pd.Series( int(coord_conversion[coord])+int(genome_start) ) )


            elif (seq_strand == "-"):
                #converted = converted.append( pd.Series("converted_"+str(coord)+"_into_jkd6008_coords:"+str( int(genome_start)-int(coord_conversion[coord]) )) )
                converted = converted.append( pd.Series( int(genome_start)-int(coord_conversion[coord]) ) )

            else:
                print("Strand not parsed from term_df for gene aligned in "+str(align_file_path)+"\n")
                #converted = converted.append( pd.Series("converted_"+str(coord)+"_into_jkd6008_coords:"+str(coord_conversion[coord])) )
                converted = converted.append( "strand_not_parsed_converted_coord_is_"+str(coord_conversion[coord]) )
        except KeyError:
            converted = converted.append(pd.Series("coord_off_limits"))

    ### start and end for terminator annotation, GFF.
    jkd_term_start = int(genome_start) + min(jkd_coords[0], jkd_coords[len(jkd_coords)-1])
    jkd_term_end = int(genome_start) + max(jkd_coords[0], jkd_coords[len(jkd_coords)-1])

    converted = converted.append( pd.Series(jkd_term_start) )
    converted = converted.append( pd.Series(jkd_term_end) )

    return converted
# predicted terminator sequence (og_strain) in term_df[4]



### --------------- change the return object type of parse_alignment_file_for_coordinates() like so:
### (longer running time returning a Series than returning an array)
#        >>> def func(df_row):
#        ...    A = int(df_row[0])+5
#        ...    B = int(df_row[1])-5
#        ...    series = pd.Series({'A':A,'B':B})
#        ...    return series

#        >>> df.apply(lambda Row: func(Row), axis=1)
#        #           A  B
#        0  6 -3
#        1  8 -1
#        >>> result = df.apply(lambda Row: func(Row), axis=1)
#        >>> type(result)
#        <class 'pandas.core.frame.DataFrame'>
#        >>> df
#           0  1
#        0  1  2
#        1  3  4
#        >>> pd.concat([df,result],axis=1)
#           0  1  A  B
#        0  1  2  6 -3
#        1  3  4  8 -1

###----------------         End of Functions                ----------------###

term_df = pd.read_csv(options.df_term,sep='\t',index_col=0,skiprows=1,header=None)

# Doesn't work leads to ValueError: cannot reindex from a duplicate axis: term_df.dropna(axis=0, how='any', inplace=True)

term_df.columns = [0,1,2,3,4,5,6,7,8,9,10,11,12,13]

if (options.debug == 1):
    print("term_df dataframe after numbering the columns...\n\n")
    print(str(term_df.head(n=3)))
#  Row[9] is genes_name i.e. name in jdk6008. apple(parse_alignment_file_for_coordinates) returns a pandas series.
# seq_length=len(str(Row[4]))
conv_coords_cols = term_df.apply(lambda Row: parse_alignment_file_for_coordinates(int(Row[5]),int(Row[6]),int(Row[7]),int(Row[8]),align_file_path="alignment_between_terminator_in_"+str(Row[0])+"and"+str(Row[9])+".txt",seq_strand=str(Row[12]),genome_start=int(Row[11]) ) if ("NA" not in [str(Row[11]),str(Row[5]),str(Row[6]),str(Row[7]),str(Row[8])] ) else pd.Series(["NA","NA","NA","NA","NA","NA"],index=[0,0,0,0,0,0]),axis=1)

term_df = pd.concat( [term_df,conv_coords_cols],axis=1 )



if (options.debug == 1):
    print("term_df dataframe after pd.concat( [term_df,conv_coords_cols],axis=1 )...\n\n")
    print(str(term_df.head(n=3)))

term_df.columns = ['original_locus_tag', 'original_gene','predicted_terminator','free_energy','original_dna_seq','hairpin_base_left','hairpin_loop_left','hairpin_loop_right','hairpin_base_right','orthologous_locus_in_jkd6008','jkd6008_locus_start','jkd6008_locus_end','jkd6008_locus_strand','jkd6008_sequence','jkd_hairpin_base_left','jkd_hairpin_loop_left','jkd_hairpin_loop_right','jkd_hairpin_base_right','JKD_start_of_terminator','JKD_end_of_terminator']

term_df.to_csv('after_terminators_coord_conversion_predicted_terminators_in_'+str(options.from_strain)+'_orthologues_in_jkd6008.tsv',sep='\t')

# ----------------- See if I can do this all in a pandas array way??
# term_df[5] is absolute index of the terminator 5' end in og_strain
# ortho_hairpin_baseL = term_df[5]
# ortho_hairpin_loopL = term_df[6]
# ortho_hairpin_loopR = term_df[7]
# ortho_hairpin_baseR = term_df[8]
