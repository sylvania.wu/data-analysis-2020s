#!/usr/bin/python
from optparse import OptionParser
import re



####### Tested. This script works to convert the output of my thresholding_algo() program to a gff3. 
#######  Next, merge all results into one file, convert to gff3, do intersect search with bedtools on the gene coding seqs of Staph.

parser = OptionParser()
parser.add_option("-f", "--fileTermPeaks",action="store",type="string",dest="termPeaksFile")
parser.add_option("-n", "--nameOut",action="store",type="string",dest="name_output")
(options, args) = parser.parse_args()


termPeaksFile = open(options.termPeaksFile,'r')
name_output = options.name_output
outGff = open("./"+str(name_output)+".gff",'w')

enum_term = enumerate(termPeaksFile)
for i,line in enum_term:
    if ("python find_all_threshold_peak" in line):
        continue
    search_termPeakPosn = re.search(r"Term-seq peak found at: ([0-9]*).[0-9]\t",line)
    termPeakPosn = search_termPeakPosn.group(1)
    search_termStrand = re.search(r"Strand: (.)\t",line)
    termStrand = search_termStrand.group(1)
    search_rDepth = re.search(r"Read depth: ([0-9]*).[0-9]\n",line)
    rDepth  = search_rDepth.group(1)
    outGff.write("CP002120.1\ttermSeq\ttermPeak\t"+str(termPeakPosn)+"\t"+str(termPeakPosn)+"\t.\t"+str(termStrand)+"\t.\t"+"ID=termPeak_found_in_"+str(name_output)+"_at_"+str(termPeakPosn)+";Name=termPeak_found_in_"+str(name_output)+"_at_"+str(termPeakPosn)+";Note=Coverage of "+str(rDepth)+"Term-seq reads, trimmed by 3'-end base only;")
