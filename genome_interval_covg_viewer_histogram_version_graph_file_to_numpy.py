#!/usr/bin/env python
from optparse import OptionParser
import os
import sys
import subprocess
from datetime import datetime
import re
import numpy as np
import pandas as pd
import matplotlib.pyplot as matplot_plt
import matplotlib.patches as matplot_patches
from matplotlib.pyplot import figure
import matplotlib.gridspec as gridspec

# Last update August 24, 2020 at 5:30 PM


# Usage August 2020:
# genome_interval_covg_viewer_histogram_version_graph_file_to_numpy.py
# cd /srv/scratch/z3460340/PhDfiles/termseq2020/alignment_vancomcyin_rnaseq_from_DanMediati/
# module add python/intel-3.6.8
# export PATH="$PATH:/srv/scratch/z3460340/PhDfiles/termseq2020/alignment_vancomcyin_rnaseq_from_DanMediati/"
# graphA="/srv/scratch/z3460340/PhDfiles/termseq2020/alignment_vancomcyin_rnaseq_from_DanMediati/strand_specific_rnaseq_dmediati_van1/rnaseq_dmediati_van1_forward_strand_coverage.gr"
# graphB="/srv/scratch/z3460340/PhDfiles/termseq2020/alignment_vancomcyin_rnaseq_from_DanMediati/strand_specific_rnaseq_dmediati_van1/rnaseq_dmediati_van1_reverse_strand_coverage.gr"
# graphC="/srv/scratch/z3460340/PhDfiles/termseq2020/alignment_vancomcyin_rnaseq_from_DanMediati/strand_specific_rnaseq_dmediati_cont1/rnaseq_dmediati_cont1_forward_strand_coverage.gr"
# graphD="/srv/scratch/z3460340/PhDfiles/termseq2020/alignment_vancomcyin_rnaseq_from_DanMediati/strand_specific_rnaseq_dmediati_cont1/rnaseq_dmediati_cont1_reverse_strand_coverage.gr"
# python genome_interval_covg_viewer_histogram_version_graph_file_to_numpy.py --rnaseq_plus_file=${graphA}, --rnaseq_minus_file=${graphB}, --termseq_plus_file=${graphC}, --termseq_minus_file=${graphD} --begin=432991 --end=433953 --output="RNA-seq_control_and_vanco_test_gene_SAA6008_00395" --gene_name="SAA6008_00395" --filetype="graph", --workingDir=$(pwd) --condition="vancomycin"

''' Show histogram of RNA read depth from the SAM/graph file from begin_interval to end_interval '''



# sys.path.append('/srv/scratch/z3460340/PhDfiles/practice_runs/my_termseq_scripts/')
# from common_gene_manipulations import define_gene_coding_sequences

dateTimeObj = datetime.now()

my_timestamp = str(dateTimeObj.day)+"-"+str(dateTimeObj.month)+"-"+str(dateTimeObj.year)+"-"+str(dateTimeObj.hour)+":"+str(dateTimeObj.minute)
print("Hi this program was run at time... "+str(my_timestamp)+"\n")
#Format is year/month/day/time 2018-11-18 09:32:36.435350

parser = OptionParser()

parser.add_option("-A", "--graphA",action="store",type="string",dest="graphA_path")
parser.add_option("-i", "--graphB",action="store",type="string",dest="graphB_path")
parser.add_option("-j", "--graphC",action="store",type="string",dest="graphC_path")
parser.add_option("-k", "--graphD",action="store",type="string",dest="graphD_path")

parser.add_option("-b", "--begin",action="store",type="int",dest="begin_interval")
parser.add_option("-n", "--end",action="store",type="int",dest="end_interval")

parser.add_option("-o", "--output",action="store",type="string",dest="output_filename")
parser.add_option("-a", "--fig_annotation",action="store",type="string",dest="fig_annotation")

parser.add_option("-g", "--gene_name",action="store",type="string",dest="fetch_gene")

parser.add_option("-F", "--filetype",action="store",type="string",dest="file_type")

parser.add_option("-c", "--condition",action="store",type="string",dest="exp_condition")
parser.add_option("-D", "--workingDir",action="store",type="string",dest="currentDirectory")
##### parser.add_option("-M", "--runMode",action="store",type="string",dest="prog_run_mode")

(options, args) = parser.parse_args()

#annotations_file = open(options.annotations_file,'r')

my_working_dir = options.currentDirectory ##### Provide the full path as an argument to the program. This will be used to print out results and/or temporary files....


fetch_condition = options.exp_condition
fetch_gene = options.fetch_gene
output_name = options.output_filename


# RNAseq_samfile_path = options.samfile_path
my_graphA_path = options.graphA_path
my_graphB_path = options.graphB_path
my_graphC_path = options.graphC_path
my_graphD_path = options.graphD_path



start = min(options.end_interval,options.begin_interval)
end = max(options.end_interval,options.begin_interval)

print(f"Start of interval is {start}\n")
print(f"End of interval is {end}\n")

if ( my_working_dir.endswith("/") == False ):
        my_working_dir = my_working_dir+"/"


#----------------- Cut out code to read from sam file into separate function read_covg_histogram_from_samfile(), edit later ---------#

if (options.file_type == "graph"):
    print("Reading from depth graph (.gr) file.\n")

    graphA_df = pd.read_csv(my_graphA_path,sep='\t',index_col=1,header=None,keep_default_na=False, na_values='')
    graphB_df = pd.read_csv(my_graphB_path,sep='\t',index_col=1,header=None,keep_default_na=False, na_values='')
    graphC_df = pd.read_csv(my_graphC_path,sep='\t',index_col=1,header=None,keep_default_na=False, na_values='')
    graphD_df = pd.read_csv(my_graphD_path,sep='\t',index_col=1,header=None,keep_default_na=False, na_values='')

    graphA_df.columns = ['col1','col2']
    graphB_df.columns = ['col1','col2']
    graphC_df.columns = ['col1','col2']
    graphD_df.columns = ['col1','col2']

    col_to_drop = [ x for x in ('col1','col2') if (graphA_df.iloc[1][x] == 'CP002120.1') ][0]
    depth_col = [ y for y in ('col1','col2') if y != str(col_to_drop) ][0]

    graphA_df.drop(labels=col_to_drop, axis=1, inplace=True)
    graphB_df.drop(labels=col_to_drop, axis=1, inplace=True)
    graphC_df.drop(labels=col_to_drop, axis=1, inplace=True)
    graphD_df.drop(labels=col_to_drop, axis=1, inplace=True)

    graphA_array = graphA_df.iloc[start-1:end]
    graphB_array = graphB_df.iloc[start-1:end]

    graphC_array = graphC_df.iloc[start-1:end]
    graphD_array = graphD_df.iloc[start-1:end]

    print(f"Done making numpy array graph A:\n")
    print(graphA_array[:10])
    print("\n")
else:
    print(f"options.file_type is not valid? {options.file_type}\n")



fig = figure(num=None, constrained_layout=True, dpi=200, figsize=(6.5,11),facecolor='w', edgecolor='k')
spec = gridspec.GridSpec(ncols=1, nrows=6, figure=fig)
fig_ax0 = fig.add_subplot(spec[0, 0])
fig_ax1 = fig.add_subplot(spec[1, 0])
fig_ax2 = fig.add_subplot(spec[2, 0])
fig_ax3 = fig.add_subplot(spec[3, 0])
fig_ax4 = fig.add_subplot(spec[4, 0])
fig_ax5 = fig.add_subplot(spec[5, 0])
spec.update(wspace = 0.5, hspace = 0.3)
# EDIT August 20, 2020: REPLACE fig_ax0 with fig_ax0 etc.
# PREVIOUSLY: fig, ax = matplot_plt.subplots(6,sharex=False,figsize=(1, 8) )

# PREVIOUSLY: for axIndex in range(0,4):
for figAxis in [fig_ax0,fig_ax1,fig_ax2,fig_ax3]:
    figAxis.xaxis.set_tick_params(which='both', labelbottom=True)

#### That is, make part of the title (gene name only) italic.
#### NORMAL way (definitely works)
#### fig.suptitle("Transcriptional readthrough at "+str(fetch_gene)+" in "+str(fetch_condition)+" condition")
matplot_plt.subplots_adjust(hspace=0.8)

fig_ax0.title.set_text(os.path.basename(my_graphA_path).split('.')[0])
fig_ax1.title.set_text(os.path.basename(my_graphB_path).split('.')[0])
fig_ax2.title.set_text(os.path.basename(my_graphC_path).split('.')[0])
fig_ax3.title.set_text(os.path.basename(my_graphD_path).split('.')[0])
# fig_ax4.title.set_text('Genes and features on plus strand')
# fig_ax5.title.set_text('Genes and features on minus strand')

#fig_ax0.set_xticks([])
#fig_ax1.set_xticks([])
#fig_ax2.set_xticks([])
#fig_ax3.set_xticks([])

matplot_plt.xticks(rotation=45)

#fig_ax0.set_xlim(0,abs(end-start)+1)
fig_ax0.set_xlim(start-10,end+10)

#fig_ax1.set_xlim(0,abs(end-start)+1)
fig_ax1.set_xlim(start-10,end+10)

#fig_ax2.set_xlim(0,abs(end-start)+1)
fig_ax2.set_xlim(start-10,end+10)

#fig_ax3.set_xlim(0,abs(end-start)+1)
fig_ax3.set_xlim(start-10,end+10)


''' EDIT STARTING FROM HERE '''
graphA_Bar = fig_ax0.bar(x=np.array(graphA_array.index),height=np.array(graphA_array[depth_col]),color='#7ac4bf',width=1.1) # col2 is col_not_removed '#7ac4bf' '#84597e'  '#84597e'
graphB_Bar = fig_ax1.bar(x=np.array(graphB_array.index),height=np.array(graphB_array[depth_col]),color='#7ac4df',width=1.1)

graphC_Bar = fig_ax2.bar(x=np.array(graphC_array.index),height=np.array(graphC_array[depth_col]),color='#df7aba',width=1.1) # col2 is col_not_removed '#7ac4bf' '#84597e'  '#84597e'
graphD_Bar = fig_ax3.bar(x=np.array(graphD_array.index),height=np.array(graphD_array[depth_col]),color='#b9b5c7',width=1.1)

npAindex=np.array(graphA_array.index)
fig_ax0.set_xticks( [i for i in range(npAindex[0],npAindex[len(npAindex)-2],500)] )

npBindex=np.array(graphB_array.index)
fig_ax1.set_xticks( [i for i in range(npBindex[0],npBindex[len(npBindex)-2],500)] )

npCindex=np.array(graphC_array.index)
fig_ax2.set_xticks( [i for i in range(npCindex[0],npCindex[len(npCindex)-2],500)] )

npDindex=np.array(graphD_array.index)
fig_ax3.set_xticks( [i for i in range(npDindex[0],npDindex[len(npDindex)-2],500)] )



################################################################################################################
##### Untested. Draw a single box for the annotation of a gene, named in the commandline.
##### All code before this block (starting if ( args["gene_name"] is not None ):) works just fine.
if ( options.fetch_gene != "ignore" ):
    fig.suptitle("Transcriptional readthrough at "+ r"$\i{" + str(fetch_gene) + "}$"+" in "+str(fetch_condition)+" condition")
    fetch_gene = options.fetch_gene
    print("my gene is "+str(fetch_gene)+" \n")
    fetch_gene_process = subprocess.Popen( [ 'grep',str(fetch_gene),'/srv/scratch/z3460340/PhDfiles/termseq2019/all_Saureus_Termseq_results/config_and_annotation_files/jkd6008_gene_annotations_genbank.gff3' ], stdout=subprocess.PIPE, universal_newlines=True)
    my_stdout_lines = fetch_gene_process.communicate()[0].replace("b'","").replace("\n'","\n")
    lines = my_stdout_lines.split('\n')
    print("my my_stdout_lines is ...\n")
    ## b'output from grep command line of gff file.....\n'
    print(my_stdout_lines)

    line = lines[0]

    print("reading line...\n");
    # line_bytes = fetch_gene_process.stdout.readline()
    # the_encoding = chardet.detect(line_bytes)['encoding']
    # print("the encoding is \n")
    # print(the_encoding)
    # line = line_bytes.decode('ascii','strict')
    line = line.rstrip()
    print("my line is ...\n")
    ## b'output from grep command line of gff file.....\n'
    print(line)
    print("\n")
    #-------------------------reading gff single line ------------------------------#
    if line != '':
        print("Line from stdout of the grep command is....\n")
        # os.write(1, line_bytes)
        ##### TO use a variable in a regex search, you have # TODO:
        #### You have to build the regex as a string:
        my_regex1 = r"ID="+re.escape(fetch_gene)+r";"
        genename_matchobj = re.search(my_regex1,str(line))
        if ( genename_matchobj == None ):
              my_regex2 = r"gene="+re.escape(fetch_gene)+r";"
              genename_matchobj = re.search(my_regex2,str(line))
        if (genename_matchobj):
              line_fields = str(line).split('\t')
              my_CDS_start_pos = int(line_fields[3]) # This is the start codon for a CDS
              my_CDS_end_pos = int(line_fields[4])
              gene_width = abs(my_CDS_start_pos-my_CDS_end_pos)
              strand = line_fields[6]
        else:
              print("could not find gene name in gff file (grep)...\n")
              quit()

    else:
        print("could not read gff file line by line (grep)...\n")
        quit()
    #-------------------------END reading gff single line ------------------------------#

    rect_box_corner_X = min(int(my_CDS_start_pos),int(my_CDS_end_pos))
    rect = matplot_patches.Rectangle( (rect_box_corner_X,0),gene_width,0.5,linewidth=1,edgecolor='b',facecolor='b',clip_on=True)
    fig_ax4.set_xlim(start-10,end+10)
    fig_ax4.add_patch(rect)


    if strand == "+":
        fig_ax4.title.set_text(str(fetch_gene)+' gene on plus strand')
    if strand == "-":
        fig_ax4.title.set_text(str(fetch_gene)+' gene on minus strand')
    ############ END of block if fetch_gene != "ignore"

if (options.fetch_gene == "ignore"):
    show_start = min(end,start) - 10
    show_end = max(end,start) + 10
   ############## bedtools intersect strandedness option (-s or -S) does NOT work! This is very specific to the version of bedtools and to the version of BED format required. Best not to use, or hard-code the method to extract strand info from the results.
    os.system('module add bedtools/2.27.1; cd '+str(my_working_dir)+';'+' echo -e "CP002120.1\t'+str(show_start)+'\t'+str(show_end)+'" > my_interval_temp.bed; bedtools intersect -wb -a my_interval_temp.bed -b /srv/scratch/z3460340/PhDfiles/termseq2019/all_Saureus_Termseq_results/config_and_annotation_files/jkd6008_gene_annotations_genbank.gff3 > my_gff_lines_in_range_tempfile.gff3;')
    gff_file_in_range = open('./my_gff_lines_in_range_tempfile.gff3','r')
    enum = enumerate(gff_file_in_range)

    dict_of_annotations = dict();

    name_operon = ""

    for i,gffline in enum:
        print("Running enum_Plus...\n")
        gffline = gffline.rstrip()
        gffFields = gffline.split('\t')
        gffStart = gffFields[1]
        gffEnd = gffFields[2]
        gffStrand = gffFields[9]

        plusStrand_non_empty = 0
        minusStrand_non_empty = 0

        ######## match_geneName_obj = re.match( r'gene=([^;]+?);',str(gffFields[11]) )
        geneName = "undef"

        if (gffStart, gffEnd, gffStrand) in dict_of_annotations.values():
            print("Skipping duplicate CDS entry...\n")
            continue

        namefields=str(gffFields[11]).split(';')
        for string in namefields:
            matchobj = re.match(r'gene=(.*)',string)
            if (matchobj != None):
                geneName = str(matchobj.group(1))
                name_operon= name_operon+geneName+"-"
                if (gffStrand == '+'):
                    dict_of_annotations[geneName] = (gffStart,gffEnd,'+')
                    if (plusStrand_non_empty == 0):
                        plusStrand_non_empty = 1
                elif (gffStrand == '-'):
                    dict_of_annotations[geneName] = (gffStart,gffEnd,'-')
                    if (minusStrand_non_empty == 0):
                        minusStrand_non_empty = 1
                print("added item to dict_of_annotations . Item is "+str(dict_of_annotations[geneName])+"\n")

        if (geneName == "undef"):
            match_geneName_obj2 = re.match( r'ID=([^;]+?);',str(gffFields[11]) )
            if (match_geneName_obj2 != None):
                print("matched gene name is "+str(match_geneName_obj2)+" \n")
                geneName = match_geneName_obj2.group(1)
                name_operon= name_operon+geneName+"-"
                if (gffStrand == '+'):
                    dict_of_annotations[geneName] = (gffStart,gffEnd,'+')
                    if (plusStrand_non_empty == 0):
                        plusStrand_non_empty = 1
                elif (gffStrand == '-'):
                    dict_of_annotations[geneName] = (gffStart,gffEnd,'-')
                    if (minusStrand_non_empty == 0):
                        minusStrand_non_empty = 1
                print("added item to dict_of_annotations . Item is "+str(dict_of_annotations[geneName])+"\n")
            elif (match_geneName_obj2 == None):
                if (gffFields[11] != None):
                    print("Could not find gene name in line...\n"+str(gffFields[11])+"\n")
                    continue
                else:
                    print("Could not find gene name in GFF and GFF line not the right format (written for bedtools version 2.27.1)?\n")
                    continue

    print("Is my dictionary filled? dict_of_annotations. dictionary below... \n"+str(dict_of_annotations)+"\n")

    if (plusStrand_non_empty == 0):
        fig.delaxes(fig_ax4)  #### Bookmark. Does fig_ax5 still have the x-axis labels??
        matplot_plt.xticks(rotation=45)
        ##### fig_ax5.xticks(rotation=45)
    if (minusStrand_non_empty == 0):
        fig.delaxes(fig_ax5) #### Bookmark. Does fig_ax4 still have the x-axis labels??
        matplot_plt.xticks(rotation=45)
        #### fig_ax4.set_xticklabels( [range(show_start,show_end-250,250)] )
        #### matplot_plt.xticks(rotation=45)

    gff_file_in_range.close()
    ####### The gene label ribbons and text will be staggered vertically, do they do not overlab / hide each other.
    rect_box_Ycoord = -0.25
    labeltext_Y_coord = -0.25

    for annotItem in dict_of_annotations.items():
        print("Hi. The key,value in dict_of_annotations.items() loop is running....\n")
        print("Hi. The item is : "+str(annotItem[0])+":("+str(annotItem[1][0])+","+str(annotItem[1][1])+","+str(annotItem[1][2])+")\n")

        if ( int(annotItem[1][0]) not in range(show_start,show_end) ) and ( int(annotItem[1][0]) not in range(show_start,show_end) ):
            print("Gene out of range? Range is: "+str(annotItem[1][0])+" "+str(annotItem[1][1])+"\n")
            continue
        elif ( int(annotItem[1][0]) < min(show_end,show_start) ):
            annotItem[1][0] = show_start
        elif ( int(annotItem[1][1]) < min(show_end,show_start) ):
            annotItem[1][1] = show_start
        elif ( int(annotItem[1][0]) > max(show_end,show_start) ):
            annotItem[1][0] = show_end
        elif ( int(annotItem[1][1]) > max(show_end,show_start) ):
            annotItem[1][1] = show_end

        if (str(show_start) in annotItem[1]) and (str(show_end) in annotItem[1]):
            print("Found the weird rectangle that covers entire range of x-axis...\n");
            continue
        print("Debugging. The range (for drawing rectangle gene annotation) is: "+str(annotItem[1][0])+" "+str(annotItem[1][1])+"\n")

        rect_box_corner_X = min(int(annotItem[1][0]),int(annotItem[1][1]))
        gene_width = abs(int(annotItem[1][0])-int(annotItem[1][1]))

        if (gene_width == abs(show_end-show_start)):
            print("Found the weird rectangle that covers entire range of x-axis...\n");
            continue

        name_operon = name_operon[:-1] ######### Remove the final trailing dash (-) from operon name.
        print("Debugging, the name_operon variable is: "+str(name_operon)+"\n")

        fig_ax4.set_xlim(start-10,end+10)
        fig_ax5.set_xlim(start-10,end+10)
        fig_ax4.set_ylim(0,2)
        fig_ax5.set_ylim(0,2)
        # Untested code for hiding y-axis tick labels
        fig_ax4.set_yticklabels([])
        fig_ax5.set_yticklabels([])

        #### fig.suptitle("Transcriptional readthrough at "+ r"$\i{" + str(name_operon) + "}$"+" in "+str(fetch_condition)+" condition")
        fig.suptitle("Transcriptional readthrough in "+str(fetch_condition)+" condition")
        rect_box_Ycoord = rect_box_Ycoord + 0.25
        rect = matplot_patches.Rectangle((rect_box_corner_X,rect_box_Ycoord),gene_width,0.25,linewidth=1,edgecolor='#2e232c',facecolor='#f2cedd',clip_on=True, label='Label', alpha=0.65) ####### Won't display. clip_on True or False???
        #### Genes are usually about four characters long. Start writing at about 1/3 of the way into the gene length. Key is the gene name.
        ########## Making the gene labels shorter so that they will fit inside the annotation boxes. Omg this is so frustrating.
        ########## if (len(annotItem[0]) > 14):
        ##########     myGeneLabel = annotItem[0].replace("-","\n",1)
        ##########     myGeneLabel = annotItem[0].replace(":","\n",1)
        ########## else:
        myGeneLabel = annotItem[0]
        ##########    Don't know how to correct horizontal border overflows. Really can't be bothered right now. :(
        labeltext_X_coord = rect_box_corner_X ###### To recentre.... + gene_width/2
        labeltext_Y_coord = labeltext_Y_coord + 0.25

        if (annotItem[1][2] == "+"):
            fig_ax4.add_patch(rect)
            fig_ax4.text(labeltext_X_coord, labeltext_Y_coord, str(myGeneLabel), fontsize=8, style='italic')
        if (annotItem[1][2] == "-"):
            fig_ax5.add_patch(rect)
            fig_ax5.text(labeltext_X_coord, labeltext_Y_coord, str(myGeneLabel), fontsize=8, style='italic')



# fig.tight_layout(pad=2)
# fig.subplots_adjust(wspace=2.2)

# fig_ax4.annotate(,xy=(1, 1), xycoords='figure pixels')
fig.text(.5, .05, str(options.fig_annotation), ha='center')
print(f"Debug figure annotation: {str(options.fig_annotation)}\n")


matplot_plt.show()
matplot_plt.savefig('Final_drawing_'+str(output_name)+'_Seq_coverage_graph_interval_'+str(options.fetch_gene)+'_'+str(start)+'_to_'+str(end)+'_'+str(my_timestamp)+'.png', bbox_inches='tight')

sys.exit(0)
