#!/usr/bin/env perl -w
use strict;
use lib "/srv/scratch/z3460340/PhDfiles/termseq2020/scripts/" ;
use lib "/apps/perl/5.28.0/" ;
#use genes_scripts;


### my $oneFileOnly = $ARGV[0];

my $novo_file = $ARGV[0];
my $novoBasename = $ARGV[1];




#my @novo_file_paths = ("/srv/scratch/z3460340/PhDfiles/termseq2019/all_Saureus_Termseq_results/jkd6009_termseq_project/control_run1/intermediate/control_run1.novo","/srv/scratch/z3460340/PhDfiles/termseq2019/all_Saureus_Termseq_results/jkd6009_termseq_project/control_run2/intermediate/control_run2.novo")


open(NOVO,$novo_file) or die "can't open novoalign alignment file $novo_file\n";
open(OUTNOVO,'>',"./${novoBasename}_strand_is_corrected.novo") or die "can't write new output novo file $novo_file ...\n";


	while(<NOVO>) {
		chomp;

		my $old_line = $_ ;

		if ( $old_line =~ /A short read aligner|Novocraft Technologie|License file|Licensed for evaluation|novoalign -d|Starting at|Interpreting input|Index Build Version|Hash length|Step size/ ) {
			print "Printed header line\n";
			print "${old_line}\n";
			# print OUTNOVO "${old_line}\n";
		}

		if ( $old_line =~ m/^(.+?\t.+?\t)(U|R|QC|NM|QL)\t(.+?\t)(F|R)(.*?)$/ ) {
			my $start_string = $1;
			my $map_type = $2;
			my $middle_string = $3;
			my $map_strand = $4;
			my $end_string = $5;

			next unless ($map_type eq 'U');

			if ($map_strand eq "F"){
	      my $novo_new_map_strand = "R";

				# TEST LINE
				#print "start_string is:\n${start_string}\nmap_type is:\n${map_type}\nmiddle_string is:${middle_string}\nmap_strand is ${map_strand}\nend_string is:${end_string}\n";
	      print OUTNOVO "${start_string}${map_type}\t${middle_string}${novo_new_map_strand}${end_string}\n";

			}	elsif ($map_strand eq "R"){

	      my $novo_new_map_strand = "F";

				# TEST LINE
				#print "start_string is:\n${start_string}\nmap_type is:\n${map_type}\nmiddle_string is:${middle_string}\nmap_strand is ${map_strand}\nend_string is:${end_string}\n";
	      print OUTNOVO "${start_string}${map_type}\t${middle_string}${novo_new_map_strand}${end_string}\n";

			} else {

				print "Unexpected NOVO value for strand, should be F or R... strand: $map_strand \n";
	      print "Offending line (map_strand not changed) is ....\n";
	      print $old_line;

		}


	} else { print "Did not match REGEX!"; }


} # END WHILE LOOP

		################# end edits


	#     	my @line = split(/\t/);
  #     	if (scalar @line < 9) {
	#     	print "number of fields less than nine\n";
	#     	}
	#     	my ($read_seq,$map_type,$map_pos,$map_strand) = @line[2,4,8,9];
	#     	my ($fieldZero, $fieldOne, $fieldThree, $fieldFive, $fieldSix, $fieldSeven, $fieldTen, $fieldEleven, $fieldTwelve) = @line[0,1,3,5,6,7,10,11,12];
	#     	print "The map_type variable equals $map_type";file_paths
	#     	print "The line being read in is $_";
	#     	next unless defined($map_strand);
	#     	    next unless ($map_type eq 'U');
	#     	    print "Apparently $map_strand is defined \n";
	#     	    print "Novo map strand (old) is $map_strand \n";
	#     	    if ($map_strand eq "F"){
	#     	      my $novo_new_map_strand = "R";
	#     	      print OUTNOVO "$fieldZero\t$fieldOne\t$read_seq\t$fieldThree\t$map_type\t$fieldFive\t$fieldSix\t$fieldSeven\t$map_pos\t$novo_new_map_strand\t$fieldTen\t$fieldEleven\t$fieldTwelve\n";
  	#     	  } elsif ($map_strand eq "R"){
	#     	      my $novo_new_map_strand = "F";
	#     	      print OUTNOVO "$fieldZero\t$fieldOne\t$read_seq\t$fieldThree\t$map_type\t$fieldFive\t$fieldSix\t$fieldSeven\t$map_pos\t$novo_new_map_strand\t$fieldTen\t$fieldEleven\t$fieldTwelve\n";}		else {
	#     	      print "Unexpected NOVO value for mapping or strand, should be F or R... $map_strand \n";
	#     	      print "Offending line (map_strand not changed) is ....\n";
	#     	      print "$fieldZero\t$fieldOne\t$read_seq\t$fieldThree\t$map_type\t$fieldFive\t$fieldSix\t$fieldSeven\t$map_pos\t$map_strand\t$fieldTen\t$fieldEleven\t$fieldTwelve\n";    }


	#     		}

close NOVO;
close OUTNOVO;


# Now change the strandedness in SAM files. Only if not using novo2sam.#!/usr/bin/env perl
# novo2sam.pl [-p] [-g $'@RG\tID:id\t...'] <aln.novo>
# Format of sam files. Field 0+1 = FLAG. this is 16 for reverse and 0 for forward strands....
# NS500339:337:HJNLWBGXB:1:11101:23512:1057       16      CP002120.1      521172  70      75M     *       0       0       CCGTAGGGTAGCCGAGATT
# TAGCTAACGACTTTGGTTACGTTCGTGAATTACGTTNGATGCTTAGGTGCACGGTT     AEEE6EEEEEEEEE<EEEEEEEEEEEAEEEEEEEEEEEEEEEEEE/EEEEEEEEE#EEEEEEEEEEEEEEAAAAA
#     PG:Z:novoalign  AS:i:6  UQ:i:6  NM:i:1  MD:Z:55C19

#my @sam_paths = ("/srv/scratch/z3460340/PhDfiles/termseq2019/all_Saureus_Termseq_results/jkd6009_termseq_project/alignment_samfiles_for_viewing/vancomycin_run2_trimmed_alignment.sam","/srv/scratch/z3460340/PhDfiles/termseq2019/all_Saureus_Termseq_results/jkd6009_termseq_project/alignment_samfiles_for_viewing/vancomycin_run1_trimmed_alignment.sam","/srv/scratch/z3460340/PhDfiles/termseq2019/all_Saureus_Termseq_results/jkd6009_termseq_project/alignment_samfiles_for_viewing/tigecycline_run4_trimmed_alignment.sam","/srv/scratch/z3460340/PhDfiles/termseq2019/all_Saureus_Termseq_results/jkd6009_termseq_project/alignment_samfiles_for_viewing/control_run2_trimmomatic_trimmed.sam","/srv/scratch/z3460340/PhDfiles/termseq2019/all_Saureus_Termseq_results/jkd6009_termseq_project/alignment_samfiles_for_viewing/control_run1_trimmomatic_trimmed.sam");

### if ($oneFileOnly eq "expFile"){
###    my $sam_exp_file = "/srv/scratch/z3460340/PhDfiles/termseq2019/all_Saureus_Termseq_results/jkd6009_termseq_project/strandedness_inverted_novoalign_July29/sam_experiment_description";
###    #my $exp_file = @ARGV; # Use the same experiment file as for map_primary_3ends.pl
###    my ($sam_file_paths_ref,$sam_sample_names_ref,$sam_exp_names_ref,$sam_exp_names_and_count_href) = &parse_exp_file($sam_exp_file,'sam');
###    my @sam_paths = @{$sam_file_paths_ref};
### }
### else { ... }




# Important, exp file may need to match RNA-seq read names and directory paths EXACTLY.
# Can I simplify all code to just do without parsing an exp file?
# Edits on May 7: ............
# So far the only .exp file, is
# end3    riboswitch_followups    LB_10min_1_2014-11-06_17_35_04  LB_1    LB_nov2014
# which fits the tab-separated format below:
# [end3] [project_dir] [run_dir] [sample_name] [experiment_name]

###           my $exp_file = "/srv/scratch/z3460340/PhDfiles/termseq2019/all_Saureus_Termseq_results/config_and_annotation_files/jkd6008_termseq_all_antibiotics_descr.exp";
###           my $exp_file = @ARGV; # Use the same experiment file as for map_primary_3ends.pl
###           my ($novo_file_paths_ref,$novo_sample_names_ref,$novo_exp_names_ref,$novo_exp_names_and_count_href) = &parse_exp_file($exp_file,"novo");
###           my @novo_file_paths = @{$novo_file_paths_ref};

###           sub parse_exp_file {
###           	my ($exp_file_path,$type) = @_;
###           	if (!$type){
###               print "Type error neither novo nor sam \n";
###               die;
###             }
###             next unless ($type=='sam' or $type=='novo');
###           	my @exp_project_dirs;
###           	my @exp_run_dirs;
###           	my @sample_names;
###           	my @exp_names;
###           	my %exp_names_and_count_hash;
###           	my %current_exp_hash;
###           	my $first_exp;
###           	my $c=0;
###           	open(EXP,$exp_file_path) or die "can't open experiment file $exp_file_path\n";
###           	while (my $sample_line = <EXP>) {
###           		chomp $sample_line;
###           		my ($lib_type,$project_dir,$run_dir,$sample_name,$exp_name) = split(/\t/,$sample_line); # end3    riboswitch_followups    LB_10min_1_2014-11-06_17_35_04  LB_1    LB_nov2014
###           		next unless ($lib_type eq 'end3');
###           		die "wrong number of inputs in 3end line should be: [end3] [project_dir] [run_dir] [sample_name] [experiment_name]...\n" if (
###           		!$lib_type or !$project_dir or !$run_dir or !$sample_name or !$exp_name);
###           		$first_exp = $exp_name if (!$first_exp);
###           		if ($exp_name ne $first_exp) {
###           			$c++;
###           			$first_exp = $exp_name;
###           		}
###           		push @exp_project_dirs,$project_dir;
###           		push @exp_run_dirs,$run_dir;
###           		push @sample_names,$sample_name;
###           		push @exp_names,$exp_name;
###           		$exp_names_and_count_hash{$c}++;
###           	}

###           	my @file_paths;
###             if ($type eq 'novo'){
###               foreach (my $i=0; $i<=$#exp_names; $i++) {
###             		my $exp_type = $exp_names[$i];
###             		my $file_path = "/srv/scratch/z3460340/PhDfiles/termseq2019/all_Saureus_Termseq_results/jkd6009_termseq_project/$exp_run_dirs[$i]/intermediate/wrong_orientation/$exp_run_dirs[$i].novo";
###                 push @file_paths,$file_path;
###             } } #end foreach # end if novo
###             if ($type eq 'sam'){
###               foreach (my $i=0; $i<=$#exp_names; $i++) {
###             		my $exp_type = $exp_names[$i];
###             		my $file_path = "/srv/scratch/z3460340/PhDfiles/termseq2019/all_Saureus_Termseq_results/srv/scratch/z3460340/PhDfiles/termseq2019/all_Saureus_Termseq_results/jkd6009_termseq_project/alignment_samfiles_for_viewing/{$exp_run_dirs[$i]}_trimmed_alignment.sam";
###                 push @file_paths,$file_path;
###               }   }#end foreach # end if sam


###           	return (\@file_paths,\@sample_names,\@exp_names,\%exp_names_and_count_hash);
###           }
