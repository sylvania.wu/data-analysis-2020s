#!/usr/bin/python

import os
from optparse import OptionParser  # Allows me to set custom program arguments and names.
import subprocess  # Allows me to open other programs from the Unix shell.
import re
import numpy as np
import pandas as pd
from datetime import datetime


parser = OptionParser()
parser.add_option("-s", "--tss",action="store",type="string",dest="TSS_gff")
parser.add_option("-g", "--genes",action="store",type="string",dest="gene_annotn")
parser.add_option("-m", "--maxUTRlen",action="store",type="string",dest="max5utrLen")
(options, args) = parser.parse_args()

# Updated and tested August 30, 2020 fiveFiftySevenPM
## Parallelisation at bash script level: Split the input TSS file by chunks of N lines and run each in PBS array job.
## Tested on the TSS file located at:
# /home/z3460340/directory_structure/data/TSS_more_sensitive_NOVAN_minus_strand_updated_Sept12.gff
# dRNAseqGff="/home/z3460340/directory_structure/data/TSS_more_sensitive_NOVAN_plus_strand_updated_Sept12.gff"
# /home/z3460340/directory_structure/data/TSS_more_sensitive_VAN_minus_strand_updated_Sept12.gff
# /home/z3460340/directory_structure/data/TSS_more_sensitive_VAN_plus_strand_updated_Sept12.gff

## Tested on the gene annotations at: geneAnnots="/srv/scratch/z3460340/PhDfiles/termseq2019/all_Saureus_Termseq_results/config_and_annotation_files/jkd6008_gene_annotations_genbank.gff3"
## python make_5utrs_from_dRNAseq_and_bedtools.py --maxUTRlen=300 --tss="${dRNAseqGff}" --genes="${geneAnnots}"

TSS_gff_table = pd.read_csv(options.TSS_gff,sep='\t',index_col=None,header=None,keep_default_na=False, na_values='')

genes_gff_table = pd.read_csv(options.gene_annotn,sep='\t',index_col=None,header=None,skiprows=[0,1],keep_default_na=False, na_values='')

strand_TSSs = TSS_gff_table[6][0]
TSS_peaks = TSS_gff_table[4]

if (strand_TSSs == '+'):
    # That's
    endsFiveUTRs = TSS_peaks + int(options.max5utrLen)  ## assumes that all the results in TSS input file from the same strand
elif (strand_TSSs == '-'):
    endsFiveUTRs = TSS_peaks - int(options.max5utrLen)
else:
    # print(f"Expected strand + or - in 6th field of TSS gff file. Not found: {strand_TSSs}. Quit.\n")
    quit()

my_working_dir=os.getcwd()

start_end_df = pd.concat( [TSS_peaks,endsFiveUTRs],axis=1)
start_end_df.columns = [0,1]

def make_gff_interval_from_dataframe(df_start,df_end,out_gff_path):
    os.system(f"printf 'CP002120.1\tdRNAseq\tpotential_5utr_interval_{str(options.max5utrLen)}_bp\t{str(df_start)}\t{str(df_end)}\t.\t{strand_TSSs}\t.\t.\n' >> {out_gff_path}")
    return "called"

dateTimeObj = datetime.now()
bdt_runs_timestamp = str(dateTimeObj.day)+"-"+str(dateTimeObj.month)+"-"+str(dateTimeObj.year)+"-"+str(dateTimeObj.hour)+":"+str(dateTimeObj.minute)


path_to_utrs = str(f"./search_intervals_5utr_from_tss_to_downstream_{str(options.max5utrLen)}_{bdt_runs_timestamp}_timestamp.gff")
res = start_end_df.apply(lambda Row: make_gff_interval_from_dataframe(df_start=min( int(Row[0]),int(Row[1]) ),df_end=max( int(Row[0]),int(Row[1]) ),out_gff_path=path_to_utrs),axis=1)
print(f"Result of call: {res}\n")

# Find overlaps strictly on the same strand between coding genes and 5'UTR intervals from TSS to max5utrlen bases downstream
# This should be equivalent to finding gene start codons within max5utrlen of a transcription start site.
os.system(f'module add bedtools/2.27.1; cd {my_working_dir}; bedtools intersect -a {path_to_utrs} -b {str(options.gene_annotn)} -wo -s > ./bedtools_intersect_result_5utrs_maxUTRlen_{str(options.max5utrLen)}_{bdt_runs_timestamp}_timestamp.txt;')

# Open up result of bedtools intersect with pandas read_csv. Make into gff. file using the same original NCBI genes. However, extend the gene by its 5'utr. If there are several transcription start sites which overlapped with a single gene, just choose the one with shortest 5'utr length.

bdt_intersect_result = pd.read_csv(f"./bedtools_intersect_result_5utrs_maxUTRlen_{str(options.max5utrLen)}_{bdt_runs_timestamp}_timestamp.txt",sep='\t',index_col=None,header=None,keep_default_na=False, na_values='')
bdt_intersect_result.columns = [x for x in range(0,19)]

print("Here is second row of bedtools intersect result:\n")
print(bdt_intersect_result.iloc[1])
# TSS interval limits = field[3] and field[4]. NCBI gene limits = field[12] and field[13]
'''Filter out all results where the TSS is contained INSIDE the coding sequence'''
df = bdt_intersect_result.copy()
dfNew = df[ (df[3] <= df[12]) & ( df[6]=='+')].append(df[(df[4] >= df[13]) & ( df[6]=='-') ],ignore_index=True) # For plus strand, filter when TSS=field[3] is greater than field[12]. For minus strand, filter when TSS=field[4] is less than field[13].
print("Here is second row of dataframe after boolean indexing to filter out overlaps where the TSS is inside the coding sequence, i.e. dfNew.iloc[1]:\n")
print(dfNew)
print(type(dfNew))

'''Choose one correct transcription start site for each gene'''
dfNewer = dfNew.copy().loc[dfNew.groupby(17)[18].idxmax()] # Then for each entry with identical value of field[17], get one with highest value of field[18]. This means there is the highest possible overlap between the set 5'utr interval at the gene. That means we picked the transcription start site closest to the gene.
print("Here is second row of dataframe after grouping by gene and choosing the closest TSS / smallest 5'UTR::\n")
print(dfNewer.iloc[1])

# This should be EMPTY: df[(df[6]=='+') & (df[15]=='-') ]
def make_gff_from_intersect_result(myTSS,myStopCodon,myStrand,geneDescripn,myOutfile):
    write_start = min(myTSS,myStopCodon)
    write_end = max(myTSS,myStopCodon)
    os.system(f"cd {my_working_dir}; printf 'CP002120.1\t5UTRWriteBedtoolsIntersect\ttranscript\t{str(write_start)}\t{str(write_end)}\t.\t{myStrand}\t.\t{geneDescripn}\n' >> {myOutfile}")
    return "called_to_write_extended_transcripts"

extended_transcripts_gff=f"./extended_transcripts_jkd6008_5utrs_ofMaxLen_{str(options.max5utrLen)}_{bdt_runs_timestamp}_timestamp.txt"

write_transcripts_res = dfNewer.apply(  lambda myRow: make_gff_from_intersect_result( myTSS=int(myRow[3]),myStopCodon=int(myRow[13]),geneDescripn=f"{str(myRow[17])}Extended to 5'utr with TSS at {str(myRow[3])} but original start of gene at {str(myRow[12])};",myStrand=str(myRow[6]),myOutfile=extended_transcripts_gff ) if (str(myRow[6])=='+') else make_gff_from_intersect_result(myTSS=int(myRow[4]),myStopCodon=int(myRow[12]),geneDescripn=f"{str(myRow[17])}Extended to 5'utr with TSS at {str(myRow[4])} but original start of gene at {str(myRow[12])};",myStrand=str(myRow[6]),myOutfile=extended_transcripts_gff )  )
print(f"Result of call: {write_transcripts_res}\n")


#         df.loc[df.groupby(17)[18].idxmax()].iloc[4]
#         0                                            CP002120.1
#         1                                               dRNAseq
#         2                        potential_5utr_interval_300_bp
#         3                                                 14145
#         4                                                 14445
#         5                                                     .
#         6                                                     +
#         7                                                     .
#         8                                                     .
#         9                                            CP002120.1
#         10                                              Genbank
#         11                                                  CDS
#         12                                                14197
#         13                                                14892
#         14                                                    .
#         15                                                    +
#         16                                                    0
#         17    ID=cds-ADL64098.1;Parent=gene-SAA6008_00010;Db...
#         18                                                  249



# Next script: Overlap (bedtools intersect) Igy's list of termination peaks with all transcripts (TSS to stop codon). If there are several overlapped termination peaks within the same gene, choose the two with highest read coverage. Make into effective "splice junctions" for exons in a BED file.
# i.e. test this on /srv/scratch/treelab/genome/saus/JKD6009/Tree_Lab_Data/jdk6009_gff_table_win8_sd4_cpm0_gw5.gff
