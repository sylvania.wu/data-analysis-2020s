#!/usr/bin/env Rscript


## ------------------------------------------------------------------------
if(!require(pacman)) {
  install.packages("pacman")
  libary(pacman)
}

p_load("GenomicRanges")
library("GenomicRanges")

p_load("rtracklayer")
library("rtracklayer")

p_load("GenomicAlignments")
library("GenomicAlignments")

p_load(tidyverse)

p_load("foreach")
library("foreach")

p_load("doParallel")
library("doParallel")
## ------------------------------------------------------------------------

## ------------------------------------------------------------------------
args = commandArgs(trailingOnly=TRUE)

# test if there is at least one argument: if not, return an error
if (length(args)==0) {
  stop("At least one argument must be supplied (input file).n", call.=FALSE)
} else if (length(args)==1) {
  # default output file
  args[2] = "out.txt"
}

## ------------------------------------------------------------------------
# Read in RNA-seq BAM files, annotation (.gff3 from NCBI), and name of RNA-seq alignment (BAM)
base_path <- args[1]
bamfile_name <- args[2]
genes_gff <- args[3]

gene_annotn_GRange <- import.gff(genes_gff)

bam_reads <- file.path(base_path, bamfile_name)
## ------------------------------------------------------------------------

##  FUNCTIONS
## ------------------------------------------------------------------------
get_GAlignments_from_bam <- function(bf_path) {
  #Enter into BamFile(), a character vector of BAM file paths
  bf <- BamFile(bf_path, yieldSize=100000)

  open(bf)
  chunks_accumulate <- NULL
  repeat {
  chunk <- readGAlignments(bf)
  if (length(chunk) == 0L)
    break
  if (is.null(chunks_accumulate)) {
    chunks_accumulate <- chunk
  } else {
    chunks_accumulate <- c(chunks_accumulate,chunk)
  } # END if, else
  } # END repeat()

  close(bf)
  return(chunks_accumulate)
} # END function

convert_GAL_to_GR <- function(fromGAlign){

  toGRange <- GRanges(fromGAlign)
    ## From GAlignment object, KEEP the CIGAR (bamfile quality)
    ## and qwitdth as metadata of the new GRanges object
  metadata <- cbind( cigar(fromGAlign),qwidth(fromGAlign) )
  colnames(metadata) <- cbind("cigar","qwidth")
  mcols(toGRange) <- metadata
  ## FINAL RESULT GRange
  return(toGRange)

}

get_hitsDF <- function(range1,range2){
  found <- hits_df2[between( hits_df2$queryHits, range1, range2),]
  c(found[1,1], found[1,2])
}
## ------------------------------------------------------------------------


## ------------------------------------------------------------------------

GAlign_object <- get_GAlignments_from_bam(bam_reads)
GRange_object <- convert_GAL_to_GR(GAlign_object)

# Split each gene into 5 sections (quintiles)
subSegments <- tile(gene_annotn_GRange, 5)

hits_object <- findOverlaps( query = GRange_no_antibiotic71, subject = subSegments, type = "within", minoverlap = 1, ignore.strand = F )

hits_df <- data.frame(hits_object)

hits_df2 <- hits_df %>% mutate(subjectHits = subjectHits%%5) %>% mutate( subjectHits = ifelse(subjectHits==0,5,subjectHits) )

registerDoParallel(4)

rnaseq_hits_in_genes <- data.frame("gene_id","start","end","strand","read_hit_pos","quintile")

### foreach() %dopar% to run on parallel cores
foreach ( d=iter(data.frame(gene_annotn_GRange[100:5000,]), by='row' )) %dopar% { 
  start <- as.numeric(d[2])
  end <- as.numeric(d[3])
  strand <- "*"
  
  if ( !(start==1 && end==2924344) ){
    coord_and_quantile <- get_hitsDF(start,end)
    
    new_row <- data.frame(d[10], start, end, strand, coord_and_quantile[1], coord_and_quantile[2])
    
    rnaseq_hits_in_genes <- rbind(rnaseq_hits_in_genes, new_row)
   
  } # END IF
  } # END FOREACH





## ------------------------------------------------------------------------
