#!/usr/bin/env python3

############# Last modified July 23, 2020 at 2:33 PM

''' find_total_covg_in_interval_samtools_pipe(): Count up all RNA-seq reads using samtools. Count all reads in short transcript (TSS to early termination site in 5'UTR) and all reads in long transcript (TSS to end of the CDS). '''
def find_total_covg_in_interval_samtools_pipe(start_interval, end_interval, search_strand, path_to_bamfile, path_to_dump_tempfile):
    try:
        os
    except NameError:
        import os
    try:
        re
    except NameError:
        import re
    ###### Previously, path_to_dump_tempfile = '/srv/scratch/z3460340/PhDfiles/termseq2019/all_Saureus_Termseq_results/jkd6009_termseq_project/results_map_putative_r5UTRs_August2019/September_map_putative_r5UTRs_python_script/test'
    os.system("module add samtools/1.9; samtools view -b "+str(path_to_bamfile)+" CP002120.1:"+str(start_interval)+"-"+str(end_interval)+" > "+str(path_to_dump_tempfile)+"/mytempfile1.bam; samtools reheader -P -i /srv/scratch/z3460340/PhDfiles/test_code_covg_graph_matplotlib/new_header "+str(path_to_dump_tempfile)+"/mytempfile1.bam | samtools view -h > "+str(path_to_dump_tempfile)+"/mytempfile1.sam")

    ### print("Debug message. Path to my current working directory (before change) is: "+str(path_to_dump_tempfile)+"\n")

    if ( path_to_dump_tempfile.endswith("/") == False ):
        path_to_dump_tempfile = path_to_dump_tempfile+"/"

    filename = str(path_to_dump_tempfile)+"mytempfile1.sam"

    print("Debug message. Path to my current working directory (after check and adding / to end) is: "+str(path_to_dump_tempfile)+"\n")

    if ("//" in filename):
        filename = filename.replace('//','/')


    sam_dataset = open(filename,'r')

    #### print("Debug message. Path to my temp samfile (after removing //) is: "+str(filename)+"\n")

    Tally_reads_plusStrand = 0
    Tally_reads_minusStrand = 0

    for line in sam_dataset:
        print("Start to read temp samfile....\n")
        if ("@HD" in line):
            print("read header line\n")
            continue
        if ("@PG" in line):
            print("read header line\n")
            continue
        if ("@SQ" in line):
            print("read header line\n")
            continue
        print("line in samtools_stdout is....\n")
        print(str(line))
        print("\n")
        if (type(line) == "int"):
            print("Error in reading line...")
            print(line)
            continue

        line = line.rstrip()
        fields = line.split('\t')


        if ( ( int(fields[1]) != 16) and ( int(fields[1]) != 0 )  ):
            print("unexpected SAM flag...\n")
            continue
        if ( (search_strand == '+') and (int(fields[1]) != 0)  ):
            print("strand + doesn't match FLAG field. FLAG is: "+str(fields[1])+"\n")
            continue
        if ( (search_strand == '-') and (int(fields[1]) != 16)  ):
            print("strand - doesn't match FLAG field. FLAG is: "+str(fields[1])+"\n")
            continue
        mapped_position = int(fields[3])

        ########### Insert changes September 1: Read CIGAR string of the SAM file and include entire length of the RNA read to the coverage graph (not just leftmost mapped position)
        cigar_string = str(fields[5])
        SM_regex_cig = r"\b([0-9]*)S([0-9]*)M\b"
        MS_regex_cig = r"\b([0-9]*)M([0-9]*)S\b"
        M_regex_cig = r"\b([0-9]*)M\b"
        SM_matchobj = re.search(SM_regex_cig,cigar_string)
        MS_matchobj = re.search(MS_regex_cig,cigar_string)
        M_matchobj = re.search(M_regex_cig,cigar_string)

        if ( (SM_matchobj) and (MS_matchobj==None) and (M_matchobj==None) ):
            print("Checking, the matched CIGAR string is "+str(cigar_string)+" \n")
            bases_to_clip = int(SM_matchobj.group(1))
            bases_to_read = int(SM_matchobj.group(2))
            ######### SM
            left_map_interval = mapped_position + bases_to_clip
            right_map_interval = mapped_position + bases_to_clip + bases_to_read

        elif ( (MS_matchobj) and (SM_matchobj==None) and (M_matchobj==None) ):
            print("Checking, the matched CIGAR string is "+str(cigar_string)+" \n")
            bases_to_read = int(MS_matchobj.group(1))
            bases_to_clip = int(MS_matchobj.group(2))
            ######### MS
            left_map_interval = mapped_position
            right_map_interval = mapped_position + bases_to_read

        elif ( (M_matchobj) and (SM_matchobj==None) and (MS_matchobj==None) ):
            print("Checking, the matched CIGAR string is "+str(cigar_string)+" \n")
            bases_to_read = int(M_matchobj.group(1))
            left_map_interval = mapped_position
            right_map_interval = mapped_position + bases_to_read
        else:
            print("Error, unexpected value in CIGAR string (is it an insertion or deletion?). CIGAR is: "+str(cigar_string)+"\n")
            continue
        ########## LATER, it would be nice to retrieve the sequence from left_map_interval to right_map_interval from the FASTA genome and compare it to the SEQ field of the SAM file. This would confirm this section is working properly.
        print("Counted one read. Leftmost mapped position is "+str(left_map_interval)+" and rightmost mapped position is "+str(right_map_interval)+" \n")

        if ( int(fields[1]) == 0 ):
            for base_covered in range(left_map_interval, right_map_interval):
                print("Counting RNA-seq coerage. Looping through base_covered in my left_map_interval, right_map_interval (plus strand):"+str(left_map_interval)+", "+str(right_map_interval)+"\n")
                print("base is: "+str(base_covered)+"\n")
                Tally_reads_plusStrand += 1  ####### Here I don't care exactly WHERE the read is mapped. I just add one point for every time a base is covered by an RNA-seq read. The reads have different lenghts, so for longer reads more points are added to the tally
        elif ( int(fields[1]) == 16 ):
            for base_covered in range(left_map_interval, right_map_interval):
                print("Counting RNA-seq coerage. Looping through base_covered in my left_map_interval, right_map_interval (minus strand):"+str(left_map_interval)+", "+str(right_map_interval)+"\n")
                print("base is: "+str(base_covered)+"\n")
                Tally_reads_minusStrand += 1
        else:
            print("Strand unexpected? samfile line fields[1]: "+str(fields[1])+"\n" )
            continue
        ############################ END for line in sam_dataset

    ### Keeping tempfiles to help debugging. Remember to delete later.
    os.system("rm "+str(path_to_dump_tempfile)+"/mytempfile1.sam")
    os.system("rm "+str(path_to_dump_tempfile)+"/mytempfile1.bam")
    sam_dataset.close()

    #### Declared the original dicts before for loop through lines in temp samfile.

    if (search_strand == "+"):
        if (Tally_reads_plusStrand == 0):
            print("Warning no RNA-seq reads found on plus strand in interval "+str(start_interval)+" to "+str(end_interval)+"\n")
        print("Counted total reads on plus strand for the interval "+str(start_interval)+" to "+str(end_interval)+", tally was: "+str(Tally_reads_plusStrand)+"\n")
        return Tally_reads_plusStrand
    if (search_strand == "-"):
        if (Tally_reads_minusStrand == 0):
            print("Warning no RNA-seq reads found on minus strand in interval "+str(start_interval)+" to "+str(end_interval)+"\n")
        print("Counted total reads on minus strand for the interval "+str(start_interval)+" to "+str(end_interval)+", tally was: "+str(Tally_reads_plusStrand)+"\n")
        return Tally_reads_minusStrand


''' Convert to RPKM (of RNA-seq total reads). '''
######### Must pass int arguments. Haven't decided how to handle TypeError.
def convert_to_RPKM(rawReadCount, windowLen, totalReadsinRun):
    if (windowLen == 0):
        print("Error can't divide by zero in convert_to_RPKM(). windowLen = 0\n")
        return 0
    if (totalReadsinRun == 0):
        print("Error can't divide by zero in convert_to_RPKM(). totalReadsinRun = 0\n")
        return 0
    RPKM = rawReadCount/(  (windowLen/1000)*(totalReadsinRun/1000000)  )
    return RPKM



def find_total_covg_in_interval_from_bedgraph(bedgraph_data, start_window, end_window):
    ##    The input graph file was made with this kind of command. -scale means the data is already normalised
    ## bedtools genomecov -ibam NA18152.bam -g my.genome -d -scale 10
    ## Reports at every position
    ## chr1  6  0
    ## chr1  7  0
    try:
        np
    except NameError:
        import numpy as np
    try:
        pd
    except NameError:
        import pandas as pd

    bedgraph_df = bedgraph_data

    x = bedgraph_df.loc[start_window:end_window]["depth"]
    depth_covg_in_window = int(np.add.reduce(x).iloc[0])

    return depth_covg_in_window
# find_total_covg_in_interval_from_bedgraph()
# From earlier file reading. Graph of normalised RNA-seq depth values, and must be indexed by genome position:
# bedgraph_df = pd.read_csv(bedgraph_path,sep="\t",index_col=False,header=None,keep_default_na=False, na_values='')
         ## use keep_default_na so that zero-read-depth won't be read as NA in the table.
# bedgraph_df.columns = ["chrom","position","depth"]
# bedgraph_df.drop(labels=["chrom"],axis="columns",inplace=True)
# bedgraph_df = bedgraph_df.set_index("position")


def grep_gene_coords_from_GFF(nameToGrep, strandToGrep, gffFilePath):
    try:
        subprocess
    except NameError:
        import subprocess

    print("Debug. About to run grep to find GFF line for "+str(nameToGrep)+"\n")
    ### from subprocess import check_output
    ### runGrep = subprocess.check_output(['grep', str(nameToGrep), str(gffFilePath)])

    pipe = subprocess.Popen(['grep', str(nameToGrep), str(gffFilePath)], stdout=subprocess.PIPE)
    out, err = pipe.communicate()
    ### runGrep = subprocess.run(['grep', str(nameToGrep), str(gffFilePath)], stdout=subprocess.PIPE)
    print("Debug. Just ran grep to find GFF line for "+str(nameToGrep)+"\n")
    ### gffTempfile = runGrep.stdout
    print("Debug. The output of runGrep piped command (check_output) was ... \n")
    print(out)
    print("\n")
    ### myGFFlines = gffTempfile.split('\n')
    myDecodedLines = out.decode("utf-8")
    print("Debug. Lines decoded from byte (grep pipe) are... \n")
    print(myDecodedLines)
    print("\n")
    myGFFlines = myDecodedLines.split('\n')
    print("Debug. If I try to split gff lines by backslash-n character, I get...\n")
    print(myGFFlines)
    print("\n")
    altSplitGFFlines = myDecodedLines.split('CP002120.1')
    print("Debug. If I try to split gff lines by CP002120.1, I get...\n")
    print(altSplitGFFlines)
    print("\n")
    ## print("Debug. About to run grep to find GFF line for "+str(nameToGrep)+"\n")
    ## os.system("grep '"+str(nameToGrep)+"' "+str(gffFilePath)+" > ./tempfile_annotation_for_"+str(nameToGrep)+".txt")
    ## print("Debug. Just ran grep to find GFF line for "+str(nameToGrep)+"\n")
    ## gffTempfile = open("./tempfile_annotation_for_"+str(nameToGrep)+".txt",'r')
    ## for i,line in enumerate(gffTempfile):
    for line in myGFFlines:
        ## print("Debug. Line "+str(i)+" in GFF file from grep, for the gene "+str(nameToGrep)+". Line is.... \n")
        print("Debug. Line in GFF file from grep, for the gene "+str(nameToGrep)+". Line is.... \n")
        print(line)
        print("\n")
        gffFields = line.rstrip().split('\t')
        try:
            tempGFFstrand = gffFields[6]
        except IndexError:
            print("Error could not find strand in grep 'd GFF line for the gene "+str(nameToGrep)+" . Line is.... \n")
            print(line)
            print("\n...and the GFF fields are...\n")
            print(gffFields)
            print("\n...moving to the next grep 'd GFF line...\n")
            continue
        if (tempGFFstrand == strandToGrep):
            if ( ("gene="+str(nameToGrep) in gffFields[8]) or ("ID="+str(nameToGrep) in gffFields[8]) ):
                lower_coord = int(gffFields[3])
                higher_coord = int(gffFields[4])
                ### gffTempfile.close()
                print("Debug. grep_gene_coords_from_GFF() successfully found the coordinates for gene "+str(nameToGrep)+"\n")
                return (lower_coord, higher_coord)
    print("grep_gene_coords_from_GFF(): Finished enumerating GFF lines and and nothing found.\n")
    ####### gffTempfile.close()
    return (0,0)
