#!/usr/bin/env perl -w
use strict;
use lib "/srv/scratch/z3460340/PhDfiles/termseq2020/scripts/" ;
use lib "/apps/perl/5.28.0/" ;


#### Tested and worked on March 30, 2020. Sylvania.

my $samfile = $ARGV[0];
my $samBasename = $ARGV[1];

open(SAM,$samfile) or die "can't open novoalign alignment file (SAM) $samfile\n";
print "opened SAM file \n";
open(OUTSAM,'>',"./${samBasename}_strand_is_corrected_March2020.sam") or die "can't write new output samfile file ${samfile} ...\n";

open(UNMAPPED,'>',"./${samBasename}_unmapped_reads_March2020.sam") or die "can't write new output samfile file ${samfile} ...\n";


my $count = 0;
while(<SAM>) {
    chomp;

    my $old_line = $_;

    if ( $old_line =~ '@HD' && $count < 3) {
      $count = $count + 1;
      print OUTSAM "${old_line}\n" ;
      print "Printed header line:     $old_line\n";
      next;
    }
    if ( $old_line =~ '@PG' && $count < 3) {
      $count = $count + 1;
      print OUTSAM "${old_line}\n" ;
      print "Printed header line:     $old_line\n";
      next;
    }
    if ( $old_line =~ '@SQ' && $count < 3) {
      $count = $count + 1;
      print OUTSAM "${old_line}\n" ;
      print "Printed header line:     $old_line\n";
      next;
    }


    #my @line = split(/\t/);

    if ( $old_line =~ m/^(\S+?)\t(\d{1,2})\t(.+?)$/ ) {
      my $fieldZero = $1 ;
      my $old_map_strand = $2 ;
      my $rest_of_line = $3 ;

      print "Sam map strand (old) is $old_map_strand \n";

      if ($old_map_strand eq "16"){
        my $sam_new_map_strand = "0";
        print("Flipped strand plus to minus and writing...\n");
        print "$fieldZero\t$sam_new_map_strand\t$rest_of_line";
        print OUTSAM "$fieldZero\t$sam_new_map_strand\t$rest_of_line";

      } elsif ($old_map_strand eq "0"){
        my $sam_new_map_strand = "16";

        print("Flipped strand minus to plus and writing...\n");
        print "$fieldZero\t$sam_new_map_strand\t$rest_of_line";
        print OUTSAM "$fieldZero\t$sam_new_map_strand\t$rest_of_line";

      }  elsif ($old_map_strand eq "4"){

        print("SAM FLAG = 4, so read was unmapped. Writing to unmapped reads...\n");
        print UNMAPPED "${old_line}\n";

      } else {
        print "${old_line}\n";
        print OUTSAM "${old_line}\n";
      }   ######## End if block to test value of SAM Flag.


    } else { print "Error, did not match regex to sam line \n" ; } ######## End if block to check whether REGEX was matched.

    # my $number_of_fields = scalar @line;

    # next unless ($number_of_fields >= 15);

    # my ($fieldZero, $fieldTwo, $fieldThree, $fieldFour, $fieldFive, $fieldSix, $fieldSeven, $fieldEight, $fieldNine, $fieldTen, $fieldEleven, $fieldTwelve, $fieldThirtn, $fieldFourtn, $fieldFiftn) = @line[0,2,3,4,5,6,7,8,10,11,12,13,14,15];
    # my $old_map_strand = $line[1];

    #my $new_map_strand = ($map_strand eq '16') ? '+' : '-';
    # next unless defined($old_map_strand) ;
    #unless ($fieldFour eq 'U') {
    #  print("non-unique read, warning!\n")
    #};  ## Non-unique mapped strands are discarded? ONLY for NOVOALIGN NATIVE

} # end while <SAM>

print "Reached end of program run\n";
close OUTSAM;
close SAM;
close UNMAPPED;

##       if ( $count < 3 ) {  ####|| @line[0] eq '@HD' || @line[0] eq '@PG' || @line[0] eq '@SQ'
##      $count = $count + 1;
##      print OUTSAM $old_line ;
##      print "Printed header line:     $old_line\n";
##      next ;
##    }
