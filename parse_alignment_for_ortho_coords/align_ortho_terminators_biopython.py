#!/usr/bin/env python

import numpy as np
import pandas as pd
from optparse import OptionParser

from Bio import pairwise2
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC

# Last update July 3, 2020 at 3:26 PM.
# TO DO:

# Write unit test script (for python "nose") based on results of faked inputs. Have some max. acceptable deviations for the converted_coords and the aligned chunk starts and ends.
# Write aligner function for EMBOSS smith-waterman algorithm with default scoring. Compare results on faked inputs with unit test.

#-----    parse_alignment_for_ortho_coords_BioPython.py to be run something like this:

#  cd /srv/scratch/z3460340
#  module add python/intel-3.6.8
#  module load python/intel-3.6.8
#  . mypytho36env/bin/activate
#  module add python/intel-3.6.8
#  module load python/intel-3.6.8
#  export PATH=$PATH:"/srv/scratch/z3460340/PhDfiles/termseq2020/scripts/"

#  cd /srv/scratch/z3460340/PhDfiles/termseq2020/compare_with_predicted_terminators_hoon_etal2005/test_biopython_aligner_and_parser
#  alignment_script="../../scripts/align_ortho_terminators_biopython.py"
#  python $alignment_script --debug=1 --strain="N315" --df_term="fake_data_for_testing.tsv" > stdout_err_biopython_alignment_testing_script 2>&1


parser = OptionParser()

parser.add_option("-d","--debug",action="store",type="int",dest="debug")
parser.add_option("-s", "--strain",action="store",type="string",dest="from_strain")
parser.add_option("-T", "--df_term",action="store",type="string",dest="df_term")

(options, args) = parser.parse_args()


###----------------         Functions                ----------------###


def index_aligned_seq(aln_seq):
    count_non_gaps = -1
    for i,letr in enumerate(aln_seq):
        if (letr != "-"):
            count_non_gaps = count_non_gaps + 1
            yield count_non_gaps
        else:
            yield "-"

def reverse_complement_biopython(my_seq,alphab="DNA"):
    try:
        Seq('A')
    except NameError:
        from Bio.Seq import Seq
        from Bio.Alphabet import IUPAC
    if (alphab.upper() == "DNA"):
        bio_seq = Seq(my_seq, IUPAC.unambiguous_dna)
    elif (alphab.upper() == "RNA"):
        bio_seq = Seq(my_seq, IUPAC.unambiguous_rna)
    else:
        raise(TypeError,"Wrong string type in reverse_complement_biopython(). Sequence unchanged")
        return my_seq
    revcompl = bio_seq.reverse_complement()
    revcompl = str(revcompl)

    return revcompl

def run_emboss_aligner(querySeq,subjSeq,name_query,name_subj,strand_of_subj):
    if (strand_of_subj == "-"):
        subjSeq = reverse_complement_biopython(subjSeq,alphab="DNA")
    pass
    return "ran"


def run_pairwise_aligner(seq1,seq2,name_seq1,name_seq2,strand_of_gene,aligner_version="PairwiseAligner"):  ## for now strand_of_gene just means strand of seq2 (02/07/2020)

    if (strand_of_gene == "-"):
        seq2 = reverse_complement_biopython(seq2,alphab="DNA") ## EDIT here. reverse complement the terminator from Orthologous strain, but keep gene from JKD6008 the same (+) orientation. Cross check this with previous script.
    ##  if (seq2_strand == "-"):
    ##      seq2 = reverse_complement_biopython(seq2,alphab="DNA")

    try:
        #   pairwise2.align.localms("AT","AT")
        Align.PairwiseAligner()
    except NameError:
        from Bio import Align
        #   from Bio import SeqIO
    # alignments = pairwise2.align.localms(seq1,seq2)
    aligner = Align.PairwiseAligner()
    aligner.mode = 'local'

    aligner.match_score = 6
    aligner.mismatch_score = -3
    aligner.internal_open_gap_score = -5
    aligner.internal_extend_gap_score = -1
    aligner.left_open_gap_score = -6.5
    aligner.left_extend_gap_score = -1.3
    aligner.right_open_gap_score = -6.5
    aligner.right_extend_gap_score = -1.3
    aligner.target_end_gap_score = 0.0
    aligner.query_end_gap_score = 0.0

    report = open(f'./pairwise_alignment_between_{name_seq1}_and_{name_seq2}.txt','w')

    if options.debug==1:
        report.write(f'Set pairwise local aligner. The algorithm to used is {aligner.algorithm}, the mode is {aligner.mode}. match score is {aligner.match_score}, mismatch score is {aligner.mismatch_score}, internal open gap penalty is {aligner.internal_open_gap_score}, internal extend gap penalty is {aligner.internal_extend_gap_score}, left open gap penalty is {aligner.left_open_gap_score}, left extend gap penalty is {aligner.left_extend_gap_score}, right open gap penalty is {aligner.left_open_gap_score}, right extend gap penalty is {aligner.left_extend_gap_score} \n')

    alignments = aligner.align(seq1,seq2)
    alnd = alignments[0]

    ### for printing output to file only


    for num in [0,1,2,3] : ## ,alignments[1],alignments[2],alignments[3]
        try:
            alinm = alignments[num]
        except IndexError:
            report.write("Reached end of optimal alignments.\n")
            break

        a_score = alinm.score

        report.write(f'Finished alignment between {name_seq1} and {name_seq2} which scored {a_score}:\n')
        # report.write(pairwise2.format_alignment(*alignments[0]))
        report.write(format(alinm, "phylip"))
        report.write("\n")

    report.close()

    ## for return object of pairwise2 it's a tuple of (seqA, seqB, score, ..., ...)
    return [alnd,alnd.aligned]


def pairwise_alignment_and_parse_for_coords(*coords,seqA,seqB,seq_strand,genome_start,aln_method="PairwiseAligner",seqA_name, seqB_name):
    if (aln_method == "PairwiseAligner"):
        [ myPairwiseAl,aligned_chunk_indices ] = run_pairwise_aligner(seq1=seqA, seq2=seqB, name_seq1=seqA_name, name_seq2=seqB_name, strand_of_gene=seq_strand,aligner_version="PairwiseAligner")
        seqA_start = aligned_chunk_indices[0][0][0]
        seqA_end = aligned_chunk_indices[0][ len(aligned_chunk_indices[0])-1 ][1] -1
        seqB_start = aligned_chunk_indices[1][0][0]
        seqB_end = aligned_chunk_indices[1][ len(aligned_chunk_indices[1])-1 ][1] -1

        if abs(seqB_end-seqB_start) != abs(seqA_end-seqA_start):  ### NOTE should be the same
            print(f'PairwiseAligner: Warning, aligned chunk lengths expected to be equal but are not. length of seqB: {abs(seqB_end-seqB_start)}, length of seqA: {abs(seqA_end-seqB_start)}\n')
        indices1 = [ inx for inx in range(seqA_start,seqA_end) ]
        indices2 = [ idx for idx in range(seqB_start,seqB_end) ]


    if (aln_method == "pairwise2"):
        aligned_list = run_pairwise_aligner(seq1=seqA, seq2=seqB,strand_of_gene=seq_strand,aligner_version="pairwise2")
        alnA = aligned_list[0][0]
        alnB = aligned_list[0][1]
        # al2 is 'G-A-T'
        # index_aligned_seq(al2) is <generator object index_aligned_seq at 0x7f33335cae60>
        # list(idx1) is [0, '-', 1, '-', 2]
        idx1 = index_aligned_seq(al1)
        indices1 = list(idx1)
        idx2 = index_aligned_seq(al2)
        indices2 = list(idx2)

    coord_conversion = {k:v for k,v in zip(indices2,indices1)}

    ## Now searching by any coordinate of terminator in orthologous strain, will get coordinate in jkd6008 aligned sequence.
    ### keys of coord_conversion are seqB, which is positions of hairpin ref. points in terms of the orthologous strain (query)

    if (options.debug == 1):
        outfile = open(f'coord_conversion_between_{seqB_name}_and_{seqA_name}\n','w')
        for key,value in coord_conversion.items():
            outfile.write("coord in "+str(options.from_strain)+":"+str(key)+", coord in jkd6008: "+str(value)+"\n"+"type(key): "+str(type(key))+"\n")

    converted = pd.Series({})
    for coord in coords: ## there should be four coords
        if (options.debug == 1):
            print("coord is: "+str(coord)+"\n")
        try:
            if seq_strand == "+":
                converted = converted.append( pd.Series( int(coord_conversion[coord])+int(genome_start) ) )
            if seq_strand == "-":
                converted = converted.append( pd.Series( int(coord_conversion[  len(seqB) - coord  ])+int(genome_start) ) )
            ### keys of coord_conversion are seqB, which is positions of hairpin ref. points in terms of the orthologous strain (query)
            # NOTE does not depend on strand because genome_start and genome_end are taken from gff file, and always in increasing order.
        except KeyError:
            converted = converted.append(pd.Series("coord_off_limits"))

    ### start and end for terminator annotation, GFF.
    jkd_term_start = int(genome_start) + min( seqA_start,seqA_end )  ## should be the SAME as seqA_start
    jkd_term_end = int(genome_start) + max( seqA_start,seqA_end ) ## should be the SAME as seqA_end

    ## For test/debug only
    aligned_query_start = coord_conversion[ min(coord_conversion.keys()) ]
    aligned_query_end = coord_conversion[ max(coord_conversion.keys()) ]

    converted = converted.append( pd.Series(jkd_term_start) )
    converted = converted.append( pd.Series(jkd_term_end) )

    return converted

term_df = pd.read_csv(options.df_term,sep='\t',skiprows=1,header=None,index_col=0) #index_col=0

# Doesn't work leads to ValueError: cannot reindex from a duplicate axis: term_df.dropna(axis=0, how='any', inplace=True)

term_df.columns = [0,1,2,3,4,5,6,7,8,9,10,11,12,13]

if (options.debug == 1):
    print("term_df dataframe after numbering the columns...\n\n")
    print(str(term_df.iloc[1]))


## EDIT this
conv_coords_cols = term_df.apply(lambda Row: pairwise_alignment_and_parse_for_coords(int(Row[5]),int(Row[6]),int(Row[7]),int(Row[8]),seq_strand=str(Row[12]),seqA=str(Row[13]),seqB=str(Row[4]),genome_start=int(Row[10]),seqA_name=str(Row[9]), seqB_name=str(Row[0]) ) if ("NA" not in [str(Row[11]),str(Row[5]),str(Row[6]),str(Row[7]),str(Row[8])] ) else pd.Series(["NA","NA","NA","NA","NA","NA"],index=[0,0,0,0,0,0]),axis=1)


term_df = pd.concat( [term_df,conv_coords_cols],axis=1 )


if (options.debug == 1):
    print("term_df dataframe after pd.concat( [term_df,conv_coords_cols],axis=1 )...\n\n")
    print(str(term_df.head(n=3)))

term_df.columns = ['original_locus_tag', 'original_gene','predicted_terminator','free_energy','original_dna_seq','hairpin_base_left','hairpin_loop_left','hairpin_loop_right','hairpin_base_right','orthologous_locus_in_jkd6008','jkd6008_locus_start','jkd6008_locus_end','jkd6008_locus_strand','jkd6008_sequence','jkd_hairpin_base_left','jkd_hairpin_loop_left','jkd_hairpin_loop_right','jkd_hairpin_base_right','JKD_start_of_terminator','JKD_end_of_terminator']

term_df.to_csv('after_terminators_coord_conversion_predicted_terminators_in_'+str(options.from_strain)+'_orthologues_in_jkd6008.tsv',sep='\t')








#------------- NOTES -------------#
## The match parameters are:

# CODE  DESCRIPTION
# x     No parameters. Identical characters have score of 1, otherwise 0.
# m     A match score is the score of identical chars, otherwise mismatch score.
# d     A dictionary returns the score of any pair of characters.
# c     A callback function returns scores.
# The gap penalty parameters are:

# CODE  DESCRIPTION
# x     No gap penalties.
# s     Same open and extend gap penalties for both sequences.
# d     The sequences have different open and extend gap penalties.
# c     A callback function returns the gap penalties.
