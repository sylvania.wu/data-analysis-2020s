#!/usr/bin/env python
from nose import with_setup # optional
import pandas as pd
# import pytest
from align_ortho_terminators_biopython import run_pairwise_aligner
from align_ortho_terminators_biopython import pairwise_alignment_and_parse_for_coords

''' Follow this tutorial: '''
''' https://pythontesting.net/framework/nose/nose-introduction/?fbclid=IwAR2hGmJokfJy42VUNaj7im3sqaDDA4cKlN3ji5x2gLv_fQ8J9YCN1EvZ0Ag#example '''

'''
Test unnecessary_math with nose
To run tests   : nosetests    test_um_nose_fixtures.py
Verobse (-v)   : nosetests -v test_um_nose_fixtures.py
NoCapture (-s) : nosetests -s test_um_nose_fixtures.py
'''

# Misc. Notes.
#  assertEqual(arg1,arg2,msg=None)
#  nose.tools.make_decorator(func)
#  @pytest.mark.parametrize
#  pytest.fixture()

            #------------- METHOD 1 --------------#
class nestedGenomeGeneQuery(object):
    def __init__(self,genome_seq,gene_start,gene_end,query_start,query_end):
        self.genome = genome_seq
        self.gene_coords = (int(gene_start),int(gene_end))
        self.gene_seq = genome[ int(gene_coords[0]-1):int(gene_coords[1]-1) ]
        self.query_coords = { "query1":(int(query_start),int(query_end)) }
        self.query_seqs = { "query1":genome[int(query_coords[0]-1):int(query_coords[1]-1)] }

        df = pd.DataFrame(["fake_gene_name"])
        df[1] = pd.DataFrame([])
        df[2] = pd.DataFrame([])
        df[3] = pd.DataFrame([])
        df[4] = pd.DataFrame([])
        df[5] = pd.DataFrame([])
        df[6] = pd.DataFrame([])
        df[7] = pd.DataFrame([])
        df[8] = pd.DataFrame([])
        df[9] = pd.DataFrame([])
        df[10] = pd.DataFrame([])
        df[11] = pd.DataFrame([])
        df[12] = pd.DataFrame([])
        df[13] = pd.DataFrame([])
        self.input_testing_df = df

    def mutate_snps(self,num_mutations,inplace=False):
        pass
    def make_gaps(self,num_gaps,max_gap_len,inplace=False):
        pass
    def mark_special_posns(*posns,inplace=True):
        pass

def make_random_nested_genome_gene_query(genome_length,gene_length,query_length):
    try:
        random.randit(0,3)
    except NameError:
        import random
    base_letters = ["A","T","C","G"]
    made_genome = ""
    for i in range(0,genome_length-1):
        made_genome = made_genome + base_letters[random.randint(0,3)]

    start_of_gene = randint(0,genome_length -1 -gene_length)
    end_of_gene = start_of_gene + gene_length - 1

    start_of_query = randint(start_of_gene,end_of_gene -query_length)
    end_of_query = start_of_query + query_length - 1

    randomNested = nestedGenomeGeneQuery(genome_seq=made_genome,gene_start=start_of_gene,gene_end=end_of_gene,query_start=start_of_query,query_end=end_of_query)

    return randomNested

class TestAligner:
    # @classmethod
    # def setup_class(cls):

    def setup(self):
        nested_genome_gene_query = make_random_nested_genome_gene_query(2000,500,25)
        self.genome_gene_query = nested_genome_gene_query
        self.input_data = nested_genome_gene_query.input_testing_df
        print(f'Ran setup() before any methods. self.input_data is now:\n')
        for i in range(0, 13):
            print(f'df[{i}] is {self.input_data.iloc[i]}\n')
    # def teardown(self):
    #     print("Ran teardown() before any methods")

    def test_positions_within_query(self):
        my_input_data = self.input_data
        #output data is six-column dataframe. 1 = , 2 = 3 = 4 = 5 = 6 =
        output_data = my_input_data.apply(lambda Row: pairwise_alignment_and_parse_for_coords(int(Row[5]),int(Row[6]),int(Row[7]),int(Row[8]),seq_strand=str(Row[12]),seqA=str(Row[13]),seqB=str(Row[4]),genome_start=int(Row[10]),seqA_name=str(Row[9]), seqB_name=str(Row[0]) ) if ("NA" not in [str(Row[11]),str(Row[5]),str(Row[6]),str(Row[7]),str(Row[8])] ) else pd.Series(["NA","NA","NA","NA","NA","NA"],index=[0,0,0,0,0,0]),axis=1)
        for i in [0,1,2,3]:
            result_coords.append(output_data.iloc[i])
        for num in result_coords:
            assert ( num >= min(self.innermost_query[0],self.innermost_query[1]) )
            assert ( num <= max(self.innermost_query[0],self.innermost_query[1]) )

    def test_mutate_then_align(self):
        nestedGenomeGeneQueryInstance.mutate_snps(num_mutations=6)
        nestedGenomeGeneQueryInstance.make_gaps(num_gaps=3,max_gap_len=5)
        nestedGenomeGeneQueryInstance.mark_special_posns(left_hairpin_base,left_loop_base,right_loop_base,right_hairpin_base)


            #------------- METHOD 2 --------------#
            #  from nose import with_setup
            #  @with_setup(setup_func_read_input_seqs)
            #  def test_myfunction(func):
            #      arg1 = func()
            #      assertTrue(arg1,arg2,msg=None)
