#!/usr/bin/python

import os
from optparse import OptionParser
import subprocess
import re
import numpy as np
import pandas as pd
from datetime import datetime

# Updated October 27, 2020 at 9:29PM (see pybedtools use)

# Overlap (bedtools intersect) Igy's list of termination peaks with all transcripts (TSS to stop codon). If there are several overlapped termination peaks within the same gene, choose the two with highest read coverage. Make into effective "splice junctions" for exons in a BED file.
# i.e. test this on myTermPeaks="/srv/scratch/treelab/genome/saus/JKD6009/Tree_Lab_Data/jdk6009_gff_table_win8_sd4_cpm0_gw5.gff"

parser = OptionParser()
parser.add_option("-t", "--transcripts",action="store",type="string",dest="transcripts_gff")
parser.add_option("-T", "--termPeaks",action="store",type="string",dest="term_peaks_gff")
parser.add_option("-m", "--maxNumberExons",action="store",type="string",dest="max_num_exons")
# parser.add_option("-g", "--genes",action="store",type="string",dest="gene_annotn")

(options, args) = parser.parse_args()

transcripts_table = pd.read_csv(options.transcripts_gff,sep='\t',index_col=None,header=None,keep_default_na=False, na_values='')

termpeaks_table = pd.read_csv(options.term_peaks_gff,sep='\t',index_col=None,header=None,skiprows=[0,1],keep_default_na=False, na_values='')

strand_termpeaks = termpeaks_table[6][0]

def bdtools_intersect_termpeaks_with_transcripts(path_termpeaks,path_transcripts,get_Nlargest):
    return "called"
# Score of Term-seq peak height is in field[5] of the gff file in options.term_peaks_gff
def choose_highest_term_peaks_per_transcript_in_bdtools_result(bdtools_res_df,termpeak_gff_df):
    termpeak_gff_df.columns = ["termpeak_genomeName","termpeak_source","termpeak_annotType","termpeak_startPosn","termpeak_endPosn","covgScore","termpeak_strand","termpeak_blank","termpeak_description"]
    bdtools_res_df.columns = ["A","B","C","termpeak_startPosn","termpeak_endPosn","covgScore","termpeak_strand","D","E","F","G","H","transcript_startPosn","transcript_endPosn","I","transcript_strand","J","transcript_description","bdtools_num_bases_overlap"]

    # INNER MERGE: How many fields do I need to make this totally unambiguous?
    merged_df = pd.merge(how="inner",on=["termpeak_startPosn","termpeak_endPosn","covgScore","termpeak_strand","transcript_startPosn","transcript_endPosn","transcript_strand","transcript_description"])
    # Test this logic on a constructed table. It is meant to group lines of df by name of TRANSCRIPT and return lines where coverage score (termpeak_gff_df[5]) is the HIGHEST for each transcript.
    if (get_Nlargest==None):
        nlargest_covgScore_termpeaks = merged_df.copy().loc[merged_df.groupby("transcript_description")["covgScore"].idxmax()] # read the docs for numpy.idxmax()
    else:
        nlargest_covgScore_termpeaks = merged_df.copy()[(merged_df["covgScore"].isin(merged_df["covgScore"].nlargest(n=get_Nlargest)) ) ]

    return nlargest_covgScore_termpeaks


def make_BEDfile_with_exons_from_transcripts_and_termpeaks(myTSS,myStopCodon,myStrand,geneDescripn,myOutfile):
    ''' Edit from example code '''
    write_start = min(myTSS,myStopCodon)
    write_end = max(myTSS,myStopCodon)
    os.system(f"cd {my_working_dir}; printf 'CP002120.1\t5UTRWriteBedtoolsIntersect\ttranscript\t{str(write_start)}\t{str(write_end)}\t.\t{myStrand}\t.\t{geneDescripn}\n' >> {myOutfile}")
    return "called_to_write_extended_transcripts"











'''              Drafty draft, rewrite in OOP style and study this github for examples:       '''
# https://github.com/NICHD-BSPC/termseq-peaks/blob/master/peaklib/peaklib.py

import pandas as pd
import numpy as np
import pybedtools

# transcripts gff
# term-peaks gff

class GeneWithExons(object):
    def __init__(self, name, st_base, en_base, strand="+"):
        self.name = name
        self.strand = strand
        self.low_bound = min(int(st_base),int(en_base))
        self.up_bound = max(int(st_base),int(en_base))
        self.exons = {}

    def add_exon(self,start,end,ex_strand=self.strand):
        allow_lim = range( self.up_bound,self.low_bound+1 )
        if ( int(start) in allow_lim ) and ( int(end) in allow_lim ):
            st_exon = min(int(start),int(end))
            en_exon = max(int(start), int(end))
            self.exons.add( [ int(st_exon),int(en_exon),ex_strand ] )
        else:
            print("Exon not within gene limits.")


class PeakAnnotater(object):
    def __init__(self, intervals_file, peaks_file, strand="+"):
        ''' Class to find intersections between two sets of intervals (peaks/intervals) '''
        self.annot_intervs = intervals_file
        self.peaks = peaks_file
        self.strand = strand
        self.intervals_df = pd.read_table( self.annot_intervs, names=["genome","source","type","start","end","score","strand","NA","description"] )
        self.peaks_df = pd.read_table(self.peaks, names=["genome","source","type","start","end","score","strand","NA","description"])

    def inputs_to_bedtools_obj(self):
        self.bedtools_intervs = pybedtools.BedTool(self.annot_intervs)
        self.bedtools_peaks = pybedtools.BedTool(self.peaks)
        # Usage: a_and_b = a.intersect(b, wo=True)

    def intersect_peaks_in_intervals(self,woOpt=True):
        self.intersection = self.bedtools_peaks.intersect(self.bedtools_intervs,wo=woOpt)
        intersect_df = self.intersection.to_dataframe(names=["chromA","startA","endA","nameA","scoreA","strandA","chromB","startB","endB","nameB","scoreB","strandB","bs_overlap"])
        return intersect_df

    def intersection_to_pseudo_exons(self, max_divis):
        ''' Says that each interval can be divided by peak(s) up to max_divis times into so-called exons '''
        ''' The top peaks are chosen by score / height '''
        try:
            int_res = self.intersect_peaks_in_intervals()
        except AttributeError:
            print("Yet to run bedtools intersect on peaks and intervals.\n")

        int_res
        ''' Sort dataframe by name of gene (B in intersect) '''



            ### Dict of GeneWithExons objects, the key is self.name

        return exons_per_interv

    def exons_per_interv_to_bed(self, outf="./termseq_peaks_to_exons_per_interv.bed"):
        return























## 27/10/2020
## -------------- Figure this out: Use pybedtools filter() and groupby() functions to find highest term peak per transcript (gene) in bedtools intersect result

#annotGff=pybedtools.BedTool('/srv/scratch/z3460340/PhDfiles/termseq2019/all_Saureus_Termseq_results/config_and_annotation_files/jkd6008_gene_annotations_genbank.gff3')

# Annotation now has genes and 5'UTRs from dRNA-seq
annotGff=pybedtools.BedTool('/srv/scratch/z3460340/PhDfiles/termseq2020/compare_transcript_readthrough_ratios_linezolid_RNAseq_2020/Final_JKD6009_5UTR_annotations_dRNAseq_TEX_no_antibiotic.gff')

#tssGff=pybedtools.BedTool('newTSS.gff')


termPeaksGff=pybedtools.BedTool('/srv/scratch/z3460340/PhDfiles/termseq2020/compare_transcript_readthrough_ratios_new_RNAseq_data_2020/jkd6009_terminators_gff_table_win8_sd4_cpm0_gw5.gff')

inter=annotGff.intersect(termPeaksGff,wo=True)


def give_random_covg_score(a_bed_feature):
    newscore=int(random.random()*20)
    a_bed_feature[5]=str(newscore)
    return a_bed_feature


### Now for the intersection bedTool, sort by identity of the gene
### Now among all intersections of one gene, print highest scoring (field 5, function above with each() ).

Tfilter=test.filter(lambda f: "ID=cds-ADL66767.1" in str(f[8]))


#### Join the second half of each intersection line into one string, to reconstruct BedTool features.
#### Make the 6th column = Term-seq intersected position.
#### Make the 8th column = Term-seq intersected read depth.

def remove_spaces_in_attrs(bdt_feat,fields=[]):
    bdt_feat.attrs = str(bdt_feat[8]).replace(' ','_')

    if ( len(fields > 0) ):
        for (idx in fields):
            bdt_feat[idx] = str(bdt_feat[idx]).replace(' ','_')

    return bdt_feat

def merge_intersected_feats_posn_and_depth(feat,which="left"):
    feat[8] = str(feat[8]).replace(' ','_')
    feat[5] = str(feat[14])
    #print("changed score")
    feat[8] = str(feat[8]) + f'InternalTermPeak={str(feat[13])};'
    #print("changed attrs")
    # OR:     gene_feat.score = str(termSeq_feat.score)
    gene_feat = pybedtools.BedTool(' '.join(feat[0:9]),from_string=True)
    #print("gene_feat after merging is:\n")
    #print(gene_feat)
    termSeq_feat = pybedtools.BedTool(' '.join(feat[9:18]),from_string=True)
    return gene_feat

    if (which == "left"):
        return gene_feat
    elif (which == "right"):
        return termSeq_feat
    else:
        raise TypeError("The argument 'which' in merge_intersected_feats_posn_and_depth takes vals left or right only.")
        return feat

res=map(merge_intersected_feats_posn_and_depth,inter)
print(list(res)[1])

# ''' Now stack together the BedTool objects into one BedTool (concat. vertically)'''

grouped = merged_inter.groupby(g=[4,5,7],c=6,o=['distinct**sort**num**desc'],full=True) ## Group by 1-indexed columns 4, 5, and 7, which are the start, end, and strand, of the original GFF genes. But summarise by the 1-indexed column 6, which is the Term-seq read depth?

sorted = merged_inter.sort(chrThenScoreD=True) ## This does the same thing as
''' After sorting by score, ONLY get the top two results by Term-seq coverage.'''

# ''' Results of merging Term-seq info with overlapped gene info is printed below: '''
# CP002120.1	dRNA-seq	five_prime_UTR	2715050	2715294	6.61	ID=rna-ada;Note=5UTR_of_ada,_supported_by_dRNA-seq_experiment_control_run1;InternalTermPeak=2715191;

# This is the BED format we aim for:
#chr7 gene_start gene_end gene_name score + cds_start cds_end always_zero_0 NUMBER_of_blocks exon1_length,exon2_length,exon3_length
#exon1_start_RE_gene_start,exon2_start_RE_gene_start,exon3_start_RE_gene_start


def yield_BED_exons_from_merged_inter(inter):
    import re
    chrm_name = str(inter[0])
    gene_start = str(inter[3])
    gene_end = str(inter[4])
    gene_strand = str(inter[6])
    depth_score = str(inter[5])
    descr = str(inter[8])

    match_obj = re.match('ID=(.?);',descr)
    gene_name = match_obj.group(1)
    match_obj2 = re.match('InternalTermPeak=(.?);',descr)
    termseq_start = match_obj2.group(1)
    fivePrime_end = [ min(int(gene_start),int(gene_end)) if (gene_strand == "+") else max(int(gene_start),int(gene_end)) ]
    exon_len = abs(termseq_start-fivePrime_end)

    num_blocks = 0
    block_lens = ""
    block_starts = ""

    for exon in exons:
        num_blocks = num_blocks + 1
        block_lens = block_lens+str(exon_len)+","
        block_starts = block_starts+str(termseq_start)+","

    block_lens = block_lens[:-1]
    block_starts = block_starts[:-1]

    bed_str = f'{chrm_name}\t{gene_start}\t{gene_end}\t{gene_name}\t{depth_score}\t{gene_strand}\t{gene_start}\t{gene_end}\t0\t{num_blocks}\t{block_lens}\t{block_starts}'

    bed_line = pybedtools.BedTool(bed_str, from_string=True)

    return bed_line

grouped.each(yield_BED_exons_from_merged_inter).saveas('term_peaks_as_exons.bed')










#---- Written and tested October 27, 2020 at 9:28. zfirst get your BedTools object in format of sortA, but grouped by larger transcript bounds and strand using groupby()


def sort_peak_by_score_per_gene(feat):
   scores = str(feat[9]).split(",")
   scores = [ float(s) for s in scores ]
   peaks = str(feat[10]).split(",")
   dict = zip(scores,peaks)
   print(dict)
   sorted_by_score = sorted(dict)
   feat[10] = str(",".join([ str(key)+":"+str(val) for key,val in sorted_by_score[0:2] ] ))
   return feat

sortB = sortA.each(sort_peak_by_score_per_gene)
print(sortB)

print(sortA)
# CP002120.1	Genbank	CDS	1212643	1213260	2.0	+	1212662	ID=cds-ADL65192.1;Parent=gene-SAA6008_01146;Dbxref=NCBI_GP:ADL65192.1;Name=ADL65192.1;gbkey=CDS;locus_tag=SAA6008_01146;product=putative-cell-division-initiation-protein;protein_id=ADL65192.1;transl_table=11;TermPeak=1212646;	2.0,11.0,3.6	1212662,1212651,1212656
# CP002120.1	Genbank	gene	874908	877703	9.0	+	874917	ID=gene-SAA6008_00803;Name=SAA6008_00803;gbkey=Gene;gene_biotype=protein_coding;locus_tag=SAA6008_00803TermPeak=874955;	9.0,5.0	874917,874922

   
