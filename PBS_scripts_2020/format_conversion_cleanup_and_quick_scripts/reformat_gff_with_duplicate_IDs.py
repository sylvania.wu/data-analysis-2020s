#!/usr/bin/env python3
import sys
import re

''' If there are consecutive gff3 entries with the same ID (ID=blah_my_gene; in the last field), then this script with number every duplicate entry blah_my_gene_repNum2, _repNum3, etc... NOTE this only works if all duplicates are grouped consecutively '''

gff_file_path = sys.argv[1]


gff_file = open(gff_file_path,'r')
new_gff = open(f"Rewrite_{gff_file_path}",'w')

enum_gff = enumerate(gff_file)

last_ID="no_gene_to_start"

for i,line in enum_gff:
    finder=re.findall(r"ID=([^;\s]+?);",line)
    if ( finder != [] ):
        my_ID = finder[0]
    else:
        print(f"REGEX not found for line number {i}. Original line written\n")
        new_gff.write(line)
        # last_ID = "no_gene_to_start" ### RESET the last ID for next loop iteration to compare against
        continue

    if ( my_ID == last_ID.split("_repNum")[0] ):
        if (len(last_ID.split("_repNum")) > 1):
            last_index = int(last_ID.split("_repNum")[1])
        else:
            last_index = 1
        repIndex = last_index + 1

        ID_rewrite = my_ID+"_repNum"+str(repIndex)

        line_rewrite = line.replace(f"ID={my_ID};",f"ID={ID_rewrite};")
        new_gff.write(line_rewrite)
        last_ID = ID_rewrite     ### RESET the last ID for next loop iteration to compare against
    else:
        print("Original line written\n")
        new_gff.write(line)
        last_ID = my_ID  ### RESET the last ID for next loop iteration to compare against

gff_file.close()
new_gff.close()
