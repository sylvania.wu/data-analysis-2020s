#!/usr/bin/env python3
import sys
import re
import funcs_for_short_long_transcript_comparison as myFunctions

#Updated August 5, 2020 at 1:29pm.
# Usage
# python ../scripts/convert_format_bedtools_intersect_wo_to_table5utrs.py intersect_results_control_terminators_with_rnaseq_dmediati_van1_forward_strand_coverage.txt


# ORIGINAL LINE from bedtools intersect:
# CP002120.1      TermSeq terminator      37798   37818   0.58    -       .       ID=TermGroup14  CP002120.1      Genbank pseudogene  37349   38332   .       -       .       ID=gene-SAA6008_00031;Name=SAA6008_00031;Note=truncated replication protein for plasmid;gbkey=Gene;gene_biotype=pseudogene;locus_tag=SAA6008_00031;pseudo=true  21

# CONVERT to this format: field0=Gene_or_locus_tag, field1=TSS/dRNAseq, field2=terminator, field3=gene_stop_codon, field4=strand.

# Hard-coded for now
annotations = "/srv/scratch/z3460340/PhDfiles/termseq2019/all_Saureus_Termseq_results/config_and_annotation_files/jkd6008_gene_annotations_genbank.gff3"

BT_intersect_path = sys.argv[1]
BT_intersect_file = open(BT_intersect_path,'r')
enum_BT = enumerate(BT_intersect_file)

new_file = open(f"Input_Table_for_compare_transcript_rt_{BT_intersect_path}",'w')
new_file.write("Gene_or_locus_tag\tTSS\tterminator\tgene_stop_codon\tstrand\n")

for i,line in enum_BT:
    fields = line.split("\t")
    type_annot = fields[11]
    strand_annot = fields[6]
    tss_or_start_posn = fields[12] # field1 (tss or gene's start codon)
    terminator_posn = round( ( int(fields[3])+int(fields[4]) )/2 ) # field2 (terminator)

    #--------- For overlaps with 5'UTR
    if (type_annot == "five_prime_UTR"):
        #-----------------------------------------------------------
        finder=re.findall(r"Note=5UTR of ([^,\s]+?),",fields[17])
        if (finder != []):
            gene_of_annot_tss = finder[0]
            print(f"regex found gene_of_annot_tss: {gene_of_annot_tss}\n Rest of the regex match:\n")
            print(finder)
            print(f"\nand fields[17]:{fields[17]}")
        else:
            print("ERROR five_prime_UTR annotation but could not find gene of 5'utr (regex problem). Quit.\n")
            quit()
        #-----------------------------------------------------------

        (gene_lowerCoord, gene_higherCoord) = myFunctions.grep_gene_coords_from_GFF(gene_of_annot_tss,strand_annot,annotations)

        genes_stop_codon = max(int(gene_lowerCoord), int(gene_higherCoord)) # field3 (gene's stop codon)

    #--------- For overlaps with a gene annotation from NCBI, NOT with one of my 5'utrs
    else:
        finderTwo = re.findall(r"ID=([^;\s]+?);",fields[17])
        #-----------------------------------------------------------
        if (finderTwo != []):
            gene_of_annot_tss = finderTwo[0]
            print(f"regex found gene_of_annot_tss: {gene_of_annot_tss}\n Rest of the regex match:\n")
            print(finderTwo)
            print(f"\nand fields[17]:{fields[17]}")
        else:
            print(f"REGEX not found for line number {i}. Quit.\n")
            quit()
        #-----------------------------------------------------------
        genes_stop_codon = fields[13] # field3 (gene's stop codon)

    new_file.write(f"{gene_of_annot_tss}\t{tss_or_start_posn}\t{terminator_posn}\t{genes_stop_codon}\t{strand_annot}\n")
#--------------------------------------------------------------------------------------------------------------------------------

new_file.close()
BT_intersect_file.close()
