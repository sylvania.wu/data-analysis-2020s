#!/bin/bash
#PBS -N rnaseq_alignment_to_bamfile_strand_splitting_and_depth_graph_2020.pbs
#PBS -l select=1:ncpus=1:mem=60gb
#PBS -l walltime=10:00:00
#PBS -j oe
#PBS -m ae
#PBS -M sylvania.wu@gmail.com
#PBS -k oed
#PBS -J 1-3

## Last update October 19, 2020 at 2:11 PM
## Last submitted job: 784984[].kman.restech.unsw.edu.au
## NOTE running only PBS_ARRAY_INDEX 2 to 5. (next job run index 0 only)


## This is an array job with one "PBS-J" index per PAIR of files for each biological replicate
## (e.g. WT_V_3_1 and WT_V_3_2 are both from vancomycin-treated,
## biological replicate number 3, but the last number _1 and _2 indicate paired reads.
#     #1 is the i7 primer pair and #2 is the i5 primer pair. These details from Daniel Mediati, 2020.)
#    NOTE: In relation to the genome, which pair (R1 or R2) a read comes from does NOT definitely tell us
#  which genome strand it maps to. R1 may come from the genome plus or minus strand.
# R2 may come from the genome plus or minus strand.
# We are ONLY guaranteed that any given pair of reads is complementary to each other. Check it later.

function test_existing_folder_or_make {
  folderName="$1" ;
  if [[ -d $folderName ]]; then
    echo "folder exists" ;
  else
    mkdir $folderName ;
  fi ;
}
#

# Alternatively, this command #PBS -t 1-100%5
# submits an array with 100 jobs in it, but the system will only ever have 5 running at one time.
function run_if_below_max_files_else_sleep {
  maxFiles=$1 ;
  fileNamePattern1=$2;
  fileNamePattern1=$3;
  #okayToGo=0; ## In BASH, return value 0 means success and anything else means fail.

  myJobID=$( echo $PBS_JOBID | sed -e s/\.kman.*// );
  # OR ELSE TRY:     myJobID="${PBS_ARRAY_ID}[${PBS_ARRAY_INDEX}]"
  # timeUsed=$(qstat -a | grep z3460340 | grep $myJobID | head -n1 | cut -f26 -d ' ') ;  ## For single job it's qstat -a
  # timeUsed=$(qstat -t | grep z3460340 | grep $myJobID | head -n1 | cut -f4 -d ' ') ; ## For array job it's qstat -t
  timeUsed=0
  numberMatched=$(ls . | egrep ${fileNamePattern1} | wc -l)
  if [[ ${numberMatched} -gt 0 ]]; then
    
    :  ## Files with this pattern exist (at least one). Do nothing and proceed to the WHILE loop below.
  else
    local okayToGo="yes" ; ## NO files with this pattern exist, so our directory is not full (it is okay to run the next commands...)
    
    # echo "after checking file pattern ${fileNamePattern1}, status of okayToGo (1 means succeeed & move on to next command) is ${okayToGo}" ;
    echo "$okayToGo"
    return ;
  fi ;

  while [[ $timeUsed -lt 28800 ]]; do  ## While more than 2 hrs left of compute walltime.
    if [[ $(ls . | egrep ${fileNamePattern1} | wc -l) -gt $maxFiles ]] || [[ $(ls . | egrep ${fileNamePattern2} | wc -l) -gt $maxFiles ]]; then
      # echo "Pausing program because number of large files (${fileNamePattern1} or ${fileNamePattern2}) greater than ${maxFiles}...";
      numSeconds=$(($RANDOM % 900)) ;
      numSeconds=$(($numSeconds + 240)) ;
      timeUsed=$(($timeUsed + $numSeconds)) ; 
      sleep $[ ${numSeconds} ]s ;  ### Sleep between 4 and 19 mins
      # echo "Finished pause of 900 + ${numSeconds} seconds." ;
      # timeUsed=$(qstat -t | grep z3460340 | grep $myJobID | head -n1 | cut -f4 -d ' ') ;
    else local okayToGo="yes" ;  break;## return success, move on to next command.
      # run_if_below_max_files_else_sleep 2 "*sam" "*fq" && run_my_next_command;
    fi ;
  done

  echo $okayToGo ;
  return;
}

export PATH="$PATH:/home/z3460340/PhDfiles/termseq2020/scripts/"

cd /srv/scratch/z3460340/PhDfiles/termseq2020/
# mkdir alignment_vancomcyin_rnaseq_from_DanMediati
test_existing_folder_or_make "alignment_linezolid_rnaseq_Sylvania_2020"
cd alignment_linezolid_rnaseq_Sylvania_2020

##----------------      All Daniel's raw data files from RNA-seq
#---- Dan's data: gzip_fastq_vanco1_pairOne="/srv/scratch/treelab/DGM_190803_RNAseq_JKD6008_vanco/WT_V_1_1.fq.gz"
#---- Dan's data: gzip_fastq_vanco1_pairTwo="/srv/scratch/treelab/DGM_190803_RNAseq_JKD6008_vanco/WT_V_1_2.fq.gz"
#---- Dan's data: gzip_fastq_vanco2_pairOne="/srv/scratch/treelab/DGM_190803_RNAseq_JKD6008_vanco/WT_V_2_1.fq.gz"
#---- Dan's data: gzip_fastq_vanco2_pairTwo="/srv/scratch/treelab/DGM_190803_RNAseq_JKD6008_vanco/WT_V_2_2.fq.gz"
#---- Dan's data: gzip_fastq_vanco3_pairOne="/srv/scratch/treelab/DGM_190803_RNAseq_JKD6008_vanco/WT_V_3_1.fq.gz"
#---- Dan's data: gzip_fastq_vanco3_pairTwo="/srv/scratch/treelab/DGM_190803_RNAseq_JKD6008_vanco/WT_V_3_2.fq.gz"

gzip_fastq_linz1_pairOne="/srv/scratch/treelab/SW_Novogene_RNASeq_2020/X201SC20090987-Z01-F001/raw_data/SW_LINZ1/SW_LINZ1_1.fq.gz"
gzip_fastq_linz1_pairTwo="/srv/scratch/treelab/SW_Novogene_RNASeq_2020/X201SC20090987-Z01-F001/raw_data/SW_LINZ1/SW_LINZ1_2.fq.gz"
gzip_fastq_linz2_pairOne="/srv/scratch/treelab/SW_Novogene_RNASeq_2020/X201SC20090987-Z01-F001/raw_data/SW_LINZ2/SW_LINZ2_1.fq.gz"
gzip_fastq_linz2_pairTwo="/srv/scratch/treelab/SW_Novogene_RNASeq_2020/X201SC20090987-Z01-F001/raw_data/SW_LINZ2/SW_LINZ2_2.fq.gz"
gzip_fastq_linz3_pairOne="/srv/scratch/treelab/SW_Novogene_RNASeq_2020/X201SC20090987-Z01-F001/raw_data/SW_LINZ3/SW_LINZ3_1.fq.gz"
gzip_fastq_linz3_pairTwo="/srv/scratch/treelab/SW_Novogene_RNASeq_2020/X201SC20090987-Z01-F001/raw_data/SW_LINZ3/SW_LINZ3_2.fq.gz"
gzip_fastq_contr2_pairOne="/srv/scratch/treelab/SW_Novogene_RNASeq_2020/X201SC20090987-Z01-F001/raw_data/SW_CONT2/SW_CONT2_1.fq.gz"
gzip_fastq_contr2_pairTwo="/srv/scratch/treelab/SW_Novogene_RNASeq_2020/X201SC20090987-Z01-F001/raw_data/SW_CONT2/SW_CONT2_2.fq.gz"

pairOne_gzipped_files=( $gzip_fastq_linz1_pairOne $gzip_fastq_linz2_pairOne $gzip_fastq_linz3_pairOne $gzip_fastq_contr2_pairOne ) ;

pairTwo_gzipped_files=( $gzip_fastq_linz1_pairTwo $gzip_fastq_linz2_pairTwo $gzip_fastq_linz3_pairTwo $gzip_fastq_contr2_pairTwo ) ;

# gzip_fastq_contr1_pairOne="/srv/scratch/treelab/DGM_190803_RNAseq_JKD6008_vanco/WT_1_1.fq.gz"

# gzip_fastq_contr1_pairTwo="/srv/scratch/treelab/DGM_190803_RNAseq_JKD6008_vanco/WT_1_2.fq.gz"
#---- Dan's data: gzip_fastq_contr2_pairOne="/srv/scratch/treelab/DGM_190803_RNAseq_JKD6008_vanco/WT_2_1.fq.gz"
#---- Dan's data: gzip_fastq_contr2_pairTwo="/srv/scratch/treelab/DGM_190803_RNAseq_JKD6008_vanco/WT_2_2.fq.gz"
#---- Dan's data: gzip_fastq_contr3_pairOne="/srv/scratch/treelab/DGM_190803_RNAseq_JKD6008_vanco/WT_3_1.fq.gz"
#---- Dan's data: gzip_fastq_contr3_pairTwo="/srv/scratch/treelab/DGM_190803_RNAseq_JKD6008_vanco/WT_3_2.fq.gz"

##----------------      In each PBS array index, process one PAIR of R1 and R2 reads from a single SAMPLE (biological replicate + or - antibiotic)
#---- Dan's data: pairOne_gzipped_files=($gzip_fastq_vanco1_pairOne $gzip_fastq_vanco2_pairOne $gzip_fastq_vanco3_pairOne $gzip_fastq_contr1_pairOne $gzip_fastq_contr2_pairOne $gzip_fastq_contr3_pairOne ) ;
#---- Dan's data: pairTwo_gzipped_files=($gzip_fastq_vanco1_pairTwo $gzip_fastq_vanco2_pairTwo $gzip_fastq_vanco3_pairTwo $gzip_fastq_contr1_pairTwo $gzip_fastq_contr2_pairTwo $gzip_fastq_contr3_pairTwo) ;
my_sample_names=("rnaseq_SWu_linz1" "rnaseq_SWu_linz2" "rnaseq_SWu_linz3" "rnaseq_SWu_cont2") ;
mygzipPairOne=${pairOne_gzipped_files[$PBS_ARRAY_INDEX]}
mygzipPairTwo=${pairTwo_gzipped_files[$PBS_ARRAY_INDEX]}

mybasenamePairOne=$( basename $mygzipPairOne )
mybasenamePairTwo=$( basename $mygzipPairTwo )
##----------------      Name of the current fastQ file without .gz extension
myfastqPairOne=${mybasenamePairOne//\.gz/}
myfastqPairTwo=${mybasenamePairTwo//\.gz/}
##----------------      Unzip fastQ
[[ $(run_if_below_max_files_else_sleep 6 '*fq' '*fq') == "yes" ]] && echo "running gunzip..." && gunzip -c $mygzipPairOne > "./$myfastqPairOne" && gunzip -c $mygzipPairTwo > "./$myfastqPairTwo" ;

# p1_sample_names=("rnaseq_dmediati_van1_pairOne" "rnaseq_dmediati_van2_pairOne" "rnaseq_dmediati_van3_pairOne" "rnaseq_dmediati_cont1_pairOne" "rnaseq_dmediati_cont2_pairOne" "rnaseq_dmediati_cont3_pairOne")
# p2_sample_names=("rnaseq_dmediati_van1_pairTwo" "rnaseq_dmediati_van2_pairTwo" "rnaseq_dmediati_van3_pairTwo" "rnaseq_dmediati_cont1_pairTwo" "rnaseq_dmediati_cont2_pairTwo" "rnaseq_dmediati_cont3_pairTwo")

##----------------      Final NAME of the biological replicate and condition, WITHOUT distinguishing R1 and R2 reads.
samfileName=${my_sample_names[$PBS_ARRAY_INDEX]}

module add samtools/1.9 ;
module load samtools/1.9 ;
module add novocraft/3.04.06 ;
myGenomeIndex="/srv/scratch/z3460340/PhDfiles/termseq2019/all_Saureus_Termseq_results/ref_genome/jkd6008genomeINDEXED" ;

##----------------      PAIRED-END READ ALIGNMENT (novoalign)
[[ $(run_if_below_max_files_else_sleep 6 '*sam' '*sam') == "yes" ]] && echo "running novoalign..." && novoalign -d $myGenomeIndex -f $myfastqPairOne $myfastqPairTwo -o SAM > "${samfileName}.sam" ;
[[ -f "${samfileName}.sam" ]] && rm $myfastqPairOne && rm $myfastqPairTwo ;


##----------------      SAM to BAM file compression
samtools view -Sb "${samfileName}.sam" > "${samfileName}.bam" ;


##----------------      Get total read counts for normalisation later
myTotalReads=$( samtools view -c "${samfileName}.bam") ;
echo "Total reads in the sample ${samfileName} is ${myTotalReads}.";

CPMs=$( echo print 1000000/$myTotalReads | perl ) ;
CPMscale=$(printf "%.3f\n" "$CPMs") ;
echo "Counts per million scaling factor in the sample ${samfileName} is ${CPMscale}.";
## CPM = readsMappedToGene * 1/totalNumReads * 10^6

# Does NOT work because bash doesn't do floating-point arithmetic: CPMscale=(( 1000000/${myTotalReads} )) ;


##----------------      BAM file sorting and indexing (pair-one pair-two plus-strand minus-strand reads still mixed up)
samtools view -H "${samfileName}.sam" > "header_${samfileName}.sam" ;
samtools sort "${samfileName}.bam" > "${samfileName}_SORTED.bam" && rm "${samfileName}.bam";
samtools index "${samfileName}_SORTED.bam" && test_existing_folder_or_make "strand_specific_${samfileName}" && rm "${samfileName}.sam";


##----------------      SPLIT aligned paired-end reads into those on genomic plus and minus strands
##----------------      Shell script made public by Joseph Chao-Chung Kuo.
## Run like this: sh bam_split_paired_end.sh target.bam ../split_bam/
## Output will have same name except "_fwd.bam" for plus strand and "_rev.bam" for minus strand. Manually check against known genes, etc.
scriptSplitPairedBAMtoStrands="/srv/scratch/z3460340/PhDfiles/termseq2020/scripts/bam_split_paired_end.sh" ;
sh $scriptSplitPairedBAMtoStrands "${samfileName}_SORTED.bam" "./strand_specific_${samfileName}/" ;
# worked: sh $scriptSplitPairedBAMtoStrands rnaseq_dmediati_van1_SORTED.bam ./strand_specific_rnaseq_dmediati_van1/
module add bedtools/2.27.1 ;

###     BUT FIRST figure out which are the plus and minus strand mapped reads??
bedtools genomecov -ibam "./strand_specific_${samfileName}/${samfileName}_SORTED_fwd.bam" -d -scale $CPMscale > "./strand_specific_${samfileName}/${samfileName}_forward_strand_coverage.gr" ;
bedtools genomecov -ibam "./strand_specific_${samfileName}/${samfileName}_SORTED_rev.bam" -d -scale $CPMscale > "./strand_specific_${samfileName}/${samfileName}_reverse_strand_coverage.gr" ;




## Options for Read processing:
##    -f read1 read2     Filenames for the read sequences for Side 1 & 2.
##                   If only one file is specified then single end reads are processed.
##                   If two files are specified then the program will operate in paired end mode.
