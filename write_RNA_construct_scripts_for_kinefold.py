#!/usr/bin/python
import random
from optparse import OptionParser

## Last updated January 28, 2020 at 4:08 PM.

###### Expected outputs of Kinefold
##### The Kinefold output .e file is the input of makeplot to generate trace pictures
##### perl makeplot_example.pl example.e
##### and all of the .rnm etc. outputs of Kinefold are used by the RNAMOVIES program.
##### you need to add the libforms.so.0.89 file in the appropriate library directory, eg /usr/lib (or install RNAMOVIES sources from rnamovies-1.2-src.tar.gz)

parser = OptionParser()

parser.add_option("-p", "--output_path",action="store",type="string",dest="output_path")

# parser.add_option("-c", "--myConstruct",action="store",type="string",dest="myConstruct")

# parser.add_option("-s", "--mySequence",action="store",type="string",dest="mySequence")

parser.add_option("-f", "--requirementsFile",action="store",type="string",dest="requirementsFile")

parser.add_option("-m", "--modeTraceOrForce",action="store",type="string",dest="modeTraceOrForce")

parser.add_option("-e", "--minEnergy",action="store",type="float",dest="minEnergy")

parser.add_option("-t", "--timeLimit",action="store",type="float",dest="timeLimit")

parser.add_option("-k", "--pseudoKnots",action="store",type="int",dest="pseudoKnots")

parser.add_option("-n", "--entanglements",action="store",type="int",dest="entanglements")

parser.add_option("-r", "--randomise",action="store",type="int",dest="num_random_seeds")


(options, args) = parser.parse_args()

random_repeats = options.num_random_seeds

outputsPath = options.output_path

helix_minFreeEng = options.minEnergy
foldTime_msec = options.timeLimit
pseudoknots_allowed = options.pseudoKnots
entangles_allowed = options.entanglements
trace_or_force = options.modeTraceOrForce  ## Must be T or F

def write_kinefold_req_files(seqs_name, model_sequence, prandom_seed, trace_force_mode, path_to_out, trace_i, trace_j, trace_k, entangles, pseudo_knots, model_folding_time, helix_energy):
    kinefold_script = open("./kinefold_script_construct_"+str(seqs_name)+"_seed"+str(prandom_seed)+"_mode"+str(trace_force_mode)+".req",'w')

    kinefold_script.write(str(prandom_seed)+"\n")
    kinefold_script.write(str(path_to_out)+str(seqs_name)+".p\n")
    kinefold_script.write(str(path_to_out)+str(seqs_name)+".e\n")
    kinefold_script.write(str(path_to_out)+str(seqs_name)+".rnm\n")
    kinefold_script.write(str(path_to_out)+str(seqs_name)+".rnms\n")
    kinefold_script.write(str(path_to_out)+str(seqs_name)+".rnml\n")
    kinefold_script.write(str(path_to_out)+str(seqs_name)+".rnm2\n")
    kinefold_script.write(str(path_to_out)+str(seqs_name)+".dat\n")
    kinefold_script.write("0\n")  ### at this line, "0" means model RNA and "1" means model DNA folding
    kinefold_script.write(str(helix_minFreeEng)+"\n")
    kinefold_script.write("10000000\n")
    kinefold_script.write(str(model_folding_time)+"\n")
    kinefold_script.write(str(pseudo_knots)+"\n") ## pseudoknots   1=yes 0=no
    kinefold_script.write(str(entangles)+"\n") # # entanglements 1=yes 0=no
    kinefold_script.write("2 40\n") # simulation type: 1=renaturation; 2 20=cotrans. @ 20msec/nt  ###### I changed transcription rate to 25 nucleotides per second = 0.04 sec/nt = 40 msec/nt
    # kinefold_script.write("# IMPORTANT: add T i j k or F i j k options here \n")
    if (trace_or_force == "T") or (trace_or_force == "F"):
        if trace_or_force != "N":
            kinefold_script.write(str(trace_force_mode)+" "+str(trace_i)+" "+str(trace_j)+" "+str(trace_k)+"\n")

    kinefold_script.write(str(seqs_name)+"\n")
    kinefold_script.write(str(seqs_name)+".zip\n")
    kinefold_script.write("<SEQNAME>"+str(seqs_name)+"_"+str(prandom_seed)+"\n")
    kinefold_script.write("<BASE>"+str(seqs_name)+"\n")
    kinefold_script.write("<SEQUENCE>"+str(model_sequence)+"\n")
    kinefold_script.write("<ZIPFILE>"+str(seqs_name)+".zip\n")

    kinefold_script.close()

    output_dat_file = open("./kinefold_script_construct_"+str(seqs_name)+"_seed"+str(prandom_seed)+"_mode"+str(trace_force_mode)+".dat",'w')
    ## Or just the base name??
    ##  output_dat_file = open("./kinefold_script_construct_"+str(seqs_name)+".dat",'w')
    output_dat_file.write("< "+str(seqs_name)+"_"+str(prandom_seed)+"\n")
    output_dat_file.write(str(model_sequence)+"\n")

    output_dat_file.close()

    return

## Read from excel file / table of constructs"
## construct_name = options.myConstruct
## my_sequence = options.mySequence


###### Note that the sequence (and nothing else) must be in a separate file named construct_name.dat in the folder of outputsPath (options.output_path)


constructsFile = open(options.requirementsFile, 'r')

enumFile = enumerate(constructsFile)

for i,line in enumFile:
    lineFields = line.split('\t')
    if lineFields[0] == "seqName":
        continue
    construct_name = lineFields[0]
    my_sequence = lineFields[1]
    trace_start_i = lineFields[2]
    trace_end_j = lineFields[3]
    stem_length_k = lineFields[4]

    if (trace_or_force != "T") and (trace_or_force != "F") and (trace_or_force != "N"):
        trace_or_force = "T"
        print("Kinefold mode not properly specified, by default I set mode to Trace (T)\n")

    ## Python's pseudorandom integer generator random.randint(start_range, end_range)
    for repeat in range(1,random_repeats):
        myRandoSeed = random.randint(1,10000)
        write_kinefold_req_files(seqs_name=construct_name, model_sequence=my_sequence, prandom_seed=myRandoSeed, trace_force_mode=trace_or_force, path_to_out=outputsPath, trace_i=trace_start_i, trace_j=trace_end_j, trace_k=stem_length_k, entangles=0, pseudo_knots=1, model_folding_time=foldTime_msec, helix_energy=helix_minFreeEng)


print("Finished program run.\n")
