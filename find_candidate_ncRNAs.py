#!/usr/bin/python
from functions_used_in_find_5utrs import define_5prime_UTRs as define_intervals
from optparse import OptionParser
import re
import os
from datetime import datetime


##     First test run of this script (certainly will find bugs) Feb 11, 2020.
##     qsub -I -l select=1:ncpus=1:mem=10gb,walltime=4:00:00
##     export PATH="$PATH:/srv/scratch/z3460340/PhDfiles/termseq2020/find_candidate_ncRNAs/"
##     module add python/intel-3.6.8
##     test_term_input="/srv/scratch/z3460340/PhDfiles/termseq2020/find_candidate_ncRNAs/test_table_of_term_ends"
##     moreSensitive_TSS="/srv/scratch/z3460340/PhDfiles/termseq2020/find_candidate_ncRNAs/MasterTable_JDK6009_TSS_moreSensitive.tsv"
##     python find_candidate_ncRNAs.py --term_ends="$test_term_input" --tss_table="$moreSensitive_TSS" --run_bedtools="yes"

parser = OptionParser()





# Coverage graph of all Term-seq in a given interval, one strand only.
parser.add_option("-t", "--term_ends",action="store",type="string",dest="three_prime_posns")
parser.add_option("-T", "--tss_table",action="store",type="string",dest="tss_table")
parser.add_option("-b", "--run_bedtools",action="store",type="string",dest="run_bedtools")

(options, args) = parser.parse_args()

three_prime_posns = open(options.three_prime_posns,'r')

enum_term = enumerate(three_prime_posns)
plusStrand_3ends = dict()
minusStrand_3ends = dict()
for i,line in enum_term:
    isFirstLine = re.search(r"Start",line)
    if isFirstLine :
        print("Read first line, pass...")
        continue
    ## My input 3'-ends, from thresholding_algo()
    ## Term-seq peak found at: 2916956.0	Strand: +	Read depth: 53.0
    # Tuple of transcription start sites looks like (TSS_posn,TSS_strand,TSS_coverage)
    ## Dictionary of CDS start codons looks like { start_pos:gene_name }
    Match_read_depth = re.search(r"Read depth: ([0-9]*)\.[0-9]\n",line)
    try:
        read_depth = int(Match_read_depth.group(1))
    except AttributeError as E1:
        print(str(E1))
        print("\nRead depth not parsed check line input:\n"+str(line)+"\n")

    Match_termEnd_posn = re.search(r"Term-seq peak found at: ([0-9]*)\.[0-9]\t",line)
    try:
        termEnd_posn = int(Match_termEnd_posn.group(1))
    except AttributeError as E2:
        print(str(E2))
        print("\nTerm-seq peak position not parsed check line input:\n"+str(line)+"\n")

    Match_termEnd_strand = re.search(r"Strand: (.)\t",line)
    try:
        termEnd_strand = Match_termEnd_strand.group(1)
    except AttributeError as E3:
        print(str(E3))
        print("\nStrand not parsed check line input:\n"+str(line)+"\n")

    if termEnd_strand == "+":
        plusStrand_3ends[termEnd_posn] = read_depth
    elif termEnd_strand == "-":
        minusStrand_3ends[termEnd_posn] = read_depth
    else:
        print("Could not read correct strand from 3'-end table file for the position "+str(termEnd_posn)+".\n")

myTSS=[]
tss_table = open(options.tss_table,'r')
enum_tss = enumerate(tss_table)
for i, line in enum_tss:
    tss_line_fields = line.rstrip().split('\t')
    if "SuperPos" in tss_line_fields[0]:
        print("Read first line of TSS master table, continue...\n")
        continue
    # Tuple of transcription start sites looks like (TSS_posn,TSS_strand,TSS_coverage)
    TSS_posn = tss_line_fields[0]
    TSS_strand = tss_line_fields[1]
    # TSS_coverage = tss_line_fields[2]
    TSS_coverage = 300
    myTSS.append( (TSS_posn,TSS_strand,TSS_coverage) )
    ### Input tuple of transcription start sites looks like (TSS_posn,TSS_strand,TSS_coverage)
    ##  if (TSStuple[1] is '+'):
    print("TSS_posn,TSS_strand,TSS_coverage are...")
    print(TSS_posn,TSS_strand,TSS_coverage)
    print("\n")

tss_table.close()

#### my_5UTRs = dict()

# my_5UTRs[TSS] = ( end_of_5UTR,'+',gene_of_5UTR )
# Note that in re-use of this function gene == Term-seq coverage , and end_of_5UTR is simply a Term-seq significant peak

maybe_ncRNAs = define_intervals(plus_start_codons_dict=plusStrand_3ends, minus_start_codons_dict=minusStrand_3ends, TSS_posns_tuples=myTSS, max_length=1000)

dateTimeObj = datetime.now()

my_timestamp = str(dateTimeObj.day)+"-"+str(dateTimeObj.month)+"-"+str(dateTimeObj.year)+"-"+str(dateTimeObj.hour)+":"+str(dateTimeObj.minute)

aureus_gff = "/srv/scratch/z3460340/PhDfiles/termseq2019/all_Saureus_Termseq_results/config_and_annotation_files/jkd6008_gene_annotations_genbank.gff3"
output_filename = "./output_putative_ncRNAs_time_"+str(my_timestamp)+".gff"
output_intervals_gff = open(str(output_filename),'w')

for key_TSS in maybe_ncRNAs.keys():
    found_termEnd = maybe_ncRNAs[key_TSS][0]
    found_strand = maybe_ncRNAs[key_TSS][1]
    found_termCovg = maybe_ncRNAs[key_TSS][2]
    output_intervals_gff.write("CP002120.1\tTermseqAnddRNAseq\tncRNA\t"+str(key_TSS)+"\t"+str(found_termEnd)+"\t.\t"+str(found_strand)+"\t.\tID\n")


output_intervals_gff.close()
## Bedtools intersect -v	Only report those entries in A that have no overlap in B. Restricted by -f and -r.
## query (-a) is my output intervals or candidate ncRNAs

if options.run_bedtools == "yes":
    os.system("module add bedtools/2.27.1; bedtools intersect -wa -v -a "+str(output_filename)+"-b "+str(aureus_gff))

print("Finished program run.\n(Feb 11, 2020: I have temporarily set TSS_coverage to 300 so it won't be missed by the define_5prime_UTRs() function. This should not affect resutls, all input from master tables already confirmed by TSSPredator)\n")
