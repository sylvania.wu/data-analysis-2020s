#!/usr/bin/python
import os
import re
import pandas as pd
from optparse import OptionParser
from functions_used_in_find_5utrs import get_base_at_position as get_one_base

### Inputs: transTermHP_predictions, gff file for JKD6008, orthologue_table (rom aureowiki), genome of JKD6008 (FASTA)
### Do test runs in /srv/scratch/z3460340/PhDfiles/termseq2020/compare_with_predicted_terminators_hoon_etal2005/
# qsub -I -l select=1:ncpus=1:mem=9gb,walltime=5:00:00
# cd /srv/scratch/z3460340/PhDfiles/termseq2020/compare_with_predicted_terminators_hoon_etal2005/
# annot="/srv/scratch/z3460340/PhDfiles/termseq2019/all_Saureus_Termseq_results/config_and_annotation_files/jkd6008_gene_annotations_genbank.gff3"
# ortho_script="../scripts/Staph_orthologues_to_jkd6008_seqs.py"
# module add python/intel-3.6.8
# export PATH=$PATH:"/srv/scratch/z3460340/PhDfiles/termseq2020/scripts/"
# python $ortho_script --debug=1 --strain="N315" --orth="OrthologueTableAureoWiki.tsv" --term="Hoon2005_terminator_predictions_Firmicutes/first_100_lines_for_test_no_NAs_NC_002745.trms" --gff=$annot > stdout_stderr 2>&1
#
#
# ### Last updated June 10, 2020 at 4:42 pm.

parser = OptionParser()

parser.add_option("-g", "--gff",action="store",type="string",dest="annotations_file")
parser.add_option("-o", "--orth",action="store",type="string",dest="orthologue_table")
parser.add_option("-t", "--term",action="store",type="string",dest="transtermHP_predictions")
parser.add_option("-s", "--strain",action="store",type="string",dest="from_strain")
parser.add_option("-d","--debug",action="store",type="int",dest="debug")

(options, args) = parser.parse_args()


terms_filepath = options.transtermHP_predictions
terminators = open(terms_filepath,'r')

orthologues = open(options.orthologue_table,'r')
og_strain = options.from_strain

ref_annotation = options.annotations_file



############### functions ################
def match_orthologous_tag(locus_id):
    conversion_row = orthologues_no_NA[orthologues_no_NA[og_strain].str.match(locus_id)]

    if (conversion_row.empty):
        return "NA"
        # for whole row: pd.DataFrame(np.array(["NA","NA","NA","NA","NA","NA","NA"]).reshape(1,7))
    else:
        try:
            new_locus_id = conversion_row.iloc[-1]["JKD6008"]
            if (new_locus_id == 'missing'):
                return "NA"
        except IndexError:
            return "NA"
        return new_locus_id

#   old_locus_tag = terms_fields[0]
#   prediction_seq = terms_fields[2]

def get_gene_limits_from_gff_table(loc_tag):
    gff_row = gff_table[gff_table[8].str.contains('(ID=.{0,8}'+str(loc_tag)+'|Name='+str(loc_tag)+'|Parent=gene-'+str(loc_tag)+')')]
    # gff_table[3] is start of gene,  gff_table[4] is end of gene, gff_table[8] is description field
    if (gff_row.empty):
        return ("NA","NA","NA")
    else:
        gff_first_row = gff_row.iloc[0]
        return ( gff_first_row[3],gff_first_row[4],gff_first_row[6] )


def get_reverse_complement_seq(range_in_genome):
    return_seq = ""
    for pos in range_in_genomee:
        new_base = get_one_base(pos)
        if new_base.upper() == "A":
            new_base = "T"
        elif new_base.upper() == "T":
            new_base = "A"
        elif new_base.upper() == "C":
            new_base = "G"
        elif new_base.upper() == "G":
            new_base = "C"
        else:
            print("unexpected base after get_base_at_position()...")
            quit()
        return_seq = return_seq + new_base
    return return_seq


def get_gene_and_flanking_seq(term_df_row):
    if (options.debug == 1):
        print("Running get_gene_and_flanking_seq()\n")
        print("Row is: ")
        print(term_df_row)

    start_coord = term_df_row[10]
    end_coord = term_df_row[11]
    gene_strand = term_df_row[12]

    #### add conditions for NA?
    if (end_coord == "NA") or  (start_coord == "NA" ):
        return "NA"

    my_seq = ""
    plus_strand_range = range(    min(int(start_coord),int(end_coord))-100,max(int(start_coord),int(end_coord))+100    )
    for pos in plus_strand_range:
        new_base = get_one_base(pos).upper()
        my_seq = my_seq + new_base
    # EDIT uncomment when debugged.
    #if gene_strand == "+":
    #    plus_strand_range = range(    min(int(start_coord),int(end_coord))-100,max(int(start_coord),int(end_coord))+100    )
    #    for pos in plus_strand_range:
    #        new_base = get_one_base(pos).upper()
    #        my_seq = my_seq + new_base
    #elif gene_strand == "-":
    #    minus_strand_range = range(    max(int(start_coord),int(end_coord))+100,min(int(start_coord),int(end_coord))-100    )
    #    my_seq = get_reverse_complement_seq(minus_strand_range)

    #else:
    #    return "NA"

    print("Got sequence: "+str(my_seq)+"\n")
    return my_seq

## apply to one line of term_df ?
def write_ortho_seqs_and_align(df):

    if (options.debug == 1):
        print("write_ortho_seqs_and_align: Row is: \n")
        print(df)

    genes_name = df[9]
    orthologue_name = df[0]
    orthologue_seqn = df[4]
    jkd6008_strand = df[12]
    jkd6008_seqn = df[13]

    if (genes_name == "NA") or (orthologue_name == "NA") or (orthologue_seqn == "NA") or (jkd6008_seqn == "NA"):
        return "not_aligned"+str(orthologue_name)

    #  genes_name = df['orthologous_locus_in_jkd6008']  ## The [] operator will return a pandas Series object with same indexes as dataFrame
    #  orthologue_name = df['original_locus_tag']
    #  orthologue_seqn = df['original_dna_seq']
    #  jkd6008_seqn = df['jkd6008_sequence']

    # ['original_locus_tag', 'original_gene','predicted_terminator','free_energy','original_dna_seq','hairpin_base_left','hairpin_loop_left','hairpin_loop_right','hairpin_base_right','orthologous_locus_in_jkd6008','jkd6008_locus_start','jkd6008_locus_end','jkd6008_locus_strand']

    temp_refRNA = open("tempfile_region_jkd6008_"+str(genes_name)+".txt",'w')
    temp_termRNA = open("./tempfile_predicted_terminator_"+str(og_strain)+"_"+str(genes_name)+"orthologue"+str(orthologue_name)+".txt",'w')

    temp_refRNA.write(str(jkd6008_seqn))
    temp_termRNA.write(str(orthologue_seqn))

    temp_refRNA.close()
    temp_termRNA.close()

    if (options.debug == 1):
        print("Running emboss matcher....\n")

    if (str(jkd6008_strand) == "-"):  ## EDIT 10/06/2020: added -sreverse2 strand-reverse option to alignment call (matcher), for minus strand sequences.
        os.system("module add emboss/6.6.0; matcher -asequence tempfile_region_jkd6008_"+str(genes_name)+".txt -bsequence tempfile_predicted_terminator_"+str(og_strain)+"_"+str(genes_name)+"orthologue"+str(orthologue_name)+".txt -sreverse2 -outfile alignment_between_terminator_in_"+str(orthologue_name)+"and"+str(genes_name)+".txt -alternatives 2")
    else:
        os.system("module add emboss/6.6.0; matcher -asequence tempfile_region_jkd6008_"+str(genes_name)+".txt -bsequence tempfile_predicted_terminator_"+str(og_strain)+"_"+str(genes_name)+"orthologue"+str(orthologue_name)+".txt -outfile alignment_between_terminator_in_"+str(orthologue_name)+"and"+str(genes_name)+".txt -alternatives 2")

    return "aligned_"+str(genes_name)+"_"+str(orthologue_name)
############################################

orthologues_df = pd.read_csv(orthologues, index_col=0, sep="\t")
# orthologues_df = pd.read_csv('OrthologueTableAureoWiki.tsv', index_col=0, sep="\t")

# EDIT 10/6/2020: previously: orthologues_no_NA = orthologues_df.dropna()
orthologues_no_NA = orthologues_df.fillna('missing', inplace=True)


### Read in terminators found in S. aureus strains from Hoon et al., 2005 (.trms file):
### /srv/scratch/z3460340/PhDfiles/termseq2020/compare_with_predicted_terminators_hoon_etal2005/Hoon2005_terminator_predictions_Firmicutes/NC_002953.trms
term_df = pd.read_csv(terms_filepath,sep='\t',skiprows=1,header=None)

### Make a pure sequence column by removing the tags for left and right bases of the terminator hairpin
term_df[4] = term_df[2].apply(lambda text: str(text).replace("</left>","").replace("<left>","").replace("</right>","").replace("<right>","").replace("U","T") )

## Find positions of the left base of hairpin, left end of loop, right end of loop, right base of hairpin.
term_df[5] = term_df[2].apply(lambda seq: seq.replace("</left>","").replace("</right>","").replace("<right>","").find("<left>") if ("<left>" in seq) else "NA")
term_df[6] = term_df[2].apply(lambda seq: seq.replace("<left>","").replace("</right>","").replace("<right>","").find("</left>") if ("</left>" in seq) else "NA")
term_df[7] = term_df[2].apply(lambda seq: seq.replace("<left>","").replace("</left>","").replace("<right>","").find("</right>") if ("</right>" in seq) else "NA")
term_df[8] = term_df[2].apply(lambda seq: seq.replace("</right>","").replace("<left>","").replace("</left>","").find("<right>") if ("<right>" in seq) else "NA")

### Read locus tag from strain (og_strain = options.from_strain)
### Convert to orthologous locus tag from JKD6008 using orthologue table: OrthologueTableAureoWiki.tsv
### term_df[0] is locus tag in the other S. aureus strain

term_df[9] = term_df[0].apply(match_orthologous_tag)

gff_table = pd.read_csv(ref_annotation,sep='\t',skiprows=2,header=None)


gene_lim_cols = term_df[9].apply( lambda my_gene: pd.Series( get_gene_limits_from_gff_table(my_gene) ) )

gene_lim_cols.to_csv('gene_lim_cols.tsv',sep='\t')

term_df = pd.concat([term_df,gene_lim_cols],axis=1) ### put columns side-by-side (add columns with start (column 10) and end (column 11) and strand (column 12) of corresponding gene in JKD6008)

term_df.columns = [0,1,2,3,4,5,6,7,8,9,10,11,12]
term_df.to_csv('after_pd_concat_table_predicted_terminators_in_'+str(og_strain)+'_orthologues_in_jkd6008.tsv',sep='\t')

# Axis 0 will act on all the ROWS in each COLUMN. Axis 1 will act on all the COLUMNS in each ROW.

# sequence is extracted from jdk6008 genome.
jkd_seqs_col = term_df.apply(lambda Row: get_gene_and_flanking_seq(Row), axis=1,result_type='expand')
term_df = pd.concat([term_df,jkd_seqs_col],axis=1) # and now join jkd_seqs_col as column of term_df



# term_df.columns = ['original_locus_tag', 'original_gene','predicted_terminator','free_energy','original_dna_seq','hairpin_base_left','hairpin_loop_left','hairpin_loop_right','hairpin_base_right','orthologous_locus_in_jkd6008','jkd6008_locus_start','jkd6008_locus_end','jkd6008_locus_strand']
term_df.columns = [0,1,2,3,4,5,6,7,8,9,10,11,12,13]
term_df.to_csv('after_getting_jkd6008_sequences_predicted_terminators_in_'+str(og_strain)+'_orthologues_in_jkd6008.tsv',sep='\t')


alignment_result = term_df.apply(lambda row: write_ortho_seqs_and_align(row),axis=1) ## emboss matcher is run here.
alignment_result.to_csv('alignment_results_per_row.tsv',sep='\t')

term_df.to_csv('table_predicted_terminators_in_'+str(og_strain)+'_orthologues_in_jkd6008.tsv',sep='\t')
