#!/bin/bash
#PBS -N thresholding_algo_discrete_mode_median_ABS_deviation_JKD6008_March_2020.pbs
#PBS -l select=1:ncpus=10:mem=30gb
#PBS -l walltime=21:00:00
#PBS -j oe
#PBS -m ae
#PBS -M sylvania.wu@gmail.com
#PBS -J 0-39


## Interactive job: qsub -I -l select=1:ncpus=1:mem=10gb,walltime=5:00:00 ; 245487[].kman.restech.unsw.edu.au
## update scripts with changes to parse_depth_graph(  *baseLimits  ) ---> parse_depth_graph(  baseLimits=(tuple,tuple)  )
## enter commands below one-by-one and test with export VERBOSE_FUNCS="yes"




### First array job! March 12, 2020
## Testing just for the control run2, plus strand input: ........................
### Testing for all other conditions: ...........................................
### threshold-4, influence-0, and lag-4000 leads to way too many peaks being called.
### I tried with a  large lag (10000), threshold=6, and influence=0.05 since I am using data filtered to only 3'-end-bases of each read. As a result, there are many more long gaps with zero or very low read-count. On "normal" settings, the algorithm will call everything as a peak because it's significantly above zero.

### Much higher threshold standard deviations (median abs. deviation
function backup_to_gitlab_and_clean_tempfiles () {
  folder_to_clean=$1

  #### For S. aureus data, this is the path to my gitlab repo. Deposit results in here.
  #### /srv/scratch/z3460340/PhDfiles/termseq2020/data_2020_git_repository/data-analysis-2020s/Results_thresholding_3ends_smallLagSet_March3_2020

  if [ ! -d /srv/scratch/z3460340/PhDfiles/termseq2020/data_2020_git_repository/ ]; then
    mkdir /srv/scratch/z3460340/PhDfiles/termseq2020/data_2020_git_repository/ ]
  fi

  if [ ! -d /srv/scratch/z3460340/PhDfiles/termseq2020/data_2020_git_repository/data-analysis-2020s/ ]; then
    mkdir /srv/scratch/z3460340/PhDfiles/termseq2020/data_2020_git_repository/data-analysis-2020s/ ]
  fi

  if [ ! -d /srv/scratch/z3460340/PhDfiles/termseq2020/data_2020_git_repository/data-analysis-2020s/Results_thresholding_3ends_smallLagSet_March3_2020 ]; then
    mkdir /srv/scratch/z3460340/PhDfiles/termseq2020/data_2020_git_repository/data-analysis-2020s/Results_thresholding_3ends_smallLagSet_March3_2020 ]
  fi



  cp "${folder_to_clean}" /srv/scratch/z3460340/PhDfiles/termseq2020/data_2020_git_repository/data-analysis-2020s/Results_thresholding_3ends_smallLagSet_March3_2020

  cd /srv/scratch/z3460340/PhDfiles/termseq2020/data_2020_git_repository/data-analysis-2020s

  git config --global user.name "sylvania.wu"
  export GIT_ASKPASS="1809cloverKoo"

  git add .
  git commit -m "Adding contents of folder: ${folder_to_clean}"
  echo "First few lines of file were:" && head ./$(basename ${folder_to_clean})/Results*
  git pull origin master --verbose
  git checkout -b Results_thresholding_3ends_smallLagSet_March3_2020

  git_push_status="notyet"
  git push origin master --verbose && ( if [[ "$(git push origin master --porcelain)" == *"Done"* ]]; then echo "git push was successful!"; git_push_status="done"; fi )
  percent_space="$( df -k /srv/scratch/z3460340/ )"
  if [[ $percent_space =~ ([0-9][0-9])%[[:blank:]]*.srv ]] && [[ $percent_space -ge 94 ]] ; then
    echo "Disk space getting full (${BASH_REMATCH[1]})."
    if [[ $git_push_status == "done" ]] ; then
      echo "Removing ALL files from local to clear space...";
      rm -rvf /srv/scratch/z3460340/PhDfiles/termseq2020/data_2020_git_repository/data-analysis-2020s/* ;
    else
      echo "git_push_status not done, can't clear space. Next time try moving files to Sylvania's extra space on tree lab drive..."
    fi
  fi
  # cd $folder_to_clean
  # rm ./*

}

function make_folder_for_settings_and_run_find_peaks () {

  local script_var=$1
  local index_var=$2
  local dir_var=$3
  local threshold_var=$4
  local influence_var=$5
  local lag_var=$6
  local input_termseq_var=$7
  local other_settings_var=$8

  if [[ $input_termseq_var =~ (vancomycin\_run.|linezolid\_run.|tigecycline\_run.|control\_run.) ]] ;
        then
          input_termseq_basename="${BASH_REMATCH[1]}" ;
        elif [[ $input_termseq_var =~ (vanco\_run.|linz\_run.|tige\_run.|cont\_run.) ]]
        then
          input_termseq_basename="${BASH_REMATCH[1]}" ;
        elif [[ $input_termseq_var =~ (vanco[0-9]|linz[0-9]|tige[0-9]|cont[0-9]) ]]
        then
           input_termseq_basename="${BASH_REMATCH[1]}" ;
        else
          echo "Expected sample name NOT matched. Name is:" ;
          echo "${input_termseq_var}" ;
          echo "Abort this PBS job in wrapper script..." ;
          qdel ${PBS_JOBID} ;
          #export CONDITIONAL_KILL='yes' ;      #### NO need to set enviromental variable if PBS_JOBID works perfectly.
          #echo "CONDITIONAL_KILL='yes'" >> ~/.bashrc ;
  fi
  ## linz1 tige3 vanco1 cont2
  ## -z "$var" means returns true if a bash variable is unset or set to the empty string

  [[ $input_termseq_var =~ ([pPlLmMiInN]{2,3}us).*([sS]trand) ]] && input_strand_basename="${BASH_REMATCH[1]}_${BASH_REMATCH[2]}"


  local base_script_res=$(basename "$script_var")
  local script_basename="${base_script_res//\.py/_}"
  # Fix to error... ?
  # cannot create directory ‘/home/z3460340/benchmarking_data/test_find_all_threshold_peaks/subset_13_dummyPythonScript.py//./__threshold5_influence0_lag1000/’


  local nameInfluence="${influence_var//\./_}"
  local nameThreshold="${threshold_var//\./_}"

  # local top_dir="/home/z3460340/benchmarking_data/test_find_all_threshold_peaks"
  # local top_dir="/srv/scratch/z3460340/PhDfiles/termseq2019/benchmarking_data_Jan2020/Bsubtilis_benchmarking"
  local top_dir="/srv/scratch/z3460340/PhDfiles/termseq2020/thresholding_3end_peaks_JKD6008"

########## STOP. Rename with sample/condition too. (instead of "Feb 2020")
  if [ ! -d "${top_dir}/March12_2020_${input_termseq_basename}_${input_strand_basename}_subset_${index_var}_${script_basename}_threshold${nameThreshold}_influence${nameInfluence}_lag${lag_var}/" ] ; then
    mkdir ${top_dir}/March12_2020_${input_termseq_basename}_${input_strand_basename}_subset_${index_var}_${script_basename}_threshold${nameThreshold}_influence${nameInfluence}_lag${lag_var}/
  fi
    ##   Error in generation of folder name!
    ## Folder name ends in     threshold --strand='+' _influence--imageLimit=1 _lag--seqReadLength=0/’
  cd ${top_dir}/March12_2020_${input_termseq_basename}_${input_strand_basename}_subset_${index_var}_${script_basename}_threshold${nameThreshold}_influence${nameInfluence}_lag${lag_var}/

  python $script_var --threshold=${threshold_var} --influence=${influence_var} --lag=${lag_var} --coverageGraph=${input_termseq_var} $other_settings_var && echo "Python script ran successfully."

  scratch_space="$( df -k /srv/scratch/z3460340/ )"
  if [[ $scratch_space =~ ([0-9][0-9])%[[:blank:]]*.srv ]] && [[ $scratch_space -ge 95 ]] ; then
    echo "Scratch disk space getting full (${BASH_REMATCH[1]}), start gitLab backup.";
    backup_to_gitlab_and_clean_tempfiles ${top_dir}/March12_2020_${input_termseq_basename}_${input_strand_basename}_subset_${index_var}_${script_basename}_threshold${nameThreshold}_influence${nameInfluence}_lag${lag_var}/ ;
    #### Combine output files later, from gitLab.
    ### Sleep here to avoid Git backup clashes??
    sleep $((RANDOM % 240)) ;
  fi



  echo "Finished make_folder_for_settings_and_run_find_peaks() for threshold ${threshold_var}, influence ${influence_var}, lag ${lag_var}, input coverage graph ${input_termseq_var} and other settings:"
  echo "$other_settings_var";


}

interval_limit_file="/srv/scratch/z3460340/PhDfiles/termseq2020/thresholding_3end_peaks_JKD6008/intervals_jkd6008_overlap_10000bp.txt"
# interval_limit_file=$MY_VAR

# sed "${PBS_ARRAY_INDEX}q;d" gets the nth line ($PBS_ARRAY_INDEX) of the file my_interval_limits.txt
if [ "${PBS_ARRAY_INDEX}" -eq 0 ]
#if [[ "${PBS_ARRAY_INDEX}" -eq 0 ]]
then
  startInterval=$( head -1 $interval_limit_file | cut -d$'\t' -f1 )
  endInterval=$( head -1 $interval_limit_file | cut -d$'\t' -f2 )
else
  startInterval=$( sed "${PBS_ARRAY_INDEX}q;d" $interval_limit_file | cut -d$'\t' -f1 )
  endInterval=$( sed "${PBS_ARRAY_INDEX}q;d" $interval_limit_file | cut -d$'\t' -f2 )
fi


module add python/intel-3.6.8
export PATH="$PATH:/home/z3460340/benchmarking_data/scripts/"
export PATH="$PATH:/srv/scratch/z3460340/PhDfiles/termseq2019/practice_runs/my_termseq_scripts/"

# Do not print out lots of debugging lines from functions_used_in_find_5utrs.py script. To save on storage space.
export VERBOSE_FUNCS="no"


Staph_linz1_plusStrand_termseq_graph_file="/srv/scratch/z3460340/PhDfiles/termseq2020/callPeakWhere_minCovg_of_singleBaseSeqOnly_inBothReps_Feb2020/Staph_termseq_linz1_plus_strand_single_3end_base_only.gr"
Staph_linz1_minusStrand_termseq_graph_file="/srv/scratch/z3460340/PhDfiles/termseq2020/callPeakWhere_minCovg_of_singleBaseSeqOnly_inBothReps_Feb2020/Staph_termseq_linz1_minus_strand_single_3end_base_only.gr"

Staph_linz2_plusStrand_termseq_graph_file="/srv/scratch/z3460340/PhDfiles/termseq2020/callPeakWhere_minCovg_of_singleBaseSeqOnly_inBothReps_Feb2020/Staph_termseq_linz2_plus_strand_single_3end_base_only.gr"
Staph_linz2_minusStrand_termseq_graph_file="/srv/scratch/z3460340/PhDfiles/termseq2020/callPeakWhere_minCovg_of_singleBaseSeqOnly_inBothReps_Feb2020/Staph_termseq_linz2_minus_strand_single_3end_base_only.gr"

Staph_tig3_plusStrand_termseq_graph_file="/srv/scratch/z3460340/PhDfiles/termseq2020/callPeakWhere_minCovg_of_singleBaseSeqOnly_inBothReps_Feb2020/Staph_termseq_tige3_plus_strand_single_3end_base_only.gr"
Staph_tig3_minusStrand_termseq_graph_file="/srv/scratch/z3460340/PhDfiles/termseq2020/callPeakWhere_minCovg_of_singleBaseSeqOnly_inBothReps_Feb2020/Staph_termseq_tige3_minus_strand_single_3end_base_only.gr"

Staph_tig4_plusStrand_termseq_graph_file="/srv/scratch/z3460340/PhDfiles/termseq2020/callPeakWhere_minCovg_of_singleBaseSeqOnly_inBothReps_Feb2020/Staph_termseq_tige4_plus_strand_single_3end_base_only.gr"
Staph_tig4_minusStrand_termseq_graph_file="/srv/scratch/z3460340/PhDfiles/termseq2020/callPeakWhere_minCovg_of_singleBaseSeqOnly_inBothReps_Feb2020/Staph_termseq_tige4_minus_strand_single_3end_base_only.gr"

Staph_vanco1_plusStrand_termseq_graph_file="/srv/scratch/z3460340/PhDfiles/termseq2020/callPeakWhere_minCovg_of_singleBaseSeqOnly_inBothReps_Feb2020/Staph_termseq_vanco1_plus_strand_single_3end_base_only.gr"
Staph_vanco1_minusStrand_termseq_graph_file="/srv/scratch/z3460340/PhDfiles/termseq2020/callPeakWhere_minCovg_of_singleBaseSeqOnly_inBothReps_Feb2020/Staph_termseq_vanco1_minus_strand_single_3end_base_only.gr"

Staph_vanco2_plusStrand_termseq_graph_file="/srv/scratch/z3460340/PhDfiles/termseq2020/callPeakWhere_minCovg_of_singleBaseSeqOnly_inBothReps_Feb2020/Staph_termseq_vanco2_plus_strand_single_3end_base_only.gr"
Staph_vanco2_minusStrand_termseq_graph_file="/srv/scratch/z3460340/PhDfiles/termseq2020/callPeakWhere_minCovg_of_singleBaseSeqOnly_inBothReps_Feb2020/Staph_termseq_vanco2_minus_strand_single_3end_base_only.gr"

Staph_cont2_minusStrand_termseq_graph_file="/srv/scratch/z3460340/PhDfiles/termseq2020/callPeakWhere_minCovg_of_singleBaseSeqOnly_inBothReps_Feb2020/Staph_termseq_cont2_minus_strand_single_3end_base_only.gr"
Staph_cont2_plusStrand_termseq_graph_file="/srv/scratch/z3460340/PhDfiles/termseq2020/callPeakWhere_minCovg_of_singleBaseSeqOnly_inBothReps_Feb2020/Staph_termseq_cont2_plus_strand_single_3end_base_only.gr"

Staph_cont1_plusStrand_termseq_graph_file="/srv/scratch/z3460340/PhDfiles/termseq2020/callPeakWhere_minCovg_of_singleBaseSeqOnly_inBothReps_Feb2020/Staph_termseq_cont1_plus_strand_single_3end_base_only.gr"
Staph_cont1_minusStrand_termseq_graph_file="/srv/scratch/z3460340/PhDfiles/termseq2020/callPeakWhere_minCovg_of_singleBaseSeqOnly_inBothReps_Feb2020/Staph_termseq_cont1_minus_strand_single_3end_base_only.gr"

## $Staph_cont2_plusStrand_termseq_graph_file is the first test file.
arrayOfGraphFiles=( $Staph_linz1_plusStrand_termseq_graph_file $Staph_linz1_minusStrand_termseq_graph_file $Staph_linz2_plusStrand_termseq_graph_file $Staph_linz2_minusStrand_termseq_graph_file $Staph_tig3_plusStrand_termseq_graph_file $Staph_tig3_minusStrand_termseq_graph_file $Staph_tig4_plusStrand_termseq_graph_file $Staph_tig4_minusStrand_termseq_graph_file $Staph_vanco1_plusStrand_termseq_graph_file $Staph_vanco1_minusStrand_termseq_graph_file $Staph_vanco2_plusStrand_termseq_graph_file $Staph_vanco2_minusStrand_termseq_graph_file $Staph_cont1_minusStrand_termseq_graph_file $Staph_cont1_plusStrand_termseq_graph_file $Staph_cont2_minusStrand_termseq_graph_file )

cd /srv/scratch/z3460340/PhDfiles/termseq2020/thresholding_3end_peaks_JKD6008/
my_upper_dir="$(pwd)"

find_all_threshold_peaks="/srv/scratch/z3460340/PhDfiles/termseq2019/practice_runs/my_termseq_scripts/find_all_threshold_peaks_verMarch2020_MedianAbsDev.py"

other_variables="--strand=+ --imageLimit=10 --startInterval=$startInterval --endInterval=$endInterval --seqReadLength=0 --minreads=10 --discreteMode=MAD --gff=none"
#for input_termseq_var in $Staph_cont2_plusStrand_termseq_graph_file ; do
    #python $find_all_threshold_peaks --threshold=6 --influence=0.05 --lag=10000 --coverageGraph=${input_termseq_var} --strand=+ --imageLimit=10 --startInterval=1742616 --endInterval=1815725 --seqReadLength=0 --minreads=10 --discreteMode=MAD --gff=none && echo "Python script ran successfully."

for input_termseq_var in $arrayOfGraphFiles ; do
    make_folder_for_settings_and_run_find_peaks "${find_all_threshold_peaks}" $PBS_ARRAY_INDEX $my_upper_dir 6 0.05 10000 $input_termseq_var "${other_variables}" ;
done
