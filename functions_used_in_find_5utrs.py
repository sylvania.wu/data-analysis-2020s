#!/usr/bin/python
import re
import os
import numpy as np
import pylab

######################### Last updated March 12th, 2020 at 1:09 PM #####################################################
## March 8th, 2020: In thresholding_algo(): Try to filter positions with read depth 1 or 0.
## March 8th, 2020: Try to use median and median absolute deviation (MAD) instead of avg and std.
## Only print out positive resutls if the VERBOSE_FUNCS environment (bash) variable is set to 'yes'. Save storage space.

## Save on katana as functions_used_in_find_5utrs_verMarch2020_MedianAbsDev.py

## Edited filter_signals_by_expected_seqRead_width()
def numpy_mean_not_counting_below_minreads(noCountBelow, depthsArrayY):
##### This function to be used by thresholding_algo() for discrete mode, where there are large gaps with zero seq coverage.
    try:
        inputArray = np.array(depthsArrayY)
    except NameError:
        import numpy as np
        inputArray = np.array(depthsArrayY)

    minval_filtered_array = np.extract(inputArray>=int(noCountBelow),inputArray)
    mean_value = np.mean(minval_filtered_array)

    return mean_value

def numpy_stdDev_not_counting_below_minreads(dontCountBelow, inputArrayY):
##### This function to be used by thresholding_algo() for discrete mode, where there are large gaps with zero seq coverage.
    try:
        inputArray = np.array(inputArrayY)
    except NameError:
        import numpy as np
        inputArray = np.array(inputArrayY)

    minCounts_filtered = np.extract(inputArray>=int(dontCountBelow),inputArray)
    std_dev = np.std(minCounts_filtered)

    return std_dev

def give_median_and_MAD(npArray,minmCount):
    try:
        arr_input = np.array(npArray)
    except NameError:
        import numpy as np
        arr_input = np.array(npArray)

    arr_over_mincount = np.extract(arr_input>=int(minmCount),arr_input)
    median = np.median(arr_over_mincount)

    med_deviations = np.array([])
    for datapoint in arr_over_mincount:
        ## MAD = median(abs( datapoint-whole_median ))
        myDev = abs(datapoint-median)
        med_deviations = np.append(med_deviations, myDev)
    mad = np.median(med_deviations)

    return (median, mad)
######Alt code to give_median_and_MAD(npArray,minmCount)############ Editing March 8/9, 2020 ##################
##        Cannot use because statsmodels is not available on katana.
##        The only alternative is scipy but the median abs. deviation code on scipy is deprecated.
##            try:
##                median = np.median(arr_over_mincount)     ### But note avg really means Median
##                mad = sm.stand_mad(arr_over_mincount)  ### But note std really means Median Absolute Deviation
##            except NameError:
##                import statsmodels.robust.scale as sm
##                median = np.median(arr_over_mincount)     ### But note avg really means Median
##                mad = sm.stand_mad(arr_over_mincount)  ### But note std really means Median Absolute Deviation
######################## Editing March 8/9, 2020 ##################

# Function definitions start. Learn how I can move function defs to end of script (Python style best practice...?)
def thresholding_algo(y, lag, threshold, influence, discreteMode, minimReads):
    ## The input y is equal to a tuple of (genome positions, corresponding sequencing depths)
    ##  ===> i.e. (basesArray, depthsArray)
    import os
    try:
         signals = np.zeros(len(y))
    except NameError:
        import numpy as np
        if os.environ.get('VERBOSE_FUNCS', 'No verbose environment variable') == 'yes':
            print("Imported numpy within thresholding_algo()\n")
        signals = np.zeros(len(y))

    #signals = np.zeros(len(y))
    filteredY = np.array(y)
    avgFilter = [0]*len(y)
    stdFilter = [0]*len(y)

    if (discreteMode=="minreads"):
        avgFilter[lag - 1] = numpy_mean_not_counting_below_minreads(noCountBelow=minimReads, depthsArrayY=y[0:lag])
        stdFilter[lag - 1] = numpy_stdDev_not_counting_below_minreads(noCountBelow=minimReads, depthsArrayY=y[0:lag])

    elif (discreteMode=="MAD") or (discreteMode=="mad"):
        (avgFilter[lag - 1], stdFilter[lag - 1]) = give_median_and_MAD(npArray=y[0:lag],minmCount=minimReads)

    elif (discreteMode=="no"):
        avgFilter[lag - 1] = np.mean(y[0:lag])  #### March 4, 2020.
                                                #### Don't count any depth values OR
                                                #### positions to the mean if they equal zero?
        stdFilter[lag - 1] = np.std(y[0:lag])


    if os.environ.get('VERBOSE_FUNCS', 'No verbose environment variable') == 'yes':
        # print("myFunctions: About to run thresholding_algo...\n")
        pass

################### all within for-loop ########################################################################
    for i in range(lag, len(y) - 1):
        if abs(y[i] - avgFilter[i-1]) > threshold * stdFilter [i-1]:
            if y[i] > avgFilter[i-1]:
                signals[i] = 1
            filteredY[i] = influence * y[i] + (1 - influence) * filteredY[i-1]
            ## Original code:          avgFilter[i] = np.mean(filteredY[(i-lag):i])
            #### Edited March 4, 2020. Don't count any depth values OR positions to the mean if they equal zero?
            if (discreteMode=="minreads"):
                avgFilter[i] = numpy_mean_not_counting_below_minreads( noCountBelow=minimReads, depthsArrayY=filteredY[(i-lag):i] )
                stdFilter[i] = numpy_stdDev_not_counting_below_minreads(noCountBelow=minimReads, depthsArrayY=filteredY[(i-lag):i])
            elif (discreteMode=="MAD") or (discreteMode=="mad"):
                (avgFilter[i], stdFilter[i]) = give_median_and_MAD(npArray=filteredY[(i-lag):i],minmCount=minimReads)
            else:
                avgFilter[i] = np.mean(filteredY[(i-lag):i])
                stdFilter[i] = np.std(filteredY[(i-lag):i])

        else:           ### If threshold not passed. Outer "else" to march outmost "if" right under for-loop.
            signals[i] = 0
            filteredY[i] = y[i]
            #### Edited March 4, 2020. Don't count any depth values OR positions to the mean if they equal zero?
            if (discreteMode=="minreads"):
                avgFilter[i] = numpy_mean_not_counting_below_minreads( noCountBelow=minimReads, depthsArrayY=filteredY[(i-lag):i] )
                stdFilter[i] = numpy_stdDev_not_counting_below_minreads( noCountBelow=minimReads, depthsArrayY=filteredY[(i-lag):i])
            elif (discreteMode=="MAD") or (discreteMode=="mad"):
                (avgFilter[i], stdFilter[i]) = give_median_and_MAD(npArray=filteredY[(i-lag):i],minmCount=minimReads)
            else:
                avgFilter[i] = np.mean(filteredY[(i-lag):i])
                stdFilter[i] = np.std(filteredY[(i-lag):i])
           #### Edited March 11, 2020.
################### all within for-loop ########################################################################
    return dict(signals = np.asarray(signals),
                avgFilter = np.asarray(avgFilter),
                stdFilter = np.asarray(stdFilter))




def plot_thresholding_results(baseArray,depthArray,resultsDict,minStdDev,nameFile):
    try:
        pylab.subplot(211)
    except NameError:
        import pylab
        print("Imported pylab plot_thresholding_results()\n")
        pylab.subplot(211)

    pylab.plot(baseArray, depthArray)
    pylab.plot(baseArray, resultsDict["avgFilter"], color="cyan", lw=2)
    pylab.plot(baseArray, resultsDict["avgFilter"] + float(minStdDev) * resultsDict["stdFilter"], color="green", lw=2)
    pylab.plot(baseArray, resultsDict["avgFilter"] - float(minStdDev) * resultsDict["stdFilter"], color="green", lw=2)
    pylab.subplot(212)
    pylab.step(baseArray, resultsDict["signals"], color="red", lw=2)
    pylab.ylim(-1.5, 1.5)
    pylab.title("Results for: "+str(nameFile))
    pylab.savefig(str(nameFile)+"_signals.png")
    pylab.close()

def filter_signals_by_expected_seqRead_width(readWidth,tolerance,searchRange,signalArray):
    positive_results = dict()
    for i in searchRange:
        if (signalArray[i] == 1 and signalArray[i-1] == 0):
            if (0 in signalArray[i+1:i+readWidth+tolerance]) and (0 not in signalArray[i+1:i+tolerance]):
                # Peak sharpness, given that the Term-seq read length is 75 bp. Peak no wider than 90 bp and no narrower than 20 bp. Works with lag=500, influence=0.01, threshold=5
                start_of_signal = i
                for nextBase in range(i+1, i+readWidth+tolerance+1):
                    if nextBase == 0:
                        end_of_signal = nextBase
                        break
                try:
                    # positive_results.append( (start_of_signal, end_of_signal) )
                    positive_results[start_of_signal] = end_of_signal
                except NameError:    ## end_of_signal name is caleld before assignment
                    # print("Warning, end of signal not found for the start of signal at "+str(start_of_signal)+"\n")
                    positive_results[start_of_signal] = "NA"

    return positive_results

def graph_fields_reader(enterLine):
    import os
    myFields = enterLine.split('\t')
    if (len(myFields) == 2):
        base = int(myFields[0])
        depth = float(myFields[1])
        ### If verbose option activated for debugging
        if os.environ.get('VERBOSE_FUNCS', 'No verbose environment variable') == 'yes':
            # print("myFunctions.graph_fields_reader(): Parsed base and depth from one line of graph file...\n")
            pass
    elif (len(myFields) == 3):
        base = int(myFields[1])
        depth = float(myFields[2])
        if os.environ.get('VERBOSE_FUNCS', 'No verbose environment variable') == 'yes':
            # print("myFunctions.graph_fields_reader(): Parsed base and depth from one line of graph file...\n")
            pass
    else:
        print("myFunctions.graph_fields_reader(): Wrong number of fields in coverage graph file (.gr), should be 2 or 3 (chromosome, position, read depth)\n")
        print("Line is: \n")
        print(enterLine)
        print("\nmyFields: \n")
        print(myFields)
        print("\n")
        return ("NA","NA")
    return (base, depth)

def add_to_bases_and_depths_arrays(base_to_add, depth_to_add, base_np_array, depth_np_array, sense, counter):
    #            if (len(base_np_array) >= 2) and ( base_np_array[1] < base_np_array[0] ):
    #                print("Error sort the graph (.gr) file from lowest genomic coordinate to highest.\n")
    #                print("base_np_array[0]: "+str(base_np_array[0])+"\n")
    #                print("base_np_array[1]: "+str(base_np_array[1])+"\n")
    #                print("sense: "+str(sense)+"\n")
    # Counterproductive. Do this test outside this function.
    import os
    if (sense == "descending"):
        try:
            base_np_array = np.insert(base_np_array, 0, base_to_add)
        except NameError:
            import numpy as np
            base_np_array = np.insert(base_np_array, 0, base_to_add)
        depth_np_array = np.insert(depth_np_array, 0, depth_to_add)
        if os.environ.get('VERBOSE_FUNCS', 'No verbose environment variable') == 'yes':
            # print("myFunctions.add_to_bases_and_depths_arrays(): Added base to depths array...")
            pass

    elif (sense == "ascending"):
        try:
            base_np_array = np.append(base_np_array, base_to_add)
        except NameError:
            import numpy as np
            base_np_array = np.append(base_np_array, base_to_add)
        depth_np_array = np.append(depth_np_array, depth_to_add)
        if os.environ.get('VERBOSE_FUNCS', 'No verbose environment variable') == 'yes':
            # print("myFunctions.add_to_bases_and_depths_arrays(): Added base to depths array...")
            pass

    else:
        print("Direction must be descending or ascending. direction = "+str(direction)+"\n")

    return (base_np_array,depth_np_array)

class CustomError(Exception):
    pass

def parse_depth_graph(baseLimits,path_to_gr_file, direction):  # baseLimits is a tuple defining start and end of lines to read!

    import os
    try:
        basesArray = np.array([])
    except NameError:
        import numpy as np
        print("Imported numpy within thresholding_algo()\n")
        basesArray = np.array([])

    depthsArray = np.array([])

    if len(baseLimits) == 2:
        print("baseLimits for parsing depth graph: \n"+str(baseLimits)+"\n")
        import linecache

        c = 0
        for baseNum in range( min(baseLimits),max(baseLimits) ):
            line = linecache.getline(path_to_gr_file, baseNum-1) #linecache.getline() is 0-based counter
            (position, read_depth) = graph_fields_reader(line)

            if (position == "NA"):
                print("Wrong number of fields in coverage graph line. Header or empty footer line? Pass. ine was:\n")
                print(line)
                print("\n")
                continue

            elif ( int(position) != int(baseNum-1) ):
                raise IndexError("The (N-1)th line of the coverage graph file must correspond to the Nth base of the genome... line number (minus one because linecache is 0-based) = "+str(baseNum-1)+", base of genome at line = "+str(position)+"\n")
                quit()


            if c == 0:   # Equivalent to if baseNum==min(*baseLimits):
                basesArray = np.append(basesArray, position)
                depthsArray = np.append(depthsArray, read_depth)
                if os.environ.get('VERBOSE_FUNCS', 'No verbose environment variable') == 'yes':
                    # print("myFunctions.parse_depth_graph(): Added first entry to basesArray and depthsArray. position:"+str(position)+", read_depth:"+str(read_depth)+"\n")
                    pass
                c = c + 1
            else:
                (basesArray, depthsArray) = add_to_bases_and_depths_arrays(base_to_add=position, depth_to_add=read_depth, base_np_array=basesArray, depth_np_array=depthsArray, sense=direction, counter=c)
                if os.environ.get('VERBOSE_FUNCS', 'No verbose environment variable') == 'yes':
                    #print("myFunctions.parse_depth_graph(): Added an entry to basesArray and depthsArray. position:"+str(position)+", read_depth:"+str(read_depth)+"\n")
                    pass
                c = c + 1

    else:
        print("Base limits not defined for parse_depth_graph(). Start to read the whole coverage graph file line-by-line?\n")
        import os
        inputFileSize = os.stat(path_to_gr_file).st_size
        if float(inputFileSize) > 5000000:
            raise CustomError("parse_depth_graph() in functions_used_in_find_5utrs.py: Error, coverage graph file too large to read all bases, and no base limits provided.")
            quit()

        covgGraph = open(path_to_gr_file,'r')

        for i,line in enumerate(covgGraph):
            if os.environ.get('VERBOSE_FUNCS', 'No verbose environment variable') == 'yes':
                # print("Reading line in graph file...\n")
                #print(line)
                #print("\n")
                pass

            (position, read_depth) = graph_fields_reader(line)

            if i==0:
                basesArray = np.append(basesArray, position)
                depthsArray = np.append(depthsArray, read_depth)

            else:
                (basesArray, depthsArray) = add_to_bases_and_depths_arrays(base_to_add=position, depth_to_add=read_depth, base_np_array=basesArray, depth_np_array=depthsArray, sense=direction, counter=i)
                if os.environ.get('VERBOSE_FUNCS', 'No verbose environment variable') == 'yes':
                    # print("myFunctions.parse_depth_graph(): Added an entry to basesArray and depthsArray. position:"+str(position)+", read_depth:"+str(read_depth)+"\n")
                    pass
        covgGraph.close()
    return (basesArray, depthsArray)



def extract_gene_name_from_gff(gff_descr_field_string):
            genename_matchobj = re.search(r'ID=(.+?);(.*)gene=(.+?);(.*)product=(.+?);',gff_descr_field_string)
            if (genename_matchobj):
                extracted_name = genename_matchobj.group(3)
                #product_descr = genename_matchobj.group(5)

            #if (genename_matchobj == None):
                #genename_matchobj = re.search(r'ID=(.+?);(.*)gbkey=(.+?);(.*)product=(.+?);',line_fields[8])
                #gene_name = genename_matchobj.group(1)
                #product_descr = genename_matchobj.group(5)

            if (genename_matchobj == None):   # If it is a proper Genbank entry, this will surely match
                genename_matchobj = re.search(r'ID=(.+?);',gff_descr_field_string)
                extracted_name = genename_matchobj.group(1)
                #product_descr = "NA"

                    #Regular expression to match the CDS Genbank # ID
                    #ID=cds-ADL64095.1;
            #/ID=.*;/? (the regex in perl syntax)

            return extracted_name

def define_gene_coding_sequences(GFF_input):
    my_enum_GFF = enumerate(GFF_input)
    gene_CDS_on_plus_strand = dict()
    gene_CDS_on_minus_strand = dict()

    for i, line in my_enum_GFF:
        line_fields = line.split('\t')
        if ("#" in line_fields[0]):
            continue # Read next GFF line if this line is a comment

        if (len(line_fields)>5):
            record_type = line_fields[2]
        else:
            continue

        if (record_type == "CDS"):
            strand = line_fields[6]
            my_CDS_gene_name = extract_gene_name_from_gff(line_fields[8])
            #print("I know the record type is a cds\n")
            if (strand == "+"):
                my_CDS_start_pos = line_fields[3] # This is the start codon for a CDS
                my_CDS_end_pos = line_fields[4]
                gene_CDS_on_plus_strand[my_CDS_gene_name] = (my_CDS_start_pos,my_CDS_end_pos)

            elif (strand == "-"):
                my_CDS_start_pos = line_fields[4] # This is the start codon for a CDS
                my_CDS_end_pos = line_fields[3]
                gene_CDS_on_minus_strand[my_CDS_gene_name] = (my_CDS_start_pos,my_CDS_end_pos)

            else:
                print("Error in define_gene_coding_sequences(), GFF file strand field not expected\n")

    return gene_CDS_on_plus_strand, gene_CDS_on_minus_strand


def get_gene_start_codons(GFF_input): # Input is an open GFF file.
    open_file = open(GFF_input,'r')
    enum_GFF = enumerate(open_file)
    start_codons_on_plus_strand = dict()
    start_codons_on_minus_strand = dict()
    # Read in CDS start positions - Check Genbank conventions for +/- strand and ATG start position.
    for i, line in enum_GFF:
        #print("Debugging, reading in enum_GFF line (get_gene_start_codons())\n")
        line_fields = line.split('\t')
        if ("#" in line_fields[0]):
            continue # Read next GFF line if this line is a comment

        if (len(line_fields)>5):
            record_type = line_fields[2]
        else:
            continue
        #try:
        #    record_type = line_fields[2]
        #except IndexError:
        #    print("The line of gff file with index error is ")
        #    print(i)
        #    continue
        gene_name = extract_gene_name_from_gff(line_fields[8])


        if (record_type == "CDS"):
            strand = line_fields[6]
            #print("I know the record type is a cds\n")
            if (strand == "+"):
                start_pos = line_fields[3] # This is the start codon for a CDS
                start_codons_on_plus_strand[start_pos] = gene_name
            elif (strand == "-"):
                start_pos = line_fields[4] # This is the start codon for a CDS
                start_codons_on_minus_strand[start_pos] = gene_name

            else:
                print("Error, GFF file strand field not expected\n")
    open_file.close()

    return start_codons_on_plus_strand, start_codons_on_minus_strand
        # Returns a tuple of two dictionaries.

def define_5prime_UTRs(plus_start_codons_dict, minus_start_codons_dict, TSS_posns_tuples, max_length): # Perhaps convert this into a class/object LATER
    import os
    my_5UTRs = dict() # (TSS_start, end_of_5UTR, strand, gene)
    #print(max5utrlen)
    for TSStuple in TSS_posns_tuples: # Tuple of transcription start sites looks like (TSS_posn,TSS_strand,TSS_coverage)
        print(TSStuple)
        if (TSStuple[1] is '+'):
            if os.environ.get('VERBOSE_FUNCS', 'No verbose environment variable') == 'yes':
                print("Is define_5prime_UTRs checking strand if loop running\n")
            position = int(TSStuple[0])
            plusStrandCDSKeys = plus_start_codons_dict.keys()
            while( (int(position) - int(TSStuple[0])) <= max_length ):
                #print("Is define_5prime_UTRs while position-tsstuple[0] less than max5utrlen running?\n")
                if str(position) in plusStrandCDSKeys:
                    end_of_5UTR = position
                    gene_of_5UTR = plus_start_codons_dict[str(position)]

                    print("Reached the end of a 5' utr. TSS: "+str(TSStuple[0])+" CDS: "+str(end_of_5UTR)+", minus strand, gene: "+str(gene_of_5UTR)+"\n")
                    my_5UTRs[TSStuple[0]] = ( end_of_5UTR,'+',gene_of_5UTR )
                    #### {  TSS_start => ( end_of_5UTR, strand, gene)  }
                    #### I made this dictionary by searching for downstream CDS for each unique TSS. So the TSS SHOULD be unique keys of my_5UTRs.
                    break

                elif (position >= 2924344):
                    print("Reached edge of the genome\n")
                    break

                elif (position >= int( max(plus_start_codons_dict.keys()) ) ) or (position >= max({int(coord) for (coord,y,z) in TSS_posns_tuples})):  ## (TSS_posn,TSS_strand,TSS_coverage)
                    print("Reached edge of the given interval: "+str(position)+"\n")
                    break

                else:
                    print("Counting forwards...\n")
                    position = position + 1
                    print("position is "+str(position)+"\n")


             # Dictionary of CDS start codons looks like { start_pos:gene_name }

        elif (TSStuple[1] is '-'):
            position = int(TSStuple[0])
            minusStrandCDSKeys = minus_start_codons_dict.keys()
            while( (int(TSStuple[0]) - int(position)) <= max_length ):
                if str(position) in minusStrandCDSKeys:
                    end_of_5UTR = position
                    gene_of_5UTR = minus_start_codons_dict[str(position)]
                    print("Reached the end of a 5' utr. TSS: "+str(TSStuple[0])+" CDS: "+str(end_of_5UTR)+", minus strand, gene: "+str(gene_of_5UTR)+"\n")
                    my_5UTRs[TSStuple[0]] = ( end_of_5UTR,'-',gene_of_5UTR )
                    #### {  TSS_start => ( end_of_5UTR, strand, gene)  }
                    #### I made this dictionary by searching for downstream CDS for each unique TSS. So the TSS SHOULD be unique keys of my_5UTRs.
                    break
                elif (position <= 1):
                    print("Reached edge of the genome stop counting\n")
                    break

                elif (  position <= int( min(plus_start_codons_dict.keys()) )   ) or (position >= min({int(coord) for (coord,y,z) in TSS_posns_tuples})):
                    print("Reached edge of the given interval: "+str(position)+"\n")
                    break

                else:
                    print("Counting back...\n")
                    position = position - 1
        else:
            print("Error, GFF file strand field not expected, error in define_5prime_UTRs()\n")



    return my_5UTRs

# Map termseq sites within 5' UTRs.




def get_base_at_position( position ):

    opened_genome = open("/srv/scratch/z3460340/PhDfiles/termseq2019/all_Saureus_Termseq_results/ref_genome/jkd6008genome.fasta",'r')
    all_bases = opened_genome.read().replace('\n', '').replace('\r', '')
    #print "Length of genome.read.replace is ", len(all_bases)

    char_data = all_bases[position]
    #print "char_data is ",char_data,"\n"

    if (position > len(all_bases)-1):
        position = len(all_bases)
    if (position < 1):
        position = 1

    char_data = all_bases[position]
    print("char_data is "+str(char_data)+"\n")

    if char_data in "AaTtCcGg":
         opened_genome.close()
         return char_data

    elif char_data in "\r\n":
         print('Error did not strip newlines')
         opened_genome.close()
         return "N"
    else:
         print('Nonvalid characters'+str(char_data)+'\n')
         opened_genome.close()
         return char_data


def find_maximum_covg_in_interval_samtools_pipe(start_interval, end_interval, search_strand, path_to_bamfile, path_to_dump_tempfile):
    ###### Previously, path_to_dump_tempfile = '/srv/scratch/z3460340/PhDfiles/termseq2019/all_Saureus_Termseq_results/jkd6009_termseq_project/results_map_putative_r5UTRs_August2019/September_map_putative_r5UTRs_python_script/test'
    os.system("module add samtools/1.9; samtools view -b "+str(path_to_bamfile)+" CP002120.1:"+str(start_interval)+"-"+str(end_interval)+" > "+str(path_to_dump_tempfile)+"/tempfile1.bam; samtools reheader -P -i /srv/scratch/z3460340/PhDfiles/test_code_covg_graph_matplotlib/new_header "+str(path_to_dump_tempfile)+"/tempfile1.bam | samtools view -h > "+str(path_to_dump_tempfile)+"/tempfile1.sam")

    ### print("Debug message. Path to my current working directory (before change) is: "+str(path_to_dump_tempfile)+"\n")

    if ( path_to_dump_tempfile.endswith("/") == False ):
        path_to_dump_tempfile = path_to_dump_tempfile+"/"

    print("Debug message. Path to my current working directory (after check and adding / to end) is: "+str(path_to_dump_tempfile)+"\n")

    ##### try:
    #####     filename = str(path_to_dump_tempfile)+"tempfile1.sam"
    #####     f = open(filename,'r')
    ##### except IOError:
    #####     print("Could not read file:"+str(filename)+"\n")
    #####     quit()

    filename = str(path_to_dump_tempfile)+"tempfile1.sam"

    if ("//" in filename):
        sam_dataset = sam_dataset.replace('//','/')

    sam_dataset = open(filename,'r')

    print("Debug message. Path to my temp samfile (after removing //) is: "+str(filename)+"\n")

    rnaseq_mappings_plusStrand_freqs = dict()
    rnaseq_mappings_minusStrand_freqs = dict()

    for line in sam_dataset:
        print("Start to read temp samfile....\n")
        if ("@HD" in line):
            print("read header line\n")
            continue
        if ("@PG" in line):
            print("read header line\n")
            continue
        if ("@SQ" in line):
            print("read header line\n")
            continue
        print("line in samtools_stdout is....\n")
        print(str(line))
        print("\n")
        if (type(line) == "int"):
            print("Error in reading line...")
            print(line)
            continue

        line = line.rstrip()
        fields = line.split('\t')


        if ( ( int(fields[1]) != 16) and ( int(fields[1]) != 0 )  ):
            print("unexpected SAM flag...\n")
            continue
        if ( (search_strand == '+') and (int(fields[1]) != 0)  ):
            print("strand + doesn't match FLAG field. FLAG is: "+str(fields[1])+"\n")
            continue
        if ( (search_strand == '-') and (int(fields[1]) != 16)  ):
            print("strand - doesn't match FLAG field. FLAG is: "+str(fields[1])+"\n")
            continue
        mapped_position = int(fields[3])

        ########### Insert changes September 1: Read CIGAR string of the SAM file and include entire length of the RNA read to the coverage graph (not just leftmost mapped position)
        cigar_string = str(fields[5])
        SM_regex_cig = r"\b([0-9]*)S([0-9]*)M\b"
        MS_regex_cig = r"\b([0-9]*)M([0-9]*)S\b"
        M_regex_cig = r"\b([0-9]*)M\b"
        SM_matchobj = re.search(SM_regex_cig,cigar_string)
        MS_matchobj = re.search(MS_regex_cig,cigar_string)
        M_matchobj = re.search(M_regex_cig,cigar_string)

        if ( (SM_matchobj) and (MS_matchobj==None) and (M_matchobj==None) ):
            print("Checking, the matched CIGAR string is "+str(cigar_string)+" \n")
            bases_to_clip = int(SM_matchobj.group(1))
            bases_to_read = int(SM_matchobj.group(2))
            ######### SM
            left_map_interval = mapped_position + bases_to_clip
            right_map_interval = mapped_position + bases_to_clip + bases_to_read

        elif ( (MS_matchobj) and (SM_matchobj==None) and (M_matchobj==None) ):
            print("Checking, the matched CIGAR string is "+str(cigar_string)+" \n")
            bases_to_read = int(MS_matchobj.group(1))
            bases_to_clip = int(MS_matchobj.group(2))
            ######### MS
            left_map_interval = mapped_position
            right_map_interval = mapped_position + bases_to_read

        elif ( (M_matchobj) and (SM_matchobj==None) and (MS_matchobj==None) ):
            print("Checking, the matched CIGAR string is "+str(cigar_string)+" \n")
            bases_to_read = int(M_matchobj.group(1))
            left_map_interval = mapped_position
            right_map_interval = mapped_position + bases_to_read
        else:
            print("Error, unexpected value in CIGAR string (is it an insertion or deletion?). CIGAR is: "+str(cigar_string)+"\n")
            continue
        ########## LATER, it would be nice to retrieve the sequence from left_map_interval to right_map_interval from the FASTA genome and compare it to the SEQ field of the SAM file. This would confirm this section is working properly.
        print("Counted one read. Leftmost mapped position is "+str(left_map_interval)+" and rightmost mapped position is "+str(right_map_interval)+" \n")

        if ( int(fields[1]) == 0 ):
            for base_covered in range(left_map_interval, right_map_interval):
                print("looping through base_covered in my left_map_interval, right_map_interval:"+str(left_map_interval)+", "+str(right_map_interval)+"\n")
                print("base is: "+str(base_covered)+"\n")

                if (base_covered not in rnaseq_mappings_plusStrand_freqs.keys()):
                    rnaseq_mappings_plusStrand_freqs[base_covered] = 1
                    print("mapped a new RNA-seq read to position "+str(mapped_position)+" on the FORWARD strand. \nLeftmost mapped position is "+str(left_map_interval)+" and rightmost mapped position is "+str(right_map_interval)+" \n")

                else:
                    rnaseq_mappings_plusStrand_freqs[base_covered] += 1 ### increment
                    print("mapped an RNA-seq read to position "+str(mapped_position)+" on the FORWARD strand. \nLeftmost mapped position is "+str(left_map_interval)+" and rightmost mapped position is "+str(right_map_interval)+" \n")

        elif ( int(fields[1]) == 16 ):
            for base_covered in range(left_map_interval, right_map_interval):
                print("looping through base_covered in my left_map_interval, right_map_interval:"+str(left_map_interval)+", "+str(right_map_interval)+"\n")
                print("base is: "+str(base_covered)+"\n")

                if (base_covered not in rnaseq_mappings_minusStrand_freqs.keys()):
                    rnaseq_mappings_minusStrand_freqs[base_covered] = 1
                    print("mapped a RNA-seq read to NEW position "+str(mapped_position)+" on the FORWARD strand. \nLeftmost mapped position is "+str(left_map_interval)+" and rightmost mapped position is "+str(right_map_interval)+" \n")
                else:
                    rnaseq_mappings_minusStrand_freqs[base_covered] += 1 ### increment
                    print("mapped an RNA-seq read to position "+str(mapped_position)+" on the REVERSE strand. \nLeftmost mapped position is "+str(left_map_interval)+" and rightmost mapped position is "+str(right_map_interval)+" \n")
        else:
            print("Strand unexpected? samfile line fields[1]: "+str(fields[1])+"\n" )
            continue
        ############################ END for line in sam_dataset

    ### Keeping tempfiles to help debugging. Remember to delete later.
    os.system("rm "+str(path_to_dump_tempfile)+"/tempfile1.sam")
    os.system("rm "+str(path_to_dump_tempfile)+"/tempfile1.bam")
    sam_dataset.close()

    #### Declared the original dicts before for loop through lines in temp samfile.

    checkPlusStrandKeys = rnaseq_mappings_plusStrand_freqs.keys()
    checkMinusStrandKeys = rnaseq_mappings_minusStrand_freqs.keys()

    if (  ( len(rnaseq_mappings_plusStrand_freqs.keys()) == 0 ) and ( search_strand == '+') ):
        print("Error, no Term-seq reads counted using samtools into the hash rnaseq_mappings_plusStrand_freqs \n")
        return (0, 0)
    elif ( search_strand == '+'):
        print("One of the values in rnaseq_mappings_plusStrand_freqs is..."+str(rnaseq_mappings_plusStrand_freqs.popitem())+"\n")
    elif (  ( len(rnaseq_mappings_minusStrand_freqs.keys()) == 0 ) and ( search_strand == '-') ):
        print("Error, no Term-seq reads counted using samtools into the hash rnaseq_mappings_minusStrand_freqs \n")
        return (0, 0)
    elif ( search_strand == '-'):
        print("One of the values in rnaseq_mappings_minusStrand_freqs is..."+str(rnaseq_mappings_minusStrand_freqs.popitem())+"\n")
    else:
        try:
            rnaseq_mappings_plusStrand_freqs.popitem()
        except ValueError as e1:
            E1 = "empty"
        try:
            rnaseq_mappings_minusStrand_freqs.popitem()
        except ValueError as e1:
            E2 = "empty"
        if ( (E1 == "empty") and (E2 == "empty") ):
            print(e1)
            print("\n")
            print(e2)
            return (0, 0)
        print("Error but don't know why... check strand input to find_maximum_covg_in_interval_samtools_pipe() ??\n")
        return (0, 0)

    ###### try:
    ######     print("One of the values in rnaseq_mappings_plusStrand_freqs is..."+str(rnaseq_mappings_plusStrand_freqs.popitem())+"\n")
    ###### except KeyError as e:


    local_maxima = []

    if (search_strand == '+'):
        try:
            max_coverage_plus_interval = max(rnaseq_mappings_plusStrand_freqs.values())
        except ValueError:
            print("Error, rnaseq_mappings_plusStrand_freqs dictionary empty\n")
            return (0,0)

    if (search_strand == '-'):
        try:
            max_coverage_minus_interval = max(rnaseq_mappings_minusStrand_freqs.values())
        except ValueError:
            print("Error, rnaseq_mappings_minusStrand_freqs dictionary empty\n")
            return (0,0)

    if (search_strand == '+'):
        for (Pos,Covg) in rnaseq_mappings_plusStrand_freqs.items():
            print("Looping through for (Pos,Covg) in rnaseq_mappings_plusStrand_freqs.items\n")
            if ( Covg == max_coverage_plus_interval ):
                print("Adding to local_maxima []\n")
                local_maxima.append( Pos )

    if (search_strand == '-'):
        for (Pos,Covg) in rnaseq_mappings_minusStrand_freqs.items():
            print("Looping through for (Pos,Covg) in rnaseq_mappings_minusStrand_freqs.items\n")
            if ( Covg == max_coverage_minus_interval ):
                print("Adding to local_maxima []\n")
                local_maxima.append( Pos )

    ########### Doesn't really matter if its in the 5'UTR anyway. But report closest to the TSS
    if ( (len(local_maxima) > 1) and (search_strand == '+') ):
        max_coverered_position = min(local_maxima)
        maxHeight = rnaseq_mappings_plusStrand_freqs[max_coverered_position]
        print("Term-seq peak in interval "+str(start_interval)+", "+str(end_interval)+" is at position "+str(max_coverered_position)+" with read depth "+str(maxHeight)+"\n")
    elif ( (len(local_maxima) > 1) and (search_strand == '-') ):
        max_coverered_position = max(local_maxima)
        maxHeight = rnaseq_mappings_minusStrand_freqs[max_coverered_position]
        print("Term-seq peak in interval "+str(start_interval)+", "+str(end_interval)+" is at position "+str(max_coverered_position)+" with read depth "+str(maxHeight)+"\n")
    elif (search_strand == '+'):
        max_coverered_position = local_maxima[0]
        maxHeight = rnaseq_mappings_plusStrand_freqs[max_coverered_position]
        print("Term-seq peak in interval "+str(start_interval)+", "+str(end_interval)+" is at position "+str(max_coverered_position)+" with read depth "+str(maxHeight)+"\n")
    elif (search_strand == '-'):
        max_coverered_position = local_maxima[0]
        maxHeight = rnaseq_mappings_minusStrand_freqs[max_coverered_position]
        print("Term-seq peak in interval "+str(start_interval)+", "+str(end_interval)+" is at position "+str(max_coverered_position)+" with read depth "+str(maxHeight)+"\n")
    else:
        print("Unexpected strand or local_maxima list empty?? find_maximum_covg_in_interval_samtools_pipe()\n")

    return (max_coverered_position, maxHeight)




########################################################################################################
