#!/usr/bin/env python3
import sys
from statistics import mean
from optparse import OptionParser
import funcs_for_short_long_transcript_comparison as myFunctions


############# Last modified August 24, 2020 at 5:30 PM
## python ../scripts/compare_long_short_transcript_readthrough_2020.py --ops="unix" --gff=${jkd6008Annotation} --table5utrs=${table5utrsVancomycin} --condition="vancomycin" --workingDir=$(pwd) --withinKnownCDS="yes" --bedgraph1=${rnaseqGraphPlusStrandVan1} --bedgraph1_minus_strand=${rnaseqGraphMinusStrandVan1} --bedgraph2=${rnaseqGraphPlusStrandVan2} --bedgraph2_minus_strand=${rnaseqGraphMinusStrandVan2} --bedgraph3=${rnaseqGraphPlusStrandVan3} --bedgraph3_minus_strand=${rnaseqGraphMinusStrandVan3} ;


parser = OptionParser()
parser.add_option("-g", "--gff", action="store",type="string",dest="annotations_file")
parser.add_option("-O", "--ops", action="store",type="string",dest="operating_system")
parser.add_option("-t", "--table5utrs", action="store",type="string",dest="term5utrTable_path")
parser.add_option("-c", "--condition", action="store",type="string",dest="experimentCondition")
parser.add_option("-w", "--workingDir", action="store",type="string",dest="currentDir")
parser.add_option("-C", "--withinKnownCDS", action="store",type="string",dest="withinKnownCDS")

parser.add_option("-r", "--rnaseq1", action="store",type="string",dest="rnaseq1",default=None)
parser.add_option("-R", "--rnaseq2", action="store",type="string",dest="rnaseq2",default=None)

parser.add_option("-D", "--bedgraph1", action="store",type="string",dest="bedgraph1",default=None)
parser.add_option("-d", "--bedgraph1_minus_strand", action="store",type="string",dest="bedgraph1_minus_strand",default=None)
parser.add_option("-E", "--bedgraph2", action="store",type="string",dest="bedgraph2",default=None)
parser.add_option("-e", "--bedgraph2_minus_strand", action="store",type="string",dest="bedgraph2_minus_strand",default=None)
parser.add_option("-F", "--bedgraph3", action="store",type="string",dest="bedgraph3",default=None)
parser.add_option("-f", "--bedgraph3_minus_strand", action="store",type="string",dest="bedgraph3_minus_strand",default=None)

parser.add_option("-N", "--normalisation", action="store",type="string",dest="normalisation",default=None)

(options, args) = parser.parse_args()

myOperSys = options.operating_system
my_exp_condition = options.experimentCondition
input5UTRtable = options.term5utrTable_path
annotations = options.annotations_file
workingDir = options.currentDir


if (annotations == "default"):
    annotations = "/srv/scratch/z3460340/PhDfiles/termseq2019/all_Saureus_Termseq_results/config_and_annotation_files/jkd6008_gene_annotations_genbank.gff3"

######### Hard-coded. Open up these RNA-seq aligned reads from Stinear et al., 2013 study (Howden lab Melbourne)
######### 71 and 72 = no antibiotic.
######### 75 and 76 = linezolid.
######### 77 and 78 = tigecycline.
######### 79 and 80 = vancomycin.

if (my_exp_condition == "control") and (options.rnaseq1 == "howdenlabs"):
    my_RNAseq_bamfile1 = "/srv/scratch/z3460340/PhDfiles/RNAseqhowdenlab/SRR568071_aligned_august5_SORTED.bam"
    RNAseq1_totalReads = 3290236
    my_RNAseq_bamfile2 = "/srv/scratch/z3460340/PhDfiles/RNAseqhowdenlab/SRR568072_aligned_august5_SORTED.bam"
    RNAseq2_totalReads = 5548580

if (my_exp_condition == "linezolid") and (options.rnaseq1 == "howdenlabs"):
    #### linezolid RNA-seq from Howden lab's data. Aligned, sorted, and indexed.
    my_RNAseq_bamfile1 = "/srv/scratch/z3460340/PhDfiles/RNAseqhowdenlab/SRR568075_aligned_august5_SORTED.bam"
    RNAseq1_totalReads = 7821467
    my_RNAseq_bamfile2 = "/srv/scratch/z3460340/PhDfiles/RNAseqhowdenlab/SRR568076_aligned_august5_SORTED.bam"
    RNAseq2_totalReads = 5548580

if (my_exp_condition == "tigecycline") and (options.rnaseq1 == "howdenlabs"):
    my_RNAseq_bamfile1 = "/srv/scratch/z3460340/PhDfiles/RNAseqhowdenlab/SRR568077_aligned_august5_SORTED.bam"
    RNAseq1_totalReads = 6991865
    my_RNAseq_bamfile2 = "/srv/scratch/z3460340/PhDfiles/RNAseqhowdenlab/SRR568078_aligned_august5_SORTED.bam"
    RNAseq2_totalReads = 4256302

if (my_exp_condition == "vancomycin") and (options.rnaseq1 == "howdenlabs"):
    #### vancomycin RNA-seq from Howden lab's data. Aligned, sorted, and indexed.
    my_RNAseq_bamfile1 = "/srv/scratch/z3460340/PhDfiles/RNAseqhowdenlab/SRR568079_aligned_august5_SORTED.bam"
    RNAseq1_totalReads = 3734899
    my_RNAseq_bamfile2 = "/srv/scratch/z3460340/PhDfiles/RNAseqhowdenlab/SRR568080_aligned_august5_SORTED.bam"
    RNAseq2_totalReads = 2592958

if (options.rnaseq1 != None):
    if ( str(options.rnaseq1+".")[-4:-1] == "bam" ):
        my_RNAseq_bamfile1 = options.rnaseq1

if (options.rnaseq2 != None):
    if ( str(options.rnaseq2+".")[-4:-1] == "bam" ):
        my_RNAseq_bamfile2 = options.rnaseq2



''' Start reading of plus and minus strand RNA-seq depth graphs into dataframes (pandas/numpy) '''

if ( options.rnaseq1 == None ) and ( options.rnaseq2 == None ):
    import numpy as np
    import pandas as pd

                                 ## use keep_default_na so that zero-read-depth won't be read as NA in the table.
    if (options.bedgraph1 != None):
        df_bg_1_plus = pd.read_csv(options.bedgraph1,sep="\t",index_col=False,header=None,keep_default_na=False, na_values='')
    else:
        df_bg_1_plus = pd.DataFrame(index=[], columns=[])

    if (options.bedgraph2 != None):
        df_bg_2_plus = pd.read_csv(options.bedgraph2,sep="\t",index_col=False,header=None,keep_default_na=False, na_values='')
    else:
        df_bg_2_plus = pd.DataFrame(index=[], columns=[])

    if (options.bedgraph3 != None):
        df_bg_3_plus = pd.read_csv(options.bedgraph3,sep="\t",index_col=False,header=None,keep_default_na=False, na_values='')
    else:
        df_bg_3_plus = pd.DataFrame(index=[], columns=[])

    if (options.bedgraph1_minus_strand != None):
        df_bg_1_minus = pd.read_csv(options.bedgraph1_minus_strand,sep="\t",index_col=False,header=None,keep_default_na=False, na_values='')
    else:
        df_bg_1_minus = pd.DataFrame(index=[], columns=[])

    if (options.bedgraph2_minus_strand != None):
        df_bg_2_minus = pd.read_csv(options.bedgraph2_minus_strand,sep="\t",index_col=False,header=None,keep_default_na=False, na_values='')
    else:
        df_bg_2_minus = pd.DataFrame(index=[], columns=[])

    if (options.bedgraph3_minus_strand != None):
        df_bg_3_minus = pd.read_csv(options.bedgraph3_minus_strand,sep="\t",index_col=False,header=None,keep_default_na=False, na_values='')
    else:
        df_bg_3_minus = pd.DataFrame(index=[], columns=[])

    for bg_dataframe in [df_bg_1_plus,df_bg_1_minus,df_bg_2_plus,df_bg_2_minus,df_bg_3_plus,df_bg_3_minus]:
        if (bg_dataframe.empty == False):
            bg_dataframe.columns = ["chrom","position","depth"]
            bg_dataframe.drop(labels=["chrom"],axis="columns",inplace=True)
            bg_dataframe.set_index("position",inplace=True)

''' End reading of plus and minus strand RNA-seq depth graphs into dataframes (pandas/numpy) '''


results_output = open("./transcription_readthrough_results_"+str(my_exp_condition)+".tsv",'w')
pheatmap_matrix = open("./matrix_for_pheatmap_transcription_readthrough_results_"+str(my_exp_condition)+".tsv",'w')

results_output.write("ID_downstream_gene\tTSS\tTermseq site (inside 5'UTR)\tstrand\tTerminated transcript RPKM (S)\tFull transcript RPKM (L)\tTranscription readthrough (= S/L)\n")
#### write(str(downstreamGene)+"\t"+str(myTSS)+"\t"+str(my_5utrTermsite)+"\t"+str(myStrand)+"\t"+str(RPKM_short_transcript)+"\t"+str(RPKM_full_transcript)+"\t"+str(transcription_readthrough)+"\n")

candidate5UTRs_file = open(input5UTRtable, 'r')
enumCandidates = enumerate(candidate5UTRs_file)

for i,line in enumCandidates:
    line_fields = line.rstrip().split('\t')
    if ( (line_fields[0] == "Gene_or_locus_tag") or (i == 0) ):
        print("Read header line...\n")
        continue
    myTSS = int(line_fields[1])
    my_5utrTermsite = int(line_fields[2])
    myStrand = line_fields[4]
    downstreamGene = line_fields[0]

    if (options.withinKnownCDS == "yes"):
        my_stop_codon = int(line_fields[3])
    #### Bash commandline way of doing this.
    #### os.system("grep "+str(downstreamGene)+" "+str(annotations)+" > ./tempfile_annotation_for_"+str(downstreamGene)+".txt")
    if (options.withinKnownCDS == "no") and (myStrand == "+"):
        print("Running grep_gene_coords_from_GFF() to find stop codon for " + str(downstreamGene) + "...\n")
        CDS_lowerCoord, CDS_higherCoord = myFunctions.grep_gene_coords_from_GFF(downstreamGene, '+', annotations)
        if ((int(CDS_lowerCoord) == 0) and (int(CDS_higherCoord) == 0)):
            print("Error from grep_gene_coords_from_GFF(). Returned zero coordinates because I couldn't find the annotation line for gene " + str(downstreamGene) + "\n")
        my_stop_codon = int(CDS_higherCoord)
        #### if (downstreamGene in all_geneKeys_in_CDS_plusStrand):
        ####     my_stop_codon = all_cds_plusStrand[downstreamGene][1] ##### Get gene stop codon
        ####     print("stop codon found for the gene "+str(downstreamGene)+", it is "+str(my_stop_codon)+"\n")
        #### else:
        ####     print("Oops couldn't find the GFF line for "+str(downstreamGene)+" ?\n")
            #### nameToGrep, strandToGrep, gffFilePath



    if (options.withinKnownCDS == "no") and (myStrand == "-"):
        #### if (downstreamGene in all_geneKeys_in_CDS_minusStrand):
        ####     my_stop_codon = all_cds_minusStrand[downstreamGene][1] ##### Get gene stop codon
        ####     print("stop codon found for the gene "+str(downstreamGene)+", it is "+str(my_stop_codon)+"\n")
        #### else:
        print("Running grep_gene_coords_from_GFF() to find stop codon for "+str(downstreamGene)+"...\n")
        CDS_lowerCoord, CDS_higherCoord = myFunctions.grep_gene_coords_from_GFF(downstreamGene,'-',annotations)
        if ( (int(CDS_lowerCoord) == 0) and (int(CDS_higherCoord) == 0) ):
            print("Error from grep_gene_coords_from_GFF(). Returned zero coordinates because I couldn't find the annotation line for gene "+str(downstreamGene)+"\n")
        my_stop_codon = int(CDS_lowerCoord)


    RPKM_terminated = []
    RPKM_fullLength = []

    if ( options.rnaseq1 != None ) and ( options.rnaseq2 != None ):
        if ( str(options.rnaseq1+".")[-4:-1] == "bam" ) and ( str(options.rnaseq2+".")[-4:-1] == "bam" ):
            rawCovg_terminated_1 = myFunctions.find_total_covg_in_interval_samtools_pipe(myTSS, my_5utrTermsite, myStrand, my_RNAseq_bamfile1, workingDir)
            RPKM_terminated_1 = myFunctions.convert_to_RPKM(rawCovg_terminated_1, abs(myTSS-my_5utrTermsite), RNAseq1_totalReads)

            rawCovg_terminated_2 = myFunctions.find_total_covg_in_interval_samtools_pipe(myTSS, my_5utrTermsite, myStrand, my_RNAseq_bamfile2, workingDir)
            RPKM_terminated_2 = myFunctions.convert_to_RPKM(rawCovg_terminated_2, abs(myTSS-my_5utrTermsite), RNAseq2_totalReads)

            rawCovg_fullLength_1 = myFunctions.find_total_covg_in_interval_samtools_pipe(myTSS, my_stop_codon, myStrand, my_RNAseq_bamfile1, workingDir)
            RPKM_fullLength_1 = myFunctions.convert_to_RPKM(rawCovg_fullLength_1, abs(myTSS-my_stop_codon), RNAseq1_totalReads)

            rawCovg_fullLength_2 = myFunctions.find_total_covg_in_interval_samtools_pipe(myTSS, my_stop_codon, myStrand, my_RNAseq_bamfile2, workingDir)

            RPKM_fullLength_2 = myFunctions.convert_to_RPKM(rawCovg_fullLength_1, abs(myTSS-my_stop_codon), RNAseq2_totalReads)

            RPKM_terminated.append(RPKM_terminated_1)
            RPKM_terminated.append(RPKM_terminated_2)
            RPKM_fullLength.append(RPKM_fullLength_1)
            RPKM_fullLength.append(RPKM_fullLength_2)
        else:
            print("Error at least one bam file missing...\n")


    else:  ### read bedgraphs instead of bam files

        if (myStrand == "+"):
            bg1_to_read = df_bg_1_plus
            bg2_to_read = df_bg_2_plus
            bg3_to_read = df_bg_2_plus
        if (myStrand == "-"):
            bg1_to_read = df_bg_1_minus
            bg2_to_read = df_bg_2_minus
            bg3_to_read = df_bg_3_minus

        for graph_dataframe in [bg1_to_read,bg2_to_read,bg3_to_read]:

            ''' EDIT: Read from start codon or transcription start site (TSS) to terminator. Then from terminator to downstream segment of equal length.'''
            # Previously:
            # lines_full = graph_dataframe.loc[ min(myTSS,my_stop_codon):max(myTSS,my_stop_codon) ]["depth"]

            lines_terminated = graph_dataframe.loc[ min(my_5utrTermsite,myTSS):max(my_5utrTermsite,myTSS) ]["depth"]

            seg_len = abs(my_5utrTermsite-myTSS)
            if (seg_len <= 150):
                lines_full = graph_dataframe.loc[my_5utrTermsite:(my_5utrTermsite+seg_len)]["depth"]
            else:
                lines_full = graph_dataframe.loc[my_5utrTermsite:(my_5utrTermsite+150)]["depth"]

            ## dataframe check only for the first few and last few inputs
            if (i<5) or (i>7930):
                print("Checking dataframes after formula edit.\nlines_terminated:\n")
                lines_terminated.to_csv(sys.stdout)
                print("lines_full:\n")
                lines_full.to_csv(sys.stdout)


            rawCovg_T = int(np.add.reduce(lines_terminated).iloc[0])
            rawCovg_F = int(np.add.reduce(lines_full).iloc[0])

            ''' Reads from depth graph already converted to counts per million. FPKM is fragments per gene length per kilobase per million. Theremore just need to multiply by factor of 10^3/gene_length '''

            #----------------------------------------------------------------------------------------------------------
            if (options.normalisation == "cpm"):
                Length_term = abs(myTSS-my_5utrTermsite)
                Length_full = abs(myTSS-my_stop_codon)

                if (Length_term == 0):
                    print(f"Warning length of terminated transcript is 0 for the gene {downstreamGene} in line {i}? Cannot continue to divide by 0 for CPM to FPKM conversion.\n")
                    RPKM_terminated.append(0)
                    continue

                if (Length_full == 0):
                    print(f"Warning length of full transcript is 0 for the gene {downstreamGene} in line {i}? Cannot continue to divide by 0 for CPM to FPKM conversion.\n")
                    RPKM_fullLength.append(0)
                    continue

                RPKM_T = (rawCovg_T*1000.0)/Length_term
                RPKM_F = (rawCovg_F*1000.0)/Length_full
            #----------------------------------------------------------------------------------------------------------

            elif (options.normalisation == None):
                RPKM_T = rawCovg_T
                RPKM_F = rawCovg_F

            RPKM_terminated.append(RPKM_T)
            RPKM_fullLength.append(RPKM_F)


    if (RPKM_terminated == []) or (RPKM_terminated == [0,0,0]):
        RPKM_short_transcript = 0
    else:
        RPKM_short_transcript = mean(RPKM_terminated)

    if (RPKM_fullLength == []) or (RPKM_fullLength == [0,0,0]):
        RPKM_full_transcript = 0
    else:
        RPKM_full_transcript = mean(RPKM_fullLength)

    if (RPKM_full_transcript == 0):
        print("can't calculate transcription readthrough ratio because RPKM_full_transcript is 0")
        continue
    print("Debug. For the gene "+str(downstreamGene)+", RPKM_short_transcript is"+str(RPKM_short_transcript)+" and RPKM_full_transcript is "+str(RPKM_full_transcript)+"\n")
        ### if (RPKM_short_transcript != 0):
        ###    print("Average readthrough of the short transcript (TSS to Term-seq site) is "+str(RPKM_short_transcript)+" but there is no full trancsript readthrough??\n")
        ### ^ That's not actually possible because the read count of short transcript is a subset of full transcript.
        ###    transcription_readthrough = 0

    transcription_readthrough = RPKM_short_transcript/RPKM_full_transcript

    if (myOperSys == "unix"):
        results_output.write(str(downstreamGene)+"\t"+str(myTSS)+"\t"+str(my_5utrTermsite)+"\t"+str(myStrand)+"\t"+str(RPKM_short_transcript)+"\t"+str(RPKM_full_transcript)+"\t"+str(transcription_readthrough)+"\n")
    elif (myOperSys == "windows"):
        results_output.write(str(downstreamGene)+"\t"+str(myTSS)+"\t"+str(my_5utrTermsite)+"\t"+str(myStrand)+"\t"+str(RPKM_short_transcript)+"\t"+str(RPKM_full_transcript)+"\t"+str(transcription_readthrough)+"\r\n")
    else:
        results_output.write(str(downstreamGene)+"\t"+str(myTSS)+"\t"+str(my_5utrTermsite)+"\t"+str(myStrand)+"\t"+str(RPKM_short_transcript)+"\t"+str(RPKM_full_transcript)+"\t"+str(transcription_readthrough)+"\n")
    if (myOperSys == "unix"):
        pheatmap_matrix.write(str(downstreamGene)+"\t"+str(transcription_readthrough)+"\n")
        #### In RStudio: t(pheatmap_matrix)
        #### Transpose so that genes are column names and transcription readthrough is value.
        #### Stick together so that Experimental Condition is row name.
    elif (myOperSys == "windows"):
        pheatmap_matrix.write(str(downstreamGene)+"\t"+str(transcription_readthrough)+"\r\n")
    else:
        pheatmap_matrix.write(str(downstreamGene)+"\t"+str(transcription_readthrough)+"\n")


results_output.close()
pheatmap_matrix.close()

print("Finished script run\n")
######### Next: Make heatmap in RStudio.

######### The format needed for pheatmap() in R.
##               Gene1      Gene2      Gene3      Gene4      Gene5
## Subject1 -0.3330860  0.2515602 -0.7263684 -0.1257963  1.3159837
## Subject2 -0.4128702 -0.9761655  0.2132626  0.7267015 -0.3634427
## Subject3  0.9432688  0.2973164 -1.1210232  0.9017424 -0.8098157
## Subject4  0.2894936 -0.6708301 -0.8209677  0.2204200 -1.0602915
## Subject5 -1.2131869 -1.2346285 -0.1120417  0.9439494  0.5893691
