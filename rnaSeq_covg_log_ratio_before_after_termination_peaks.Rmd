---
title: "rnaSeq_covg_log_ratio_before_after_termination_peaks"
author: "Sylvania Wu"
date: "15/04/2020"
output:
  html_document:
    highlight: pygments

---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}

if(!require(pacman)) {
  install.packages("pacman")
  libary(pacman)
}

p_load(tidyverse)

p_load("foreach")
library("foreach")

p_load("doParallel")
library("doParallel")

p_load("purrr")
library(purrr)

p_load("furrr")
library(furrr)

```


```{r}
base_dir <- file.path("/Users","z3460340","Documents","howden_2013_data_RNAseq","bedgraphs")

control71_bg_path <- file.path(base_dir, "SRR568071_rnaseq_howdenetal_aligned_august5.bedgraph")
control72_bg_path <- file.path(base_dir, "SRR568072_rnaseq_howdenetal_aligned_august5.bedgraph")
linezolid75_bg_path <- file.path(base_dir,"SRR568075_rnaseq_howdenetal_aligned_august7.bedgraph")
linezolid76_bg_path <- file.path(base_dir,"SRR568076_rnaseq_howdenetal_aligned_august5.bedgraph")
```


```{r}
sharefiles_dir <- file.path("/Users","z3460340","Documents","Igy_ShareFiles","Igy_Shared_With_Me_April_9_2020")

termPeaks_master <- vroom::vroom( file.path(sharefiles_dir,"master_table_win8_sd7_cpm1_gw5.tsv"), col_names=TRUE )

termPeaks_master
```


```{r}
control71_bedgraph <- vroom::vroom(control71_bg_path,col_names = FALSE)
colnames(control71_bedgraph) <- c("genome","position","rnaseq_coverage")
control71_bedgraph[950,]
```



***Code interpretation of Igy's advice (15/04/2020)***
*"I will take a window size of X nucleotides around the termination site on either side. Get the median read depths, add 1 (so that it does not break log transform), log2 it and get a log ratio. Do this for all termination sites and at all conditions. Probably want to do before termination sites divided after termination sites, taking care of strands."*


```{r,eval = FALSE}
window_size <- 100
my_condition <- "linz"

log2ratio_covg_in_bedgraph <- function(bedgraph_df, fromStart, toMiddle, toEnd){
  
  covg_before_middle <- bedgraph_df[fromStart:toMiddle,3]
  covg_after_middle <- bedgraph_df[fromStart:toMiddle,3]
  
  return( log2(  (median(covg_before_middle)+1)/(median(covg_after_middle)+1)  ) )
  
}

log2ratios_iterate_grouped <- function(termPeaks_df, exp_condition, bedGraph){
  
  group_num <- 1
  current_group <- vector()
  
  grouped_result <- foreach ( d=iter(termPeaks_df, by='row'), .combine = "row" ) %do% {  
    
  term_posn <- as.numeric(d[2])
  
  if (  group_num == as.numeric(d[1])  ){
    current_group <- c(  current_group, term_posn )
  } else {
    ## calculate average genomic position of previous group first
    avg_pos_in_group <- mean(current_group)
    window_start <- floor(avg_pos_in_group - window_size)
    window_end <- ceiling(avg_pos_in_group + window_size)
    
    ## return this value!!
    log2ratio_covg_in_bedgraph(bedGraph, window_start, avg_pos_in_group, window_end)
    
    ## now add position for the new group
    current_group <- vector()
    current_group <- c(  current_group, term_posn  )
  } ## end if-else
  } ##     end foreach
  
  
} # end function definition
# .x is term-seq peak position (1 bp), .y is the window size, .z is the RNA-seq coverage (bedgraph DF)



#               DRAFT              #
### works!
newmap <- as_mapper( ~log2( median(control71_bedgraph$rnaseq_coverage[(.x+.y):.x])+1) )
## works!
future_map2_dbl(c(1601,434,573362),50,newmap)
## test
log2(342)
## more test code
termPeaks_cont <- termPeaks_master %>% filter(cont == 1)
termPeaks_list <- as.list( termPeaks_cont %>% select(position, cont) )
termPeaks_list[["position"]] %>% future_map(~.x - 10000)

##  tibble for manual calculation check: data.frame(termPeaks_master[600:650,] %>% filter(cont == 1) %>% select(position, cont))[21:30,]
##   position  cont
##     <dbl> <dbl>
##   1   420965     1
##   2   421097     1
##   3   421118     1
##   4   421150     1
##   5   421165     1
##   6   426831     1
##   7   427053     1
##   8   428165     1
##   9   430371     1
##  10   430372     1

##  peaks_filtered.position
##  <dbl>
##  log2med_depth_before
##  <dbl>
##  log2med_depth_after
##  <dbl>
##  420965	2.0000000	2.0000000	
##  421097	3.4594316	3.7004397	
##  421118	2.9068906	2.8073549	
##  421150	2.0000000	2.0000000	
##  421165	1.5849625	0.5849625	
##  426831	5.6293566	5.3750394	
##  427053	3.4594316	3.5849625	
##  428165	2.3219281	2.3219281	
##  430371	0.0000000	1.0000000	
##  430372	0.5849625	1.0000000

# I manually checked the results
control71_bedgraph$rnaseq_coverage[420960:420965] # all equal to 3
log2(3+1) # correct
control71_bedgraph$rnaseq_coverage[421092:421097] # equal to 10 10 10 10 11 12
median( c(10, 10, 10, 10, 11, 12) ) +1 # equal to 11
log2(11) # 3.459432 equal to 3.4594316 correct
control71_bedgraph$rnaseq_coverage[(426831-5):426831] # equal to 48, 49, 49, 49, 44, 45
median(c(48, 49, 49, 49, 44, 45)) + 1
log2(49.5) # 5.629357 equal to 5.6293566 correct




## here, seq_covg_ordered is the rna-seq coverage ordered EXACTLY from genomic position 1 to end of genome. (control71_bedgraph$rnaseq_coverage)
log2ratios_map <- function(peaks_filtered, sample_name, seq_covg_ordered, window_bp=5){
  
  plan(multiprocess)
  
  log2_median_covg_of_window <- as_mapper( ~log2( median(seq_covg_ordered[(.x+.y):.x])+1) )
  
  # UPSTREAM/before termination peak: log2 median of rna-seq coverage.
  log2med_depth_before <- future_map2_dbl(peaks_filtered$position,-window_bp,log2_median_covg_of_window)
  
  # DOWNSTREAM/after termination peak: log2 median of rna-seq coverage.
  log2med_depth_after <- future_map2_dbl(peaks_filtered$position,window_bp,log2_median_covg_of_window)
  
 # df_with_ratio <- peaks_filtered %>% mutate(log2_median_rnaseq_before = log2med_depth_before, log2_median_rnaseq_after = log2med_depth_after, rnaseq_before_after_ratio = log2med_depth_before/log2med_depth_after)
  df_with_ratio <- data.frame( peaks_filtered$position,log2med_depth_before,log2med_depth_after,before_after_ratio=log2med_depth_before/log2med_depth_after)
  
  colnames(df_with_ratio) <- c("position",paste0("log2med_depth_before_in",sample_name),paste0("log2med_depth_after_in",sample_name),paste0("before_after_ratio_in",sample_name))
  
  return( df_with_ratio )
  
}

termPeaks_cont_filtered <- termPeaks_master[600:650,] %>% filter(cont == 1) %>% select(position, cont)

# works!
my_sample_name <- "control71_subset"
res_cont71_subset <- log2ratios_map( termPeaks_cont_filtered,my_sample_name,control71_bedgraph$rnaseq_coverage, 5 )

write_files_out_path <- here::here()

write.table(res_cont71_subset,file.path(write_files_out_path,paste0("rnaSeq_log2_median_coverage_before_and_after_termination_peaks_",my_sample_name,".tsv")), row.names = FALSE,sep="\t",append=TRUE)

#works!
test_subbing <- function(var_input){     ## attempt to turn variable value into dataframe column name works
  cn <- as.character(substitute(var_input))
  res_cont71_subset <- log2ratios_map(termPeaks_cont_filtered,cn,control71_bedgraph$rnaseq_coverage, 5 )
  res_cont71_subset
}

test_subbing("cont")



```

```{r, engine = 'bash', eval = FALSE}
#!/bin/bash

#PBS -N rnaSeq_covg_log_ratio_before_after_termination_peaks
#PBS -l nodes=1:ppn=10
#PBS -l mem=20gb
#PBS -l walltime=08:00:00
#PBS -j oe
#PBS -M z3460340@student.unsw.edu.au
#PBS -m aej

module load openmpi/4.0.3
module load R/3.6.1

cd $PBS_O_WORKDIR

COMMAND_TO_RUN="Rscript --vanilla rnaSeq_covg_log_ratio_before_after_termination_peaks.r $rnaseq_bedgraph $termpeaks_master 'cont' 20"

echo "Running command: $COMMAND_TO_RUN "

$COMMAND_TO_RUN
```