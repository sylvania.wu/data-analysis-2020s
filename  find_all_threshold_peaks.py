#!/usr/bin/python
import os
from optparse import OptionParser
import re
import functions_used_in_find_5utrs as myFunctions
import numpy as np
import pylab


######################### Last updated March 8th, 2020 at 4:41 PM #####################################################
## March 8th, 2020: In thresholding_algo(): Try to filter positions with read depth 1 or 0.
## March 8th, 2020: Try to use median and median absolute deviation (MAD) instead of avg and std.


##            Output should print in GFF format now. For easy use with bedtools intersect.

# Find in myFunctions: thresholding_algo, filter_signals_by_expected_seqRead_width, parse_depth_graph, plot_thresholding_results

parser = OptionParser()

# Coverage graph of all Term-seq in a given interval, one strand only.
parser.add_option("-c", "--coverageGraph",action="store",type="string",dest="coverageGraph")

# GFF annotation file defining an interval no bigger than 100 kbp.
parser.add_option("-g", "--gff",action="store",type="string",dest="annotations_file")

## Arguments to thresholding_algo()
parser.add_option("-L", "--lag",action="store",type="int",dest="set_lag")
parser.add_option("-i", "--influence",action="store",type="float",dest="set_influence")
parser.add_option("-t", "--threshold",action="store",type="float",dest="set_threshold")

parser.add_option("-s", "--strand",action="store",type="string",dest="scanStrand")

parser.add_option("-m", "--minreads",action="store",type="int",dest="minCoverage")
parser.add_option("-r", "--seqReadLength",action="store",type="int",dest="seqReadLength")

parser.add_option("-I", "--imageLimit",action="store",type="float",dest="imageLim")

parser.add_option("-S", "--startInterval",action="store",type="int",dest="startInterval")
parser.add_option("-E", "--endInterval",action="store",type="int",dest="endInterval")

parser.add_option("-D", "--discreteMode",action="store",type="string",dest="discreteMode")


(options, args) = parser.parse_args()

## discrete mode used for coverage where only one nucleotide per read is counted.
discrete_mode = options.discreteMode  ## Type "MAD", "minreads", or "no"
set_lag = options.set_lag
set_influence = options.set_influence
set_threshold = options.set_threshold

min_read_depth = options.minCoverage

termseq_coverage = options.coverageGraph

# opened_gff = open(str(options.annotations_file),'r')

if (options.annotations_file == None) or (options.annotations_file == "none"):
    start_interval = options.startInterval
    end_interval = options.endInterval
    genome_name = ""
else:
    import subprocess

    first_gff_line = subprocess.check_output(['head', '-n1', options.annotations_file]) # Fixed bug. A file path enclosed in single and double quotes is NOT read properly by commandline head -n1 "'/my/path/to/file'"
    first_gff_line = first_gff_line.decode("utf-8")

    if ("##sequence" in first_gff_line) or ("##species" in first_gff_line):
        print("python find_all_threshold_peaks(): Please manually remove header line before GFF input. Quitting...\n")
        quit()

    last_gff_line = subprocess.check_output(['tail', '-1', options.annotations_file])
    last_gff_line = last_gff_line.decode("utf-8")

    if last_gff_line == '\n':
        last_gff_line = subprocess.check_output(['head', '-n2', options.annotations_file])

    genome_name = first_gff_line.split('\t')[0].replace('.','')
    #  debug:           genome_name = "CP0021201"
    #  debug:           print("First gff line read?\n")
    #  debug:           print(first_gff_line)
    #  debug:           print("\nLast gff line read?\n")
    #  debug:           print(last_gff_line)

    start_interval = int(first_gff_line.split('\t')[3])
    end_interval = int(first_gff_line.split('\t')[4])

# print("python find_all_threshold_peaks(): Opening output files with open(file, 'w+')...")

os.system("touch ./Results_"+"Peaks_in_"+str(genome_name)+"_strand_"+str(options.scanStrand)+"_interval_"+str(start_interval)+"_"+str(end_interval)+"_influence_"+str(set_influence)+"_threshold_"+str(set_threshold)+"_lag_"+str(set_lag))
os.system("touch ./Results_"+"Peaks_in_"+str(genome_name)+"_strand_"+str(options.scanStrand)+"_interval_"+str(start_interval)+"_"+str(end_interval)+"_influence_"+str(set_influence)+"_threshold_"+str(set_threshold)+"_lag_"+str(set_lag)+".gff3")

final_results = open( "./Results_"+"Peaks_in_"+str(genome_name)+"_strand_"+str(options.scanStrand)+"_interval_"+str(start_interval)+"_"+str(end_interval)+"_influence_"+str(set_influence)+"_threshold_"+str(set_threshold)+"_lag_"+str(set_lag),'w' )
output_gff = open( "./Results_"+"Peaks_in_"+str(genome_name)+"_strand_"+str(options.scanStrand)+"_interval_"+str(start_interval)+"_"+str(end_interval)+"_influence_"+str(set_influence)+"_threshold_"+str(set_threshold)+"_lag_"+str(set_lag)+".gff3",'w' ) ## w+ for write?

output_gff.write("python find_all_threshold_peaks(): Start writing results? ...\n")
final_results.write("python find_all_threshold_peaks(): Start writing results...\n")

if options.scanStrand == '+':
    # strandLabel = "+"
    # print("find_all_threshold_peaks.py: About to run parse_depth_graph...\n")
    (local_bases, local_read_depth) = myFunctions.parse_depth_graph(start_interval,end_interval,path_to_gr_file=termseq_coverage, direction="descending" ) ## search 3' to 5'
    # print("find_all_threshold_peaks.py: About to run thresholding_algo()...\n")
    local_results = myFunctions.thresholding_algo(y=local_read_depth,lag=set_lag, influence=set_influence, threshold=set_threshold, discreteMode=discrete_mode)

elif options.scanStrand == '-':
    # strandLabel = "-"
    # print("find_all_threshold_peaks.py: About to run parse_depth_graph...\n")
    (local_bases, local_read_depth) = myFunctions.parse_depth_graph(start_interval,end_interval, path_to_gr_file=termseq_coverage, direction="ascending") ## search 3' to 5'
    # print("find_all_threshold_peaks.py: About to run thresholding_algo()...\n")
    local_results = myFunctions.thresholding_algo(y=local_read_depth,lag=set_lag, influence=set_influence, threshold=set_threshold, discreteMode=discrete_mode)
                  ##                    Searched 3'-to-5'.
else:
    print("options.scanStrand not an expected value. local_results not defined\n")
    print("options.scanStrand: "+str(options.scanStrand)+"\n")

# print('python find_all_threshold_peaks(): Printing local_results["signals"] for interval '+str(start_interval)+' to '+str(end_interval)+'\n')
# print(local_results["signals"])
# print("\n")

#imageLimit = 0
#if imageLimit <= options.imageLim:

myFunctions.plot_thresholding_results(baseArray=local_bases,depthArray=local_read_depth,resultsDict=local_results,minStdDev=set_threshold,nameFile="Peaks_in_"+str(genome_name)+"_strand_"+str(options.scanStrand)+"_interval_"+str(start_interval)+"_"+str(end_interval)+"_influence_"+str(set_influence)+"_threshold_"+str(set_threshold)+"_lag_"+str(set_lag))
            ## Plot result with pylab. Only print out 20 images to save space and memory
    #imageLimit=imageLimit+1

if (1 in local_results["signals"]):
    seq_read_length_peaks = myFunctions.filter_signals_by_expected_seqRead_width(readWidth=options.seqReadLength,tolerance=20,searchRange=range(0, len(local_results["signals"])-1),signalArray=local_results["signals"])
    # Indent 1
    for index in range( 0, len(local_results["signals"])-1 ):
        # print("Term-seq peak found (1 is in signals)\n")
        # Indent 2
        if ( local_results["signals"][index] == 1 ) and ( local_results["signals"][index-1] == 0 ) and (int(local_read_depth[index]) >= min_read_depth):
            # Indent 3
            signal_base = local_bases[index]

            # print("Term-seq peak found at: "+str(signal_base)+"\tStrand: "+str(options.scanStrand)+"\tRead depth: "+str(local_read_depth[index])+"\n")

            if index in seq_read_length_peaks.keys():
                # Indent 4
                # GFF3 Fields:
                # 1) seqid, 2) source, 3) type, 4) start, 5) end, 6) score(.), 7) strand, 8) phase(.), 9) attributes
                # EDIT myFunctions.filter_signals_by_expected_seqRead_width() to show where does the signal ends!!!
                signal_drop_off = seq_read_length_peaks[index] ### seq_read_length_peaks return object is dict[key = 3'end of signal] -> value = 5'end of signal
                if signal_drop_off != "NA":
                    ### To move 3'-to-5', just increase index of local_bases array.
                    signal_5end = local_bases[index+signal_drop_off]
                    output_gff.write("peakSignal_from_"+str(local_bases[index])+"_to_"+str(signal_5end)+"_fromInterval"+str(start_interval)+"\tTermSeq\tpeakSignal\t"+str(local_bases[index])+"\t"+str(signal_5end)+"\t.\t"+str(options.scanStrand)+"\t.\tID=peakSignal_from_"+str(local_bases[index])+"_to_"+str(signal_5end)+"_fromInterval"+str(start_interval)+";\n" )
                else:
                    print("Can't print end of the signal at genome coordinate "+str(local_bases[index]+" to GFF file because 5'-end not found...\n"))

                final_results.write("Term-seq peak found at: "+str(signal_base)+"\tStrand: "+str(options.scanStrand)+"\tRead depth: "+str(local_read_depth[index])+"\tPeak width is sequencing read length +/- 20 nt\n")
                output_gff.write("peakSignal_from_"+str(local_bases[index])+"_to_"+str(signal_5end)+"_fromInterval"+str(start_interval)+"\tTermSeq\tpeakSignal\t"+str(local_bases[index])+"\t"+str(signal_5end)+"\t.\t"+str(options.scanStrand)+"\t.\tID=peakSignal_from_"+str(local_bases[index])+"_to_"+str(signal_5end)+"_fromInterval"+str(start_interval)+";\n" )

            else:
                final_results.write("Term-seq peak found at: "+str(signal_base)+"\tStrand: "+str(options.scanStrand)+"\tRead depth: "+str(local_read_depth[index])+"\n")
                output_gff.write("peakSignal_from_"+str(local_bases[index])+"_to_"+str(signal_5end)+"_fromInterval"+str(start_interval)+"\tTermSeq\tpeakSignal\t"+str(local_bases[index])+"\t"+str(signal_5end)+"\t.\t"+str(options.scanStrand)+"\t.\tID=peakSignal_from_"+str(local_bases[index])+"_to_"+str(signal_5end)+"_fromInterval"+str(start_interval)+";\n" )






final_results.close()
output_gff.close()

print("Finished program run.\n")
