# Data-analysis-2020s

Files list for Igy

Raw bamfiles, but I have corrected the strand-labelling (original sam/bam files from Vertis Biotech's raw data had incorrect strand label for Term-Seq VS dRNA-seq)
/srv/scratch/z3460340/PhDfiles/termseq2019/all_Saureus_Termseq_results/jkd6009_termseq_project/alignment_samfiles_for_viewing/correct_orientation_termseq_sam/



Strand-specific bam files
/srv/scratch/z3460340/PhDfiles/termseq2019/all_Saureus_Termseq_results/jkd6009_termseq_project/alignment_samfiles_for_viewing/correct_orientation_termseq_sam/strand_specific_graph_files




The Term-seq coverage in *.gr format. That means two-column tab-delimited, first col is position in genome, second col is raw Term-seq number of reads. (Made using bedtools genomecov)
/srv/scratch/z3460340/PhDfiles/termseq2020/thresholding_3end_peaks_JKD6008

List of files...
control_run1_termseq_correctMinusStrand.gr
control_run1_termseq_correctPlusStrand.gr
linezolid_run1_termseq_correctMinusStrand.gr
linezolid_run1_termseq_correctPlusStrand.gr
tigecycline_run3_termseq_correctMinusStrand.gr
tigecycline_run3_termseq_correctPlusStrand.gr
vancomycin_run2_termseq_correctMinusStrand.gr
vancomycin_run2_termseq_correctPlusStrand.gr



The RNA 3'ends I have called using my thresholding algorithm set with parameters: Threshold = 6 (standard deviations from local mean read depth) , Influence = 0 or 0.1 or 0.05 (Fraction of a positive detected peak's read depth to count towards the local mean), Lag = 1000 bp (width of moving window to find the local mean).

Results are split by folders named for: Month of analysis / antibiotic condition and biological replicate 1 or 2 / genome strand / 1/40th subset of the genome positions / name of python program / threshold influence lag
See my thresholding algorithm python script for the exact formulae.

Here are the files locally:
/srv/scratch/z3460340/PhDfiles/termseq2020/data_2020_git_repository/data-analysis-2020s/Results_thresholding_3ends_S_aureus_Feb_2020

My PBS script was told to automatically transfer any files to GitLab if my scratch drive gets 90% full. So look in here if anything is missing:
I gave Igy access to GitLab! :)

https://gitlab.com/sylvania.wu/data-analysis-2020s/-/tree/master/Results_thresholding_3ends_S_aureus_Feb_2020

Warning: If it's not missing from the local (katana) folder, then it may be an older version of these results which may be incorrect. Trust the local files if in doubt. 





Raw bam files for dRNA-seq. Two conditions, vancomycin+ and no antibiotic (untreated). Two biological replicates per condition. Some files are separated by plus/minus strand of genome.
/srv/scratch/z3460340/PhDfiles/dRNAseq2018/


My transcription start sites in *gff format. Found by the TSSPredator program in September 12, 2018.
/home/z3460340/directory_structure/data/TSS_more_sensitive_*gff


My transcription start sites, full set of results in different formats. MasterTable.tsv has more information given by TSSPredator (e.g. gene with TSS, whether the TSS is intergenic, within-gene…..)
For reference in file names, “FivePrime” means “With added Terminator Exonuclease (TEX)” and “Normal” means no TEX. Minus and Plus refer to strands of the genome. VAN means vancomycin.

/home/z3460340/directory_structure/results/TSSPredator_updated_more_sensitive_results
