#!/usr/bin/env Rscript



# title rnaSeq_covg_log_ratio_before_after_termination_peaks
#----------------------------------------------------------
if(!require(pacman)) {
  install.packages("pacman")
  libary(pacman)
}

p_load("tidyverse")
library(tidyverse)

p_load("purrr")
library(purrr)

p_load("furrr")
library(furrr)
#----------------------------------------------------------



#----------------------------------------------------------
args = commandArgs(trailingOnly=TRUE)

if (length(args)==2) {
  # default output file
  bg_path <- args[1]
  termPeaks_path <- args[2]
  my_sample_name <- "" # no name for sample
  window_size_arg <- 0
} else if (length(args)==3){
  bg_path <- args[1]
  termPeaks_path <- args[2]
  my_sample_name <- args[3]
  window_size_arg <- 0
} else if (length(args)==4){
  bg_path <- args[1]
  termPeaks_path <- args[2]
  my_sample_name <- args[3]
  window_size_arg <- args[4]
} else { stop("Two or three arguments must be supplied (rna-seq bedgraph + term-peaks master table + sample name + window size).n", call.=FALSE) }
#----------------------------------------------------------


#----------------------------------------------------------
termPeaks_master <- vroom::vroom( termPeaks_path, col_names=TRUE )
bedgraph_df <- vroom::vroom(bg_path,col_names = FALSE)
colnames(bedgraph_df) <- c("genome","position","rnaseq_coverage")
#----------------------------------------------------------


#----------------------------------------------------------
log2ratios_map <- function(peaks_filtered, sample_name, seq_covg_ordered, window_bp=10){
  
  plan(multiprocess)
  
  log2_median_covg_of_window <- as_mapper( ~log2( median(seq_covg_ordered[(.x+.y):.x])+1) )
  
  # UPSTREAM/before termination peak: log2 median of rna-seq coverage.
  log2med_depth_before <- future_map2_dbl(peaks_filtered$position,-window_bp,log2_median_covg_of_window)
  
  # DOWNSTREAM/after termination peak: log2 median of rna-seq coverage.
  log2med_depth_after <- future_map2_dbl(peaks_filtered$position,window_bp,log2_median_covg_of_window)
  
  # df_with_ratio <- peaks_filtered %>% mutate(log2_median_rnaseq_before = log2med_depth_before, log2_median_rnaseq_after = log2med_depth_after, rnaseq_before_after_ratio = log2med_depth_before/log2med_depth_after)
  df_with_ratio <- data.frame( peaks_filtered$position,log2med_depth_before,log2med_depth_after,before_after_ratio=log2med_depth_before/log2med_depth_after)
  
  colnames(df_with_ratio) <- c("position",paste0("log2med_depth_before_in",sample_name),paste0("log2med_depth_after_in",sample_name),paste0("before_after_ratio_in",sample_name))
  
  return( df_with_ratio )
  
}
#----------------------------------------------------------




#----------------------------------------------------------
cn <- as.character(substitute(my_sample_name))
termPeaks_filtered <- termPeaks_master %>% filter(cn == 1) %>% select(position, cn)

# works!
if (window_size_arg > 0) {
  result <- log2ratios_map( peaks_filtered=termPeaks_filtered,sample_name=my_sample_name,seq_covg_ordered=bedgraph_df$rnaseq_coverage, window_bp=window_size_arg )
} else {  # default to window size of 10 bp
  result <- log2ratios_map( peaks_filtered=termPeaks_filtered,sample_name=my_sample_name,seq_covg_ordered=bedgraph_df$rnaseq_coverage )
}

write_files_out_path <- here::here()

write.table(result,file.path(write_files_out_path,paste0("rnaSeq_log2_median_coverage_before_and_after_termination_peaks_",my_sample_name,".tsv")), row.names = FALSE,sep="\t",append=TRUE)
#----------------------------------------------------------



